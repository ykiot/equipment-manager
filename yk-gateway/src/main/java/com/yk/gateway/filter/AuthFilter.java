package com.yk.gateway.filter;

import cn.dev33.satoken.context.SaHolder;
import cn.dev33.satoken.error.SaErrorCode;
import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaHttpMethod;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import cn.dev33.satoken.util.SaResult;
import com.yk.common.core.constant.HttpCodeStatus;
import com.yk.gateway.properties.IgnoreWhiteProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;


/**
 * [Sa-Token 权限认证] 拦截器
 * @author lmx
 * @description
 * @date 2023/10/16 14:54
 */
@Slf4j
@Configuration
public class AuthFilter {

    public static final long TIMEOUT = 300L;
    public static final long RENEW = 259200L;

    /**
     * 注册 Sa-Token 全局过滤器
     */
    @Bean
    public SaReactorFilter getSaReactorFilter(IgnoreWhiteProperties ignoreWhiteProperties) {
        return new SaReactorFilter()
                // 拦截地址
                .addInclude("/**")
                // 开放地址
                .setExcludeList(ignoreWhiteProperties.getWhites())
                // 鉴权方法：每次访问进入
                .setAuth(obj -> {
                    // 登录校验 -- 拦截所有路由，并排除/user/doLogin 用于开放登录
                    SaRouter.match("/**",  r -> StpUtil.checkLogin());
                    SaRouter.match("/yk-system/**",  r -> {
                        long tokenTimeout = StpUtil.getTokenTimeout();
                        if (tokenTimeout <= TIMEOUT) {
                            StpUtil.renewTimeout(RENEW);
                        }
                    });
                })
                // 异常处理方法：每次setAuth函数出现异常时进入
                .setError(e -> {
                    if (e instanceof NotLoginException) {
                        NotLoginException notLoginException = (NotLoginException) e;
                        if (SaErrorCode.CODE_11014 == notLoginException.getCode()) {
                            return SaResult.error("认证失败，无法访问系统资源").setCode(HttpCodeStatus.LINE);
                        }
                    }
                    return SaResult.error("认证失败，无法访问系统资源").setCode(HttpStatus.UNAUTHORIZED.value());
                })
                // 前置函数：在每次认证函数之前执行
                .setBeforeAuth(obj -> {
                    //  设置跨域响应头
                    SaHolder.getResponse()
                            // 允许指定域访问跨域资源
                            .setHeader("Access-Control-Allow-Origin", "*")
                            // 允许所有请求方式
                            .setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE")
                            // 有效时间
                            .setHeader("Access-Control-Max-Age", "3600")
                            // 允许的header参数
                            .setHeader("Access-Control-Allow-Headers", "*");

                    // 如果是预检请求，则立即返回到前端
                    SaRouter.match(SaHttpMethod.OPTIONS)
                            .free(r -> System.out.println("--------OPTIONS预检请求，不做处理"))
                            .back();
                });
    }
}
