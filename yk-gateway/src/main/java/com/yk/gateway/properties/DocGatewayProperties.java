package com.yk.gateway.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;


/**
 * swagger配置属性
 *
 * @author lmx
 * @date 2023/10/17 16:44
 */
@Data
@Configuration
@RefreshScope
@ConfigurationProperties(prefix = "security.doc")
public class DocGatewayProperties {
    private Boolean enabale;
    private String resources;
}
