package com.yk.common.rabbitmq.listener;

import com.yk.common.rabbitmq.constant.QueueConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

/**
 * file-listener 示例
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
public class FileQueueListener {

	/**
	 * 监听 file 队列的处理器
	 */
	@RabbitListener(queues = QueueConstants.FILE_QUEUE)
	@RabbitHandler
	public void onMessage(Long fileId) {
		log.info("文件消费端{}" ,fileId);
	}
}