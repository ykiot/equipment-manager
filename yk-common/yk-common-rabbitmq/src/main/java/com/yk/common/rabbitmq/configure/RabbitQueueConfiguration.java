package com.yk.common.rabbitmq.configure;

import com.yk.common.rabbitmq.constant.QueueConstants;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * mq队列声明，启动时，会生成该队列
 *
 * @author lmx
 * @date 2023/10/21 9:48
 */
@Configuration(proxyBeanMethods = false)
public class RabbitQueueConfiguration {

    /**
     * 设备历史数据入库队列
     */
    @Bean
    public Queue deviceHistory() {
        return new Queue(QueueConstants.DEVICE_HISTORY);
    }

    /**
     * 设备状态更新状态队列
     */
    @Bean
    public Queue deviceUpdateStatus() {
        return new Queue(QueueConstants.DEVICE_UPDATE_STATUS);
    }

    /**
     * 发送短信验证码
     */
    @Bean
    public Queue sendSms() {
        return new Queue(QueueConstants.SEND_SMS);
    }

    /**
     * 发送短信报警通知信息
     */
    @Bean
    public Queue sendAlarmSms() {
        return new Queue(QueueConstants.SEND_ALARM_SMS);
    }

    /**
     * 发送websocket消息
     */
    @Bean
    public Queue sendWebSocket() {
        return new Queue(QueueConstants.SEND_WEBSOCKET);
    }

    /**
     * 发送微信
     */
    @Bean
    public Queue sendWechat() {
        return new Queue(QueueConstants.SEND_WECHAT);
    }

    /**
     * 网关信息更新
     */
    @Bean
    public Queue gatewayUpdate() {
        return new Queue(QueueConstants.GATEWAY_UPDATE);
    }

    /**
     * 网关离线状态更新
     */
    @Bean
    public Queue gatewayOfflineUpdate() {
        return new Queue(QueueConstants.GATEWAY_OFFLINE_UPDATE);
    }

    /**
     * 自定义报表导出
     */
    @Bean
    public Queue reportExport() {
        return new Queue(QueueConstants.REPORT_EXPORT);
    }

    /**
     * 消息入库
     */
    @Bean
    public Queue messageSave() {
        return new Queue(QueueConstants.MESSAGE_SAVE);
    }

    /**
     * 流量卡流量查询
     */
    @Bean
    public Queue searchCard() {
        return new Queue(QueueConstants.SEARCH_CARD);
    }

    /**
     * 设备迁移数据队列
     */
    @Bean
    public Queue deviceMigrate() {
        return new Queue(QueueConstants.DEVICE_MIGRATE);
    }

}
