package com.yk.common.rabbitmq.constant;

/**
 * 队列配置类
 *
 * @author lmx
 * @date 2023/10/21 9:50
 */
public interface QueueConstants {

    /**
     * 文件队列
     */
    String FILE_QUEUE = "FILE";

    /**
     * 设备历史数据
     */
    String DEVICE_HISTORY = "DEVICE_HISTORY";

    /**
     * 设备状态更新
     */
    String DEVICE_UPDATE_STATUS = "DEVICE_UPDATE_STATUS";

    /**
     * 发送短信验证码
     */
    String SEND_SMS = "SEND_SMS";

    /**
     * 发送短信报警通知信息
     */
    String SEND_ALARM_SMS = "SEND_ALARM_SMS";

    /**
     * 发送websocket消息
     */
    String SEND_WEBSOCKET = "SEND_WEBSOCKET";

    /**
     * 发送微信
     */
    String SEND_WECHAT = "SEND_WECHAT";

    /**
     * 网关信息更新
     */
    String GATEWAY_UPDATE = "GATEWAY_UPDATE";

    /**
     * 网关离线状态更新
     */
    String GATEWAY_OFFLINE_UPDATE = "GATEWAY_OFFLINE_UPDATE";

    /**
     * 自定义报表导出
     */
    String REPORT_EXPORT = "REPORT_EXPORT";

    /**
     * 消息入库
     */
    String MESSAGE_SAVE = "MESSAGE_SAVE";

    /**
     * 流量卡流量查询
     */
    String SEARCH_CARD = "SEARCH_CARD";


    /**
     * 设备数据迁移
     */
    String DEVICE_MIGRATE = "DEVICE_MIGRATE";
}
