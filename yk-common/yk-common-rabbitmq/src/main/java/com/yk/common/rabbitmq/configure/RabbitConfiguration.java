package com.yk.common.rabbitmq.configure;

import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbitmq 配置
 * @author lmx
 * @date 2023/10/21 9:48
 */
@Configuration(proxyBeanMethods = false)
public class RabbitConfiguration {

    @Bean
    public Jackson2JsonMessageConverter messageConverter(){
        return new Jackson2JsonMessageConverter();
    }
}
