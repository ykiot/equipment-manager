package com.yk.common.web.jackson;


import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import java.util.Date;

/**
 * 自定义的Json模块，继承自SimpleModule类
 *
 * @author lmx
 * @date 2023/11/3 9:52
 */
public class CustomJsonModule extends SimpleModule {
    public CustomJsonModule() {
        this.addSerializer(Long.class, ToStringSerializer.instance);
        this.addDeserializer(Date.class, new DateDeserializer());
        this.addSerializer(Date.class, new DateSerializer());
    }
}
