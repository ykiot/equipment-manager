package com.yk.common.web.jackson;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Date;

/**
 * 自定义的日期反序列化器
 *
 * @author lmx
 * @date 2023/11/3 9:55
 */
public class DateDeserializer extends JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        if (StrUtil.isBlank(p.getValueAsString())) {
            return null;
        }
        return DateUtil.parseDateTime(p.getValueAsString());
    }
}
