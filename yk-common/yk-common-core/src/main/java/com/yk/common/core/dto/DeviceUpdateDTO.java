package com.yk.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 设备更新信息DTO
 *
 * @author lmx
 * @date 2023/11/23 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DeviceUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备id
     */
    private Long id;

    /**
     * 设备状态
     */
    private Integer status;
}
