package com.yk.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 发送微信公众号
 *
 * @author lmx
 * @date 2023/11/23 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendWechatDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * type 1-登录成功 2-流量卡报警推送 3-设备报警推送
     */
    private String type;

    /**
     * 物联网卡号
     */
    private String cardno;

    /**
     * 使用流量(-1表示未知，单位KB，时间当月)
     */
    private String used;

    /**
     * 剩余流量(单位KB)
     */
    private String surplus;

    /**
     * 设备名称
     */
    private String name;

    /**
     * 告警原因
     */
    private String msg;

    /**
     * 告警值
     */
    private String value;

    /**
     * 地址
     */
    private String address;

    /**
     * openId
     */
    private String openId;

    /**
     * userId
     */
    private Long userId;
}
