package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 用户状态
 *
 * @author lmx
 * @date 2023/10/18 17:03
 */
@Getter
@AllArgsConstructor
public enum UserStatus {
    /**
     * 状态
     */
    OK("0", "正常"),
    DISABLE("1", "停用"),
    DELETED("2", "删除");

    private final String code;
    private final String info;

}
