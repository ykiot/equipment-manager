package com.yk.common.core.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 分页响应信息主体
 *
 * @author lmx
 * @date 2023/10/17 10:13
 */
@Data
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 成功
     */
    public static final int SUCCESS = 200;
    /**
     * 失败
     */
    public static final int FAIL = 500;
    /**
     * 总条数
     */
    private long total;
    /**
     * 数据
     */
    private List<T> records;
    /**
     * 消息状态码
     */
    private int code;
    /**
     * 消息内容
     */
    private String msg;

    public static <T> PageResult<T> success(List<T> records, long total) {
        PageResult<T> pageResult = new PageResult<>();
        pageResult.setRecords(records);
        pageResult.setTotal(total);
        pageResult.setCode(SUCCESS);
        pageResult.setMsg("操作成功");
        return pageResult;
    }

    public static <T> PageResult<T> fail(String msg) {
        PageResult<T> pageResult = new PageResult<>();
        pageResult.setTotal(0L);
        pageResult.setCode(FAIL);
        pageResult.setMsg(msg);
        return pageResult;
    }
}
