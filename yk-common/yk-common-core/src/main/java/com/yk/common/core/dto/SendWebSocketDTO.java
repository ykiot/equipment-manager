package com.yk.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 发送websocketDTO
 *
 * @author lmx
 * @date 2023/11/23 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendWebSocketDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    private String id;

    /**
     * 消息
     */
    private String msg;
}
