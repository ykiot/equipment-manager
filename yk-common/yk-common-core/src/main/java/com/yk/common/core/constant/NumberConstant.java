package com.yk.common.core.constant;

/**
 * 数字常量
 *
 * @author lmx
 * @date 2023/10/9 17:56
 */
public interface NumberConstant {
    Integer ZERO = 0;
    Integer ONE = 1;
    Integer TWO = 2;
    Integer THREE = 3;
    Integer FOUR = 4;
    Integer FIVE = 5;
    Integer SIX = 6;
    Integer SEVEN = 7;
    Integer EIGHT = 8;
    Integer NINE = 9;
    Integer TEN = 10;

    String ZERO_STR = "0";
    String ONE_STR = "1";
    String TWO_STR = "2";
    String THREE_STR = "3";
    String FOUR_STR = "4";
    String FIVE_STR = "5";
    String SIX_STR = "6";
    String TWO_HUNDRED_STR = "200";
}
