package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 登录类型
 *
 * @author lmx
 * @date 2023/10/18 17:03
 */
@Getter
@AllArgsConstructor
public enum LoginTypeEnum {

    /**
     * 密码登录
     */
    PASSWORD("密码输入错误{0}次，帐户锁定{1}分钟", "密码输入错误{0}次"),

    /**
     * 短信登录
     */
    SMS("短信验证码输入错误{0}次，帐户锁定{1}分钟", "短信验证码输入错误{0}次"),

    /**
     * 小程序登录
     */
    XCX("", "");

    /**
     * 登录重试超出限制提示
     */
    final String retryLimitExceed;

    /**
     * 登录重试限制计数提示
     */
    final String retryLimitCount;
}
