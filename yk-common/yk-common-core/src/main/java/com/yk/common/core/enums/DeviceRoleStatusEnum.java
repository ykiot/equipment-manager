package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 设备共享类型
 *
 * @author lmx
 * @date 2023/11/8 10:13
 */
@Getter
@AllArgsConstructor
public enum DeviceRoleStatusEnum {

    /**
     * 未知
     */
    UNKNOWN("-1", "未知"),

    /**
     * 共享中
     */
    SHARING("0", "共享中"),

    /**
     * 已失效
     */
    LOSE_EFFICACY("1", "已失效");

    private final String code;
    private final String info;

    /**
     * 根据code获取
     */
    public static String getCode(String code) {
        for (DeviceRoleStatusEnum value : values()) {
            if (Objects.equals(value.getCode(), code)) {
                return value.getInfo();
            }
        }
        return UNKNOWN.getInfo();
    }
}
