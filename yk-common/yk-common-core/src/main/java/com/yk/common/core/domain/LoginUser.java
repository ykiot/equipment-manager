package com.yk.common.core.domain;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 用户信息
 *
 * @author lmx
 * @date 2023/10/17 10:13
 */
@Data
@NoArgsConstructor
public class LoginUser implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 机构ID
     */
    private Long orgId;

    /**
     * 用户唯一标识
     */
    private String token;

    /**
     * 菜单权限
     */
    private List<String> menuPermission;

    /**
     * 角色权限
     */
    private List<String> rolePermission;

    /**
     * 用户名
     */
    private String username;

    /**
     * 姓名
     */
    private String nickname;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像
     */
    private String headUrl;

    /**
     * 手机号码
     */
    private String phone;
}
