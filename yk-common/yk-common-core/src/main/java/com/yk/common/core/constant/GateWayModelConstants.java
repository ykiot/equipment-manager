package com.yk.common.core.constant;

/**
 * 网关类型常量
 *
 * @author lmx
 * @date 2023/10/20 17:36
 */
public interface GateWayModelConstants {

    String S702 = "YK-MT2-S702";
    String MT02 = "YK-MT2-MT02";
    String AM02 = "YK-DT2-AM02";
    String VM04 = "YK-MT2-VM04";
    String VM06 = "YK-DT2-VM06";
}