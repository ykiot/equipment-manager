package com.yk.common.core.enums;

import cn.hutool.core.collection.ListUtil;
import com.yk.common.core.exception.ServiceException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;
import java.util.Objects;

/**
 * 设备共享权限类型
 *
 * @author lmx
 * @date 2023/11/8 10:13
 */
@Getter
@AllArgsConstructor
public enum RoleTypeEnum {

    /**
     * 未知
     */
    UNKNOWN("-1", "未知"),

    /**
     * 仅查看
     */
    VIEW("0", "仅查看"),

    /**
     * 允许控制
     */
    CONTROL("1", "允许控制"),

    /**
     * 可管理
     */
    MANAGE("3", "可管理"),

    /**
     * 管理员 设备创建人
     */
    ADMIN("4", "管理员");

    private final String code;
    private final String info;

    /**
     * 根据code获取
     */
    public static RoleTypeEnum getCode(String code) {
        for (RoleTypeEnum value : values()) {
            if (Objects.equals(value.getCode(), code)) {
                return value;
            }
        }
        return UNKNOWN;
    }

    /**
     * 检验权限
     */
    public static void checkDeviceRole(DeviceControlsTypeEnum typeEnum, String roleType) {
        switch (typeEnum) {
            case SHARE:
            case UPDATE:
                if (!MANAGE.code.equals(roleType) && !ADMIN.code.equals(roleType)){
                    throw new ServiceException("您没有权限操作该设备");
                }
                break;
            case DELETE:
                if (!ADMIN.code.equals(roleType)){
                    throw new ServiceException("您没有权限操作该设备");
                }
                break;
            case CONTROL:
                if (!MANAGE.code.equals(roleType) && !ADMIN.code.equals(roleType) && !CONTROL.code.equals(roleType)){
                    throw new ServiceException("您没有权限操作该设备");
                }
                break;
            default:
        }
    }

    /**
     * 设备分享列表查询条件
     */
    public static List<String> getConditions() {
        return ListUtil.of(VIEW.getCode(), CONTROL.getCode(), MANAGE.getCode());
    }
}
