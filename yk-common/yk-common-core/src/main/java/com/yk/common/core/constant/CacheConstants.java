package com.yk.common.core.constant;


/**
 * 缓存常量信息
 *
 * @author lmx
 * @date 2023/10/18 17:00
 */
public interface CacheConstants {

    /**
     * 登录账户密码错误次数 redis key
     */
    String PWD_ERR_CNT_KEY = "pwd_err_cnt:";

    /**
     * 短信登录验证码
     */
    String LOGIN_SMS_VERIFICATION_CODE = "LOGIN_SMS_VERIFICATION_CODE:";

    /**
     * 短信注册验证码
     */
    String SIGN_SMS_VERIFICATION_CODE = "SIGN_SMS_VERIFICATION_CODE:";

    /**
     * 短信忘记密码验证码
     */
    String FORGET_SMS_VERIFICATION_CODE = "FORGET_SMS_VERIFICATION_CODE:";

    /**
     * 短信修改手机号验证码
     */
    String UPDATE_PHONE_SMS_VERIFICATION_CODE = "UPDATE_PHONE_SMS_VERIFICATION_CODE:";

    /**
     * 短信修改密码验证码
     */
    String UPDATE_PASSWORD_SMS_VERIFICATION_CODE = "UPDATE_PASSWORD_SMS_VERIFICATION_CODE:";

    /**
     * 公众号注册
     */
    String GZH_SMS_VERIFICATION_CODE = "GZH_SMS_VERIFICATION_CODE:";

    /**
     * 小程序登录
     */
    String XCX_SIGN_SMS_VERIFICATION_CODE = "XCX_SIGN_SMS_VERIFICATION_CODE:";

    /**
     * 萤石云token
     */
    String EZVIZ_TOKEN_KEY = "EZVIZ_TOKEN_KEY:";

    /**
     * tdengine 超极表字段信息
     */
    String SUPER_TABLE_FIELD_KEY = "SUPER_TABLE_FIELD_KEY:";

    /**
     * 设备实时数据key
     */
    String DEVICE_REAL_DATA_KEY = "DEVICE_REAL_DATA_KEY:";

    /**
     * gateway cache key
     */
    String GATEWAY_KEY = "GATEWAY_KEY:";

    /**
     * 报警规则 cache key
     */
    String ALARM_KEY = "ALARM_KEY:";

    /**
     * 设备控制 cache key
     */
    String DEVICE_CONTROL_KEY = "DEVICE_CONTROL_KEY:";

    /**
     * 设备复位控制 过期cache key
     */
    String EXPIRE_DEVICE_RESET_CONTROL_KEY = "EXPIRE_DEVICE_RESET_CONTROL_KEY:";

    /**
     * 设备复位控制 cache key
     */
    String DEVICE_RESET_CONTROL_KEY = "DEVICE_RESET_CONTROL_KEY:";

    /**
     * 过期key
     */
    String EXPIRED_TIME_KEY = "EXPIRED_TIME_KEY:";

    /**
     * 设备分享过期时间
     */
    String DEVICE_ROLE_EXPIRED_TIME_KEY = "DEVICE_ROLE_EXPIRED_TIME_KEY:";

    /**
     * 组态分享过期时间
     */
    String CONFIGURATION_ROLE_EXPIRED_TIME_KEY = "CONFIGURATION_ROLE_EXPIRED_TIME_KEY:";

    /**
     * 系统通知过期时间
     */
    String NOTIFICATION_EXPIRED_TIME_KEY = "NOTIFICATION_EXPIRED_TIME_KEY:";

    /**
     * 变量当前实时数据key
     */
    String REAL_TIME_VARIABLE_DATA = "REAL_TIME_VARIABLE_DATA:";

    /**
     * 变量当日第一条数据key
     */
    String DAY_VARIABLE_DATA = "DAY_VARIABLE_DATA:";

    /**
     * 变量当周第一条数据key
     */
    String WEEK_VARIABLE_DAY_DATA = "WEEK_VARIABLE_DAY_DATA:";

    /**
     * 变量当月第一条数据key
     */
    String MONTH_VARIABLE_DAY_DATA = "MONTH_VARIABLE_DAY_DATA:";

    /**
     * 历史数据条数是否达到阈值
     */
    String DEVICE_HISTORY_KEY = "DEVICE_HISTORY_KEY:";

    /**
     * API参数secretKey
     */
    String API_SECRET_KEY = "API_SECRET_KEY:";
}
