package com.yk.common.core.constant;


/**
 * 返回状态码
 *
 * @author lmx
 * @date 2023/10/18 17:00
 */
public interface HttpCodeStatus {

    /**
     * 未授权
     */
    int UNAUTHORIZED = 401;

    /**
     * 访问受限，授权过期
     */
    int FORBIDDEN = 403;

    /**
     * 登录已被顶下线
     */
    int LINE = 602;
}
