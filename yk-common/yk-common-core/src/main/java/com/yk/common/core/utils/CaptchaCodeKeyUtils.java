package com.yk.common.core.utils;

import com.yk.common.core.constant.CacheConstants;
import com.yk.common.core.constant.NumberConstant;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * 获取短信key 前缀
 *
 * @author lmx
 * @date 2023/10/18 17:08
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CaptchaCodeKeyUtils {

    /**
     * 根据类型获取短信key
     *
     * @param type 类型 0-登录 1-注册 2-忘记密码 3-修改手机号 4-修改密码 5-公众号注册 6-小程序登录
     * @return
     */
    public static String getCaptchaKey(Integer type) {
        String key;
        if (type.equals(NumberConstant.ZERO)) {
            key = CacheConstants.LOGIN_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.ONE)) {
            key = CacheConstants.SIGN_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.TWO)) {
            key = CacheConstants.FORGET_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.THREE)) {
            key = CacheConstants.UPDATE_PHONE_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.FOUR)) {
            key = CacheConstants.UPDATE_PASSWORD_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.FIVE)) {
            key = CacheConstants.GZH_SMS_VERIFICATION_CODE;
        } else if (type.equals(NumberConstant.SIX)) {
            key = CacheConstants.XCX_SIGN_SMS_VERIFICATION_CODE;
        } else {
            return null;
        }
        return key;
    }
}
