package com.yk.common.core.exception.user;


import com.yk.common.core.exception.base.BaseException;


/**
 * 用户信息异常类
 *
 * @author lmx
 * @date 2023/10/18 17:04
 */
public class UserException extends BaseException {
    private static final long serialVersionUID = 1L;

    public UserException(String defaultMessage) {
        super(defaultMessage);
    }
}
