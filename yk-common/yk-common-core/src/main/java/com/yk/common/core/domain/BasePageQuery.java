package com.yk.common.core.domain;

import lombok.Data;

import java.io.Serializable;

/**
 * 分页查询参数
 *
 * @author lmx
 * @date 2023/10/17 10:14
 */
@Data
public class BasePageQuery<T> implements Serializable {

    private Long pageNum;

    private Long pageSize;

    private T param;

}
