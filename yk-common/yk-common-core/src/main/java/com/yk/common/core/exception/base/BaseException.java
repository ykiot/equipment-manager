package com.yk.common.core.exception.base;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 基础异常
 *
 * @author lmx
 * @date 2023/10/18 16:58
 */
@Getter
@AllArgsConstructor
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误码对应的参数
     */
    private Object[] args;

    /**
     * 错误消息
     */
    private String defaultMessage;

    public BaseException(String defaultMessage) {
        this(null, null, null, defaultMessage);
    }
}
