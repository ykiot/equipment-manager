package com.yk.common.core.listener;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.serviceregistry.AbstractAutoServiceRegistration;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * nacos 服务下线处理
 *
 * @author lmx
 * @date 2023/11/3 10:51
 */
@Slf4j
@Component
public class SelfNacosDiscovery implements ApplicationListener<ContextClosedEvent> {

    @Resource
    private AbstractAutoServiceRegistration abstractAutoServiceRegistration;

    @Override
    public void onApplicationEvent(ContextClosedEvent event) {
        log.info("nacos  补偿注销流程开始");
        try {
            abstractAutoServiceRegistration.destroy();
        } catch (Exception e) {
            log.error("SelfNacosDiscovery error");
        }
        log.info("nacos  补偿注销流程结束");
    }
}
