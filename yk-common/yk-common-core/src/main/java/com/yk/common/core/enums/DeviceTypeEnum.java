package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 设备类型
 *
 * @author lmx
 * @date 2023/10/18 17:03
 */
@Getter
@AllArgsConstructor
public enum DeviceTypeEnum {

    /**
     * pc端
     */
    PC("pc"),

    /**
     * app端
     */
    APP("app"),

    /**
     * 小程序端
     */
    XCX("xcx");

    private final String device;
}
