package com.yk.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.LinkedHashMap;

/**
 * 短信发送DTO
 *
 * @author lmx
 * @date 2023/11/23 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SendAlibabaSmsDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 手机号
     */
    private String phone;

    /**
     * 验证码
     */
    private String content;

    /**
     * 模板ID
     */
    private String templateId;

    /**
     * 封装参数
     */
    LinkedHashMap<String, String> map;
}
