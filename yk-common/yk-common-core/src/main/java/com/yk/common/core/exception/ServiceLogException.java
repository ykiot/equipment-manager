package com.yk.common.core.exception;


import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 业务异常
 *
 * @author lmx
 * @date 2023/10/18 17:05
 */
@Getter
@NoArgsConstructor
public final class ServiceLogException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误日志
     */
    private String logMessage;

    public ServiceLogException(String message, String logMessage) {
        this.logMessage = logMessage;
        this.message = message;
    }
}

