package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * 设备操作类型
 *
 * @author lmx
 * @date 2023/11/8 10:13
 */
@Getter
@AllArgsConstructor
public enum DeviceControlsTypeEnum {

    /**
     * 分享
     */
    SHARE("0"),

    /**
     * 删除
     */
    DELETE("1"),

    /**
     * 修改
     */
    UPDATE("2"),

    /**
     * 控制
     */
    CONTROL("3"),

    /**
     * 查看
     */
    VIEW("4");

    private final String code;

    /**
     * 根据code获取
     */
    public static DeviceControlsTypeEnum getCode(String code) {
        for (DeviceControlsTypeEnum value : values()) {
            if (Objects.equals(value.getCode(), code)) {
                return value;
            }
        }
        return VIEW;
    }
}
