package com.yk.common.core.constant;

/**
 * mqtt订阅常量
 *
 * @author lmx
 * @date 2023/10/20 17:36
 */
public interface MqttConstants {

    /**
     * 主题尾缀-在线
     */
    String ONLINE = "online";

    /**
     * 离线
     */
    String OFFLINE = "offline";

    /**
     * 主题尾缀-读取数据值
     */
    String VAR_READ = "var_read";

    /**
     * 主题尾缀-读取数据值回调
     */
    String VAR_READ_ACK = "var_read_ack";

    /**
     * 主题尾缀-写入数据值
     */
    String VAR_WRITE = "var_write";

    /**
     * 主题尾缀-写入数据值回调
     */
    String WRITE_ACK = "var_write_ack";

    /**
     * 客户端ID
     */
    String CLIENT_ID = "clientid";

    /**
     * 消息ID
     */
    String MESSAGE_ID = "messageId";

    /**
     * 判断常量
     */
    String TRUE = "true";

    /**
     * 判断常量
     */
    String FALSE = "false";

    /**
     * 数据
     */
    String DATA = "data";

    /**
     * 物联网卡
     */
    String CC_ID = "ccid";

    /**
     * 空值
     */
    String NAN = "NaN";

    /**
     * 控制回调成功
     */
    String SUCCESS = "success";

    /**
     * 控制回调数据key
     */
    String RESULT = "result";

}
