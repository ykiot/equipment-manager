package com.yk.common.core.utils;

import com.yk.common.core.dto.SendAlibabaSmsDTO;
import com.yk.common.core.dto.SendWebSocketDTO;
import com.yk.common.core.dto.SendWechatDTO;
import com.yk.common.rabbitmq.constant.QueueConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.LinkedHashMap;

/**
 * 系统发送工具类
 *
 * @author lmx
 * @date 2023/11/23 14:41
 */
@Slf4j
@Component
public class SendUtils {

    @Resource
    private AmqpTemplate amqpTemplate;

    /**
     * 发送Alibaba短信
     */
    public void sendAlibabaSms(String phone, String content, String templateId) {
        amqpTemplate.convertAndSend(QueueConstants.SEND_SMS, new SendAlibabaSmsDTO(phone, content, templateId, null));
    }

    /**
     * 发送Alibaba短信报警
     */
    public void sendAlibabaAlarmSms(String phone, LinkedHashMap<String, String> map, String templateId) {
        amqpTemplate.convertAndSend(QueueConstants.SEND_ALARM_SMS, new SendAlibabaSmsDTO(phone, null, templateId, map));
    }

    /**
     * 发送Websocket消息
     */
    public void sendWebsocketMessage(String id ,String message) {
        amqpTemplate.convertAndSend(QueueConstants.SEND_WEBSOCKET, new SendWebSocketDTO(id, message));
    }

    /**
     * 发送微信
     */
    public void sendWechatMessage(SendWechatDTO dto) {
        amqpTemplate.convertAndSend(QueueConstants.SEND_WECHAT, dto);
    }

}
