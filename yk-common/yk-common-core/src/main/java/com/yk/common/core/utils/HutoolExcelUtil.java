package com.yk.common.core.utils;

import cn.hutool.poi.excel.ExcelWriter;
import cn.hutool.poi.excel.StyleSet;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.SheetUtil;

/**
 * hutool excel工具类
 *
 * @author lmx
 * @date 2023/11/17 16:43
 */
public class HutoolExcelUtil {

    /**
     * 样式设置
     */
    public static void setBaseStyle(ExcelWriter writer) {
        //表头设置字体
        CellStyle headCellStyle = writer.getHeadCellStyle();
        Font font = createFont(writer, true, false, "宋体", 18);
        headCellStyle.setFont(font);
        //普通单元格，全局样式设置
        Font font1 = createFont(writer, false, false, "宋体", 14);
        setBaseGlobalStyle(writer, font1);
        //调整自适应宽度
        adaptiveWidth(writer);
    }

    /**
     * 创建字体
     *
     * @param writer   写入对象
     * @param bold     是否加粗
     * @param italic   是否斜体
     * @param fontName 字体名称
     * @param fontSize 字体大小
     * @return
     */
    private static Font createFont(ExcelWriter writer, boolean bold, boolean italic, String fontName, int fontSize) {
        Font font = writer.getWorkbook().createFont();
        //设置字体名称 宋体 / 微软雅黑 /等
        font.setFontName(fontName);
        //设置是否斜体
        font.setItalic(italic);
        //设置字体大小 以磅为单位
        font.setFontHeightInPoints((short) fontSize);
        //设置是否加粗
        font.setBold(bold);
        return font;
    }

    /**
     * 设置全局样式
     *
     * @param writer 写入对象
     * @param font   字体
     * @return
     */
    private static StyleSet setBaseGlobalStyle(ExcelWriter writer, Font font) {
        //全局样式设置
        StyleSet styleSet = writer.getStyleSet();
        //设置全局文本居中
        styleSet.setAlign(HorizontalAlignment.CENTER, VerticalAlignment.CENTER);
        //设置全局字体样式
        styleSet.setFont(font, true);
        //设置背景颜色 第二个参数表示是否将样式应用到头部
        styleSet.setBackgroundColor(IndexedColors.WHITE, false);
        //设置自动换行 当文本长于单元格宽度是否换行
        styleSet.setWrapText();
        // 设置全局边框样式
        styleSet.setBorder(BorderStyle.THIN, IndexedColors.BLACK);
        return styleSet;
    }

    /**
     * 调整自适应宽度
     */
    private static void adaptiveWidth(ExcelWriter writer) {
        int columnCount = writer.getColumnCount();
        for (int i = 0; i < columnCount; ++i) {
            double width = SheetUtil.getColumnWidth(writer.getSheet(), i, false);
            if (width != -1.0D) {
                width *= 256.0D;
                //此处可以适当调整，调整列空白处宽度
                width += 250D;
                writer.setColumnWidth(i, Math.toIntExact(Math.round(width / 256D)));
            }
        }
    }
}
