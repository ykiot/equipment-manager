package com.yk.common.core.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Objects;

/**
 * AliYunSms短信错误码
 *
 * @author lmx
 * @date 2023/10/18 15:45
 */
@Getter
@AllArgsConstructor
public enum AliYunSmsCodeEnum {

    /**
     * 未知
     */
    unknown("unknown", "发送失败"),

    /**
     * 成功
     */
    OK("OK", "成功"),

    /**
     * 签名和模板类型不一致
     */
    SMS_SIGNATURE_SCENE_ILLEGAL("isv.SMS_SIGNATURE_SCENE_ILLEGAL", "签名和模板类型不一致"),

    /**
     * 扩展码使用错误，相同的扩展码不可用于多个签名
     */
    EXTEND_CODE_ERROR("isv.EXTEND_CODE_ERROR", "扩展码使用错误，相同的扩展码不可用于多个签名"),

    /**
     * 国际/港澳台消息模板不支持发送境内号码
     */
    DOMESTIC_NUMBER_NOT_SUPPORTED("isv.DOMESTIC_NUMBER_NOT_SUPPORTED", "国际/港澳台消息模板不支持发送境内号码"),

    /**
     * 源IP地址所在的地区被禁用
     */
    DENY_IP_RANGE("isv.DENY_IP_RANGE", "源IP地址所在的地区被禁用"),

    /**
     * 触发日发送限额
     */
    DAY_LIMIT_CONTROL("isv.DAY_LIMIT_CONTROL", "触发日发送限额"),

    /**
     * 触发月发送限额
     */
    MONTH_LIMIT_CONTROL("isv.MONTH_LIMIT_CONTROL", "触发月发送限额"),

    /**
     * 短信内容包含禁止发送内容
     */
    SMS_CONTENT_ILLEGAL("isv.SMS_CONTENT_ILLEGAL", "短信内容包含禁止发送内容"),

    /**
     * 业务停机
     */
    OUT_OF_SERVICE("isv.OUT_OF_SERVICE", "业务停机"),

    /**
     *
     * 账户异常
     */
    ACCOUNT_ABNORMAL("isv.ACCOUNT_ABNORMAL", "账户异常"),

    /**
     * 短信内容和模板属性不匹配
     */
    SMS_TEMPLATE_ILLEGAL("isv.SMS_TEMPLATE_ILLEGAL", "短信内容和模板属性不匹配"),

    /**
     * 参数格式不正确
     */
    INVALID_PARAMETERS("isv.INVALID_PARAMETERS", "参数格式不正确"),

    /**
     *
     * 系统出现错误，请重新调用
     */
    SYSTEM_ERROR("isv.SYSTEM_ERROR", "发送失败"),

    /**
     * 手机号码格式错误
     */
    MOBILE_NUMBER_ILLEGAL("isv.MOBILE_NUMBER_ILLEGAL", "手机号码格式错误"),

    /**
     * 手机号码数量超过限制，最多支持1000条
     */
    MOBILE_COUNT_OVER_LIMIT("isv.MOBILE_COUNT_OVER_LIMIT", "发送失败"),

    /**
     * 触发云通信流控限制
     */
    BUSINESS_LIMIT_CONTROL("isv.BUSINESS_LIMIT_CONTROL", "请60秒后重试"),

    /**
     * 黑名单管控
     */
    BLACK_KEY_CONTROL_LIMIT("isv.BLACK_KEY_CONTROL_LIMIT", "发送失败"),

    /**
     * 账户余额不足
     */
    AMOUNT_NOT_ENOUGH("isv.AMOUNT_NOT_ENOUGH", "请60秒后重试"),

    /**
     * 不支持的短信内容
     */
    UNSUPPORTED_CONTENT("isv.UNSUPPORTED_CONTENT", "不支持的短信内容"),
    ;

    private final String code;
    private final String errMsg;

    /**
     * 根据code获取
     */
    public static AliYunSmsCodeEnum getCodeEnum(String code) {
        for (AliYunSmsCodeEnum value : values()) {
            if (Objects.equals(value.getCode(), code)) {
                return value;
            }
        }
        return unknown;
    }
}
