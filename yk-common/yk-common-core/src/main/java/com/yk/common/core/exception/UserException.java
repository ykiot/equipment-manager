package com.yk.common.core.exception;


import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * 用户信息异常类
 *
 * @author lmx
 * @date 2023/10/18 17:04
 */
@Getter
@AllArgsConstructor
public class UserException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    /**
     * 所属模块
     */
    private String module;

    /**
     * 错误码
     */
    private String code;

    /**
     * 错误码对应的参数
     */
    private Object[] args;

    /**
     * 错误消息
     */
    private String defaultMessage;
}
