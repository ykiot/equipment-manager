package com.yk.common.core.constant;

/**
 * 设备常量类型常量
 *
 * @author lmx
 * @date 2023/10/20 17:36
 */
public interface DeviceTypeConstants {

    /**
     * 设备列表
     */
    String DEVICES = "devices";

    /**
     * 设备ID
     */
    String DEVICE_ID = "deviceId";

    /**
     * 模版id
     */
    String TEMP_ID = "tempId";

    /**
     * 设备编号
     */
    String DEVICE_NUM = "deviceNumber";

    /**
     * 设备状态
     */
    String DEVICE_STATE = "deviceState";

    /**
     * 设备数据
     */
    String DEVICE_DATA = "deviceData";
}