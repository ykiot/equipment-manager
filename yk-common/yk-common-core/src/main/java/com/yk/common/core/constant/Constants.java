package com.yk.common.core.constant;


/**
 * 通用常量信息
 *
 * @author lmx
 * @date 2023/10/18 17:00
 */
public interface Constants {
    /**
     * UTF-8 字符集
     */
    String UTF8 = "UTF-8";

    /**
     * 登录成功状态
     */
    String LOGIN_SUCCESS_STATUS = "0";

    /**
     * 登录失败状态
     */
    String LOGIN_FAIL_STATUS = "1";

    /**
     * 登录成功
     */
    String LOGIN_SUCCESS = "Success";

    /**
     * 注销
     */
    String LOGOUT = "Logout";

    /**
     * 注册
     */
    String REGISTER = "Register";

    /**
     * 登录失败
     */
    String LOGIN_FAIL = "Error";

    /**
     * 验证码有效期（分钟）
     */
    long CAPTCHA_EXPIRATION = 5;

    /**
     * xls
     */
    String XLS = "xls";

    /**
     * xlsx
     */
    String XLSX = "xlsx";
}
