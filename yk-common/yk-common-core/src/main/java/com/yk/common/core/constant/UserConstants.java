package com.yk.common.core.constant;


/**
 * 用户常量信息
 *
 * @author lmx
 * @date 2023/10/18 17:01
 */
public interface UserConstants {

    /**
     * 正常状态
     */
    String NORMAL = "0";

    /**
     * 异常状态
     */
    String EXCEPTION = "1";

    /**
     * 用户正常状态
     */
    String USER_NORMAL = "0";

    /**
     * 用户封禁状态
     */
    String USER_DISABLE = "1";

    /**
     * 角色正常状态
     */
    String ROLE_NORMAL = "0";

    /**
     * 角色封禁状态
     */
    String ROLE_DISABLE = "1";

    /**
     * 部门正常状态
     */
    String DEPT_NORMAL = "0";

    /**
     * 部门停用状态
     */
    String DEPT_DISABLE = "1";

    /**
     * 岗位正常状态
     */
    String POST_NORMAL = "0";

    /**
     * 岗位停用状态
     */
    String POST_DISABLE = "1";

    /**
     * 管理员ID
     */
    Long ADMIN_ID = 1L;
}
