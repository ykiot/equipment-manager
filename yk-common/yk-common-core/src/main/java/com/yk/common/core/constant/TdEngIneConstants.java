package com.yk.common.core.constant;


/**
 * Tdengine常量信息
 *
 * @author lmx
 * @date 2023/10/18 17:00
 */
public interface TdEngIneConstants {

    /**
     * TDengine 超级表前缀
     */
    String STABLE_PREFIX = "st_";

    /**
     * TDengine 子表前缀
     */
    String TABLE_PREFIX = "t_";

    /**
     * 主键id 时间戳
     */
    String TS = "ts";

    /**
     * 设备编号（TAG）
     */
    String DEVICE_NUM = "device_number";

    /**
     * 设备id（TAG）
     */
    String DEVICE_ID = "device_id";

    /**
     * 备份表
     */
    String DEVICE_COPY = "_copy";
}
