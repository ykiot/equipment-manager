package com.yk.common.core.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 网关更新信息DTO
 *
 * @author lmx
 * @date 2023/11/23 15:11
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GateWayUpdateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 网关id
     */
    private Long id;

    /**
     * 网关状态
     */
    private Integer status;

    /**
     * 流量卡号
     */
    private String ccId;

    /**
     * 客户端id
     */
    private String clientId;

    public GateWayUpdateDTO(String clientId) {
        this.clientId = clientId;
    }

    public GateWayUpdateDTO(Long id, String clientId) {
        this.id = id;
        this.clientId = clientId;
    }
}
