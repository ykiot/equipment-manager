package com.yk.common.core.utils;

import cn.hutool.json.JSONObject;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import java.security.Security;

/**
 * 微信解析手机号
 *
 * @author lmx
 * @date 2023/11/29 15:15
 */
public class WechatDecryptDataUtil {

    /**
     * 解析电话号码
     *
     * @param session_key
     * @param encryptedData
     * @param iv
     * @return
     */
    public static JSONObject getPhoneNumber(String session_key, String encryptedData, String iv) {
        System.out.println(session_key);
        // 这个是为了前端传过来可能会有转义字符变成空所以替换
        String s = encryptedData.replaceAll(" ", "+");
        byte[] dataByte = org.bouncycastle.util.encoders.Base64.decode(s);
        // 加密秘钥
        byte[] keyByte = org.bouncycastle.util.encoders.Base64.decode(session_key);
        // 偏移量
        byte[] ivByte = org.bouncycastle.util.encoders.Base64.decode(iv);
        try {
            // 如果密钥不足16位，那么就补足.  这个if 中的内容很重要
            int base = 16;
            if (keyByte.length % base != 0) {
                int groups = keyByte.length / base + (keyByte.length % base != 0 ? 1 : 0);
                byte[] temp = new byte[groups * base];
                Arrays.fill(temp, (byte) 0);
                System.arraycopy(keyByte, 0, temp, 0, keyByte.length);
                keyByte = temp;
            }
            // 初始化
            Security.addProvider(new BouncyCastleProvider());
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding", "BC");
            SecretKeySpec spec = new SecretKeySpec(keyByte, "AES");
            AlgorithmParameters parameters = AlgorithmParameters.getInstance("AES");
            parameters.init(new IvParameterSpec(ivByte));
            // 初始化
            cipher.init(Cipher.DECRYPT_MODE, spec, parameters);
            byte[] resultByte = cipher.doFinal(dataByte);
            if (null != resultByte && resultByte.length > 0) {
                String result = new String(resultByte, "UTF-8");
                return new JSONObject(result);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

