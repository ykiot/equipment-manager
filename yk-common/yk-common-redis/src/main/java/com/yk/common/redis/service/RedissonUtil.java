package com.yk.common.redis.service;

import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * redisson工具类
 *
 * @author lmx
 * @date 2024/1/9 15:26
 */
@Component
public class RedissonUtil {

    @Resource
    private RedissonClient redissonClient;


    /**
     * timeout为加锁时间，时间单位由unit确定
     *
     * @param lockKey 锁名
     * @param unit    时间单位
     * @param timeout 过期时间
     * @return org.redisson.api.RLock
     */
    public RLock lock(String lockKey, TimeUnit unit, long timeout) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.lock(timeout, unit);
        return lock;
    }

    /**
     * 通过lockKey解锁
     *
     * @param lockKey 锁名
     */
    public void unlock(String lockKey) {
        RLock lock = redissonClient.getLock(lockKey);
        lock.unlock();
    }

    /**
     * 直接通过锁解锁
     *
     * @param lock 加锁时 返回的锁
     */
    public void unlock(RLock lock) {
        lock.unlock();
    }

}
