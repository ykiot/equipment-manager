package com.yk.common.excel.core;

import lombok.Data;

import java.io.Serializable;
import java.util.List;


/**
 * 默认excel返回对象
 *
 * @author lmx
 * @date 2023/10/25 11:23
 */
@Data
public class ExcelResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 文件名字
     */
    private String fileName;

    /**
     * 数据和分析结果
     */
    private List<ExcelRecord<T>> list;
}
