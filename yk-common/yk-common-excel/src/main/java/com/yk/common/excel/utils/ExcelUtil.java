package com.yk.common.excel.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.write.builder.ExcelWriterSheetBuilder;
import com.yk.common.core.utils.file.FileUtils;
import com.yk.common.excel.core.CheckService;
import com.yk.common.excel.core.DefaultExcelListener;
import com.yk.common.excel.core.ExcelResult;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.List;


/**
 * Excel相关处理
 *
 * @author lmx
 * @date 2023/10/25 11:08
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ExcelUtil {

    /**
     * 同步导入(适用于小数据量)
     *
     * @param is 输入流
     * @return 转换后集合
     */
    public static <T> List<T> importExcel(InputStream is, Class<T> clazz) {
        return EasyExcel.read(is).head(clazz).autoCloseStream(false).sheet().doReadSync();
    }

    /**
     * 使用校验监听器 异步导入 同步返回
     *
     * @param is           输入流
     * @param clazz        对象类型
     * @param checkService 业务检验
     * @return 转换后集合
     */
    public static <T> ExcelResult<T> importExcel(InputStream is, Class<T> clazz, CheckService<T> checkService) {
        DefaultExcelListener<T> listener = new DefaultExcelListener<>(checkService);
        EasyExcel.read(is, clazz, listener).sheet().doRead();
        return listener.getExcelResult();
    }

    /**
     * 导出excel模版
     *
     * @param list     导出数据集合
     * @param fileName 文件名
     * @param clazz    实体类
     * @param response 响应体
     */
    public static <T> void exportExcel(List<T> list, String fileName, Class<T> clazz, HttpServletResponse response) {
        try {
            resetResponse(fileName, response);
            ServletOutputStream os = response.getOutputStream();
            ExcelWriterSheetBuilder builder = EasyExcel.write(os, clazz)
                    .autoCloseStream(false)
                    // 自动适配
                    .registerWriteHandler(new LongestCellRowHandler())
                    .registerWriteHandler(new LongestCellWidthHandler())
                    .sheet(fileName);
            builder.doWrite(list);
        } catch (IOException e) {
            throw new RuntimeException("导出Excel异常");
        }
    }

    /**
     * 导出excel
     *
     * @param list      导出数据集合
     * @param sheetName 工作表的名称
     * @param fileName  文件名
     * @param clazz     实体类
     * @param response  响应体
     */
    public static <T> void exportExcel(List<T> list, String sheetName, String fileName, Class<T> clazz, HttpServletResponse response) {
        try {
            resetResponse(sheetName, fileName, response);
            ServletOutputStream os = response.getOutputStream();
            ExcelWriterSheetBuilder builder = EasyExcel.write(os, clazz)
                    .autoCloseStream(false)
                    // 自动适配
                    .registerWriteHandler(new LongestCellRowHandler())
                    .registerWriteHandler(new LongestCellWidthHandler())
                    .sheet(sheetName);
            builder.doWrite(list);
        } catch (IOException e) {
            throw new RuntimeException("导出Excel异常");
        }
    }

    /**
     * 导出excel-动态表头
     *
     * @param headList  导出数据表头
     * @param list      导出数据集合
     * @param sheetName 工作表的名称
     * @param fileName  文件名
     * @param response  响应体
     */
    public static <T> void exportDynamicHeadExcel(List<List<String>> headList, List<List<String>> list, String sheetName, String fileName, HttpServletResponse response) {
        try {
            resetResponse(sheetName, fileName, response);
            ServletOutputStream os = response.getOutputStream();
            ExcelWriterSheetBuilder builder = EasyExcel.write(os)
                    .head(headList)
                    .autoCloseStream(false)
                    .registerWriteHandler(new LongestCellRowHandler())
                    .registerWriteHandler(new LongestCellWidthHandler())
                    .sheet(sheetName);
            builder.doWrite(list);
        } catch (IOException e) {
            throw new RuntimeException("导出Excel异常");
        }
    }

    /**
     * 重置响应体
     *
     * @param fileName 文件名
     * @param response 响应体
     */
    public static void resetResponse(String fileName, HttpServletResponse response) throws UnsupportedEncodingException {
        String filename = encodingFilename(fileName);
        response.reset();
        FileUtils.setAttachmentResponseHeader(response, filename);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
    }

    /**
     * 重置响应体
     *
     * @param sheetName 工作表的名称
     * @param fileName  文件名
     * @param response  响应体
     */
    public static void resetResponse(String sheetName, String fileName, HttpServletResponse response) throws UnsupportedEncodingException {
        String filename = encodingFilename(sheetName, fileName);
        response.reset();
        FileUtils.setAttachmentResponseHeader(response, filename);
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8");
    }

    /**
     * 编码文件名
     */
    public static String encodingFilename(String filename) {
        return filename + ".xlsx";
    }

    /**
     * 编码文件名
     */
    public static String encodingFilename(String sheetName, String filename) {
        return filename + "_" + sheetName + ".xlsx";
    }

}
