package com.yk.common.excel.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 默认excel返回对象
 *
 * @author lmx
 * @date 2023/10/25 11:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ExcelRecord<T> implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 行标
     */
    private Integer rowIndex;

    /**
     * 数据
     */
    private T data;

    /**
     * 错误消息
     */
    private String errorMsg;

}
