package com.yk.common.excel.utils;

import com.alibaba.excel.write.style.row.AbstractRowHeightStyleStrategy;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

import java.util.Iterator;
import java.util.Objects;

/**
 * 高度样式
 *
 * @author lmx
 * @date 2023/10/25 11:04
 */
public class LongestCellRowHandler extends AbstractRowHeightStyleStrategy {
    /**
     * 默认高度
     */
    private static final Integer DEFAULT_HEIGHT = 100;

    @Override
    protected void setHeadColumnHeight(Row row, int i) {

    }

    @Override
    protected void setContentColumnHeight(Row row, int i) {
        Iterator<Cell> cellIterator = row.cellIterator();
        if (!cellIterator.hasNext()) {
            return;
        }
        int maxHeight = 3;
        while (cellIterator.hasNext()) {
            Cell cell = cellIterator.next();
            if (Objects.requireNonNull(cell.getCellType() == CellType.STRING)) {
                if (cell.getStringCellValue().contains("\n")) {
                    int length = cell.getStringCellValue().split("\n").length;
                    maxHeight = Math.max(maxHeight, length);
                }
            }
        }
        row.setHeight((short) (maxHeight * DEFAULT_HEIGHT));
    }
}
