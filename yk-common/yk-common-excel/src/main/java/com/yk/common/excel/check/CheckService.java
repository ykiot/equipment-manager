package com.yk.common.excel.check;

import com.yk.common.core.domain.Result;


/**
 * 具体业务检查
 *
 * @author lmx
 */
public interface CheckService<T> {

    /**
     * 检查数据
     *
     * @param data        数据
     * @return 验证结果
     */
    Result<T> check(T data);
}