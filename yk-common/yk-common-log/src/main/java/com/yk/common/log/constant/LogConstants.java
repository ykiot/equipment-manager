package com.yk.common.log.constant;


/**
 * 日志操作类型
 *
 * @author lmx
 * @date 2023/10/18 17:01
 */
public interface LogConstants {

    /**
     * 添加
     */
    String ADD = "添加-";

    /**
     * 删除
     */
    String DELETE = "删除-";

    /**
     * 修改
     */
    String UPDATE = "修改-";

    /**
     * 克隆
     */
    String COPY = "克隆-";

    /**
     * 转让
     */
    String TRANSFER = "转让-";

    /**
     * 下发控制
     */
    String CONTROL = "下发控制-";
}
