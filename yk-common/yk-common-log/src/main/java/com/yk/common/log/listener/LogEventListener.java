package com.yk.common.log.listener;

import com.yk.api.system.dto.LogininforDTO;
import com.yk.api.system.dto.OperLogDTO;
import com.yk.api.system.model.LogFeignService;
import com.yk.common.core.utils.BeanCopyUtils;
import com.yk.common.log.domain.LogininforEvent;
import com.yk.common.log.domain.OperLogEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;

/**
 * 异步调用日志服务
 *
 * @author lmx
 * @date 2023/11/6 9:57
 */
@Component
public class LogEventListener {

    @Resource
    private LogFeignService logFeignService;

    /**
     * 保存系统日志记录
     */
    @EventListener
    public void saveLog(OperLogEvent operLogEvent) {
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
        OperLogDTO dto = BeanCopyUtils.copy(operLogEvent, OperLogDTO.class);
        logFeignService.saveLog(dto);
    }

    @Async
    @EventListener
    public void saveLogininfor(LogininforEvent logininforEvent) {
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
        LogininforDTO dto = BeanCopyUtils.copy(logininforEvent, LogininforDTO.class);
        logFeignService.saveLogininfor(dto);
    }

}
