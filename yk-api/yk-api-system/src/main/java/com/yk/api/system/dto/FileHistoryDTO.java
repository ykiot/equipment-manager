package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_file_history
 * 文件存储历史表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "FileHistoryDTO", description = "文件存储历史表")
public class FileHistoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "对象存储主键")
    private Long id;
    @ApiModelProperty(value = "文件名")
    private String fileName;
    @ApiModelProperty(value = "原名")
    private String originalName;
    @ApiModelProperty(value = "文件后缀名")
    private String fileSuffix;
    @ApiModelProperty(value = "URL地址")
    private String url;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "上传人")
    private Long createdBy;
}