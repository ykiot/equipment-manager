package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yk.common.excel.core.DateConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_video
 * 摄像头
 *
 * @author lmx
 * @since 2023-11-07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "VideoDTO", description = "摄像头")
public class VideoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "关联场景")
    private Long groupId;
    @ExcelProperty(value = "摄像头名称", index = 1)
    @ApiModelProperty(value = "摄像头名称")
    private String name;
    @ExcelProperty(value = "摄像头地址", index = 3)
    @ApiModelProperty(value = "摄像头地址")
    private String address;
    @ApiModelProperty(value = "AppKey")
    private String appkey;
    @ApiModelProperty(value = "Secret")
    private String secret;
    @ApiModelProperty(value = "设备序列号")
    private String number;
    @ApiModelProperty(value = "通道")
    private String passage;
    @ApiModelProperty(value = "验证码")
    private String code;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ExcelProperty(value = "添加时间", index = 4, converter = DateConverter.class)
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    @ApiModelProperty(value = "token")
    private String token;
    @ExcelProperty(value = "关联场景", index = 2)
    @ApiModelProperty(value = "关联场景名称")
    private String groupName;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(value = "模糊查询")
    private String q;
}