package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_menu
 * 菜单权限表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MenuDTO", description = "菜单权限表")
public class MenuDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "菜单ID")
    private Long id;
    @ApiModelProperty(value = "菜单名称")
    private String menuName;
    @ApiModelProperty(value = "父菜单ID")
    private Long parentId;
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;
    @ApiModelProperty(value = "路由地址")
    private String path;
    @ApiModelProperty(value = "组件路径")
    private String component;
    @ApiModelProperty(value = "路由参数")
    private String queryParam;
    @ApiModelProperty(value = "是否为外链（0是 1否）")
    private Integer isFrame;
    @ApiModelProperty(value = "是否缓存（0缓存 1不缓存）")
    private Integer isCache;
    @ApiModelProperty(value = "菜单类型（M目录 C菜单 F按钮）")
    private String menuType;
    @ApiModelProperty(value = "显示状态（0显示 1隐藏）")
    private String visible;
    @ApiModelProperty(value = "菜单状态（0正常 1停用）")
    private String status;
    @ApiModelProperty(value = "权限标识")
    private String perms;
    @ApiModelProperty(value = "菜单图标")
    private String icon;
    @ApiModelProperty(value = "创建者")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新者")
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "备注")
    private String remark;


}