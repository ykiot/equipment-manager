package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yk.api.system.vo.GatewayVo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * sys_user
 * 用户信息表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UserDTO", description = "用户信息表")
public class UserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Long id;
    @ApiModelProperty(value = "部门ID")
    private Long deptId;
    @ApiModelProperty(value = "用户账号")
    private String userName;
    @ApiModelProperty(value = "用户账号")
    @JsonIgnore
    private String password;
    @ApiModelProperty(value = "用户昵称")
    private String nickName;
    @ApiModelProperty(value = "用户类型（sys_user系统用户）")
    private String userType;
    @ApiModelProperty(value="帐号状态（0正常 1停用）")
    private String status;
    @ApiModelProperty(value = "手机号码")
    private String phone;
    @ApiModelProperty(value = "用户性别（0男 1女 2未知）")
    private String sex;
    @ApiModelProperty(value = "头像地址")
    private String avatar;
    @ApiModelProperty(value = "创建者")
    private String createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    @ApiModelProperty(value="公司名称")
    private String corporationName;
    @ApiModelProperty(value="区域")
    private String region;
    @ApiModelProperty(value="地址")
    private String address;
    @ApiModelProperty(value="行业")
    private String profession;
    @ApiModelProperty(value="角色ID")
    private Long roleId;

    @ApiModelProperty(value="模糊查询")
    private String q;
    @ApiModelProperty(value="true-查询管理员")
    private Boolean admin;

    @ApiModelProperty(value="网关集合")
    private List<GatewayVo> gatewayList;

    public UserDTO(String userName, String phone){
        this.userName = userName;
        this.phone = phone;
    }
}