package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

/**
 * @author lmx
 * @date 2023/11/21 16:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "触发条件")
public class ConditionDTO {

    @ApiModelProperty(value = "触发条件 0-大于A、1-小于A、2-等于A、3-大于A且小于B、4-小于A或者大于B、5-不等于")
    private Integer name;
    @ApiModelProperty(value = "触发条件值A")
    private String valueA;
    @ApiModelProperty(value = "触发条件值B")
    private String valueB;

    /**
     * 字段拼接
     *
     * @return 触发条件字符串
     */
    public static String getRuleStr(Integer name, String valueA, String valueB) {
        if (Objects.isNull(name)) {
            return null;
        }
        switch (name) {
            case 0:
                return "大于" + valueA;
            case 1:
                return "小于" + valueA;
            case 2:
                return "等于" + valueA;
            case 3:
                return "大于" + valueA + "且小于" + valueB;
            case 4:
                return "小于" + valueA + "或者大于" + valueB;
            case 5:
                return "不等于" + valueA;
            default:
                return null;
        }
    }
}
