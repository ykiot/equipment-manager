package com.yk.api.system.model;

import com.yk.api.system.dto.LogininforDTO;
import com.yk.api.system.dto.OperLogDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author lmx
 * @description 日志服务
 * @date 2023/10/13 17:42
 */
@FeignClient(contextId = "logFeignService", value = "yk-system")
public interface LogFeignService {

    /**
     * 保存系统日志
     *
     * @param dto 日志实体
     * @return
     */
    @PostMapping("/log/addOperLog")
    public Result<Boolean> saveLog(@RequestBody OperLogDTO dto);

    /**
     * 保存访问记录
     *
     * @param dto 访问实体
     * @return
     */
    @PostMapping("/log/addLogininfor")
    public Result<Boolean> saveLogininfor(@RequestBody LogininforDTO dto);
}
