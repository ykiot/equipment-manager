package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * tb_device_role
 * 设备权限表
 *
 * @author lmx
 * @since 2023-11-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DeviceRoleDTO", description = "设备权限表")
public class DeviceRoleDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备权限id")
    private Long id;
    @ApiModelProperty(value = "设备管理员id")
    private Long adminId;
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "设备id")
    private Long deviceId;
    @ApiModelProperty(value = "权限类型 0-仅查看、1-允许控制、3-可管理")
    private String roleType;
    @ApiModelProperty(value = "过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expiredTime;
    @ApiModelProperty(value = "共享状态（0-共享中、1-已失效）")
    private String status;
    @ApiModelProperty(value = "账户手机号")
    private String phone;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;

    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    private Date endTime;
    @ApiModelProperty(value = "是否同意接收 true-接收")
    private Boolean receiveStatus;
    @ApiModelProperty(value = "消息ID")
    private Long messageId;
    @ApiModelProperty(value = "微信分享token")
    private String token;
    @ApiModelProperty(value = "设备名称")
    private String deviceName;
    @ApiModelProperty(value = "分享人名称")
    private String adminName;
    @ApiModelProperty(value = "被分享人名称")
    private String userName;

    @ApiModelProperty(value = "模糊查询")
    private String q;
    @ApiModelProperty(value = "0-仅自己 1-与我分享")
    private Integer type;
    @ApiModelProperty(value = "二维码过期秒数")
    private Long timeOutSeconds;
    @ApiModelProperty(value="公司名称")
    private String corporationName;
    @ApiModelProperty(value = "设备id数组")
    private List<Long> deviceIdList;
}