package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_logininfor
 * 系统访问记录
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "LogininforDTO", description = "系统访问记录")
public class LogininforDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "访问ID")
    private Long id;
    @ApiModelProperty(value = "用户账号")
    private String userName;
    @ApiModelProperty(value = "登录IP地址")
    private String ipaddr;
    @ApiModelProperty(value = "登录地点")
    private String loginLocation;
    @ApiModelProperty(value = "浏览器类型")
    private String browser;
    @ApiModelProperty(value = "操作系统")
    private String os;
    @ApiModelProperty(value = "登录状态（0成功 1失败）")
    private String status;
    @ApiModelProperty(value = "提示消息")
    private String msg;
    @ApiModelProperty(value = "访问时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date loginTime;


}