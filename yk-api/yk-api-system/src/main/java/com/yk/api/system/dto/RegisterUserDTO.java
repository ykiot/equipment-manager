package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * RegisterUserDTO
 * 用户注册DTO
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "RegisterUserDTO", description = "用户注册DTO")
public class RegisterUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户账号")
    private String userName;
    @ApiModelProperty(value = "用户密码")
    private String password;
    @ApiModelProperty(value = "用户昵称")
    private String nickName;
    @ApiModelProperty(value = "手机号码")
    private String phone;

    public RegisterUserDTO(String userName, String phone){
        this.userName = userName;
        this.phone = phone;
    }
}