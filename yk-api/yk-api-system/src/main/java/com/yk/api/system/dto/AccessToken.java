package com.yk.api.system.dto;

import lombok.Data;

/**
 * @author lmx
 * @date 2023/11/23 16:57
 */
@Data
public class AccessToken {
    /**
     * token
     */
    private String access_token;
    /**
     * 有效时间
     */
    private String expires_in;
}