package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * sys_card
 * 流量卡信息表
 *
 * @author lmx
 * @since 2024-02-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Card", description = "流量卡信息表")
public class CardDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Long id;
    @ApiModelProperty(value = "物联网卡号")
    private String cardno;
    @ApiModelProperty(value = "使用流量(-1表示未知，单位KB，时间当月)")
    private String used;
    @ApiModelProperty(value = "	剩余流量(单位KB)")
    private String surplus;
    @ApiModelProperty(value = "物联网卡状态")
    private String state;
    @ApiModelProperty(value = "卡可使用量")
    private String canused;
    @ApiModelProperty(value = "物联网卡状态描述")
    private String message;
    @ApiModelProperty(value = "报警值")
    private String alarm;
    @ApiModelProperty(value = "卡有效期")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expiredAt;
    @ApiModelProperty(value = "创建者")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新者")
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;
    @ApiModelProperty(value = "运营商")
    private String autoname;
    @ApiModelProperty(value = "卡作用设备")
    private String gateWayName;
    @ApiModelProperty(value = "0-管理端 1-客户端")
    private String type;

    @ApiModelProperty(value = "批量物联网卡号")
    private List<String> cardnoList;
    @ApiModelProperty(value = "ids")
    private List<Long> ids;
    @ApiModelProperty(value = "卡作用设备编号")
    private String gateWayNumber;

}