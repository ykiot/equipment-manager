package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yk.common.excel.core.DateConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 报警规则表
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "AlarmDTO", description = "报警规则表DTO")
public class AlarmDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备id")
    private Long id;
    @ExcelProperty(value = "触发器名称", index = 1)
    @ApiModelProperty(value = "触发器名称")
    private String name;
    @ExcelProperty(value = "触发器类型", index = 2)
    @ApiModelProperty(value = "触发器类型 0-设备 1-变量模版")
    private String type;
    @ApiModelProperty(value = "触发对象")
    private Long targetId;
    @ApiModelProperty(value = "变量id")
    private Long variableId;
    @ApiModelProperty(value = "触发条件 0-大于A、1-小于A、2-等于A、3-大于A且小于B、4-小于A或者大于B、5-不等于")
    private ConditionDTO rule;
    @ApiModelProperty(value = "是否推送 0-是 1-否")
    private String inform;
    @ApiModelProperty(value = "推送类型 0-公众号 1-短信 2-播报")
    private String informType;
    @ApiModelProperty(value = "是否启用 true-启用")
    private Boolean enable;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ExcelProperty(value = "创建时间", index = 7, converter = DateConverter.class)
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    @ExcelProperty(value = "触发对象", index = 3)
    @ApiModelProperty(value = "触发对象名称")
    private String targetName;
    @ExcelProperty(value = "报警变量", index = 4)
    @ApiModelProperty(value = "变量名称")
    private String variableName;
    @ApiModelProperty(value = "模糊查询")
    private String q;
    @ExcelProperty(value = "触发条件", index = 5)
    @ApiModelProperty(value = "触发条件字符串 0-大于A、1-小于A、2-等于A、3-大于A且小于B、4-小于A或者大于B、5-不等于")
    private String ruleStr;
    @ExcelProperty(value = "状态", index = 6)
    @ApiModelProperty(value = "是否启用 true-启用")
    private String enableStr;
    @ApiModelProperty(value = "token")
    private String token;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "自定义消息")
    private String message;
}