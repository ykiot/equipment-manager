package com.yk.api.system.model;


import com.yk.api.system.dto.MessageWechatDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 消息
 *
 * @author lmx
 * @since 2024-04-15
 */
@FeignClient(contextId = "MessageWechatFeignService", value = "yk-system")
public interface MessageWechatFeignService {

    /**
     * 保存消息
     * @param dto
     * @return
     */
    @PostMapping("/messageWechat/save")
    Result<Boolean> save(@RequestBody MessageWechatDTO dto);
}