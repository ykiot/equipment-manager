package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_dept
 * 部门表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DeptDTO", description = "部门表")
public class DeptDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门id")
    private Long id;
    @ApiModelProperty(value = "父部门id")
    private Long parentId;
    @ApiModelProperty(value = "部门名称")
    private String deptName;
    @ApiModelProperty(value = "负责人")
    private String leader;
    @ApiModelProperty(value = "联系电话")
    private String phone;
    @ApiModelProperty(value = "部门状态（0正常 1停用）")
    private String status;
    @ApiModelProperty(value = "显示顺序")
    private Integer orderNum;
    @ApiModelProperty(value = "创建者")
    private String createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


}