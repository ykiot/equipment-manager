package com.yk.api.system.model;

import com.yk.api.system.dto.VariableDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 变量服务feign接口
 *
 * @author lmx
 * @date 2023/10/13 16:56
 */
@FeignClient(contextId = "VariableFeignService", value = "yk-system")
public interface VariableFeignService {

    /**
     * 根据变量id查询详情
     */
    @PostMapping( "/variable/listByIds")
    Result<List<VariableDTO>> listByIds(@RequestBody VariableDTO dto);

    /**
     * 根据设备id查询存储的变量
     */
    @GetMapping( "/variable/listByDeviceId")
    Result<List<VariableDTO>> listByDeviceId(@RequestParam("deviceId") Long deviceId);
}
