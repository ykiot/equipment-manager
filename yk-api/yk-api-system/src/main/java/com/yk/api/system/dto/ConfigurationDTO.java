package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_configuration
 * 组态表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ConfigurationDTO", description = "组态表")
public class ConfigurationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主组态d")
    private Long id;
    @ApiModelProperty(value = "组态名称")
    private String name;
    @ApiModelProperty(value = "组态属性值")
    private String value;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    @ApiModelProperty(value = "场景id")
    private Long groupId;
    @ApiModelProperty(value = "场景名称")
    private String groupName;
    @ApiModelProperty(value = "模糊查询")
    private String q;
    @ApiModelProperty(value = "是否可编辑")
    private Boolean edit;
}