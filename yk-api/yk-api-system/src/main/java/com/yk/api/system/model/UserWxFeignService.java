package com.yk.api.system.model;

import com.yk.api.system.dto.UserWxDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lmx
 * @description 用户-微信服务
 * @date 2023/10/13 16:56
 */
@FeignClient(contextId = "UserWxFeignService", value = "yk-system")
public interface UserWxFeignService {

    /**
     * 通过手机号查询是否关联微信公众号
     *
     * @param phone 手机号
     * @return false-未关联微信公众号，true-已关联微信公众号
     */
    @GetMapping("/userWx/wxInfoByPhone")
    Result<Boolean> wxInfoByPhone(@RequestParam("phone") String phone);

    /**
     * 用户-微信关联保存
     *
     * @param wxDTO 用户-微信关联信息
     * @return 是否成功
     */
    @PostMapping("/userWx/saveUserWx")
    Result<Boolean> saveUserWx(@RequestBody UserWxDTO wxDTO);
}
