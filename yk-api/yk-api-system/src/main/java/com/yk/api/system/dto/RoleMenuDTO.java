package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * sys_role_menu
 * 角色和菜单关联表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "RoleMenuDTO", description = "角色和菜单关联表")
public class RoleMenuDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色ID")
    private Long roleId;
    @ApiModelProperty(value = "菜单ID")
    private Long menuId;


}