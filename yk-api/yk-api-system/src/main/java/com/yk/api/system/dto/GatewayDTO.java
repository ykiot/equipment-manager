package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * tb_gateway
 * 网关
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GatewayDTO", description = "网关")
@ExcelIgnoreUnannotated
public class GatewayDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "网关名称")
    @ExcelProperty(value = "网关名称", index = 1)
    private String name;
    @ApiModelProperty(value = "网关编号")
    @ExcelProperty(value = "网关编号", index = 2)
    private String number;
    @ApiModelProperty(value = "通讯密码")
    private String communicationPassword;
    @ApiModelProperty(value = "网关型号")
    @ExcelProperty(value = "网关型号", index = 3)
    private String model;
    @ApiModelProperty(value = "物联网卡号")
    @ExcelProperty(value = "物联网卡号", index = 4)
    private String cardNumber;
    @ApiModelProperty(value = "网关地址")
    @ExcelProperty(value = "网关地址", index = 5)
    private String address;
    @ApiModelProperty(value = "经纬度")
    private String latitudeLongitude;
    @ApiModelProperty(value = "图片地址")
    private String url;
    @ApiModelProperty(value="网关连接Emq-ClientId")
    private String clientId;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


    @ApiModelProperty(value = "网关-关联设备信息")
    private List<DeviceDTO> deviceDTOList;
    @ApiModelProperty(value = "主题")
    private String topic;
    @ApiModelProperty(value="网关状态 1-在线 0-离线")
    private Boolean status;
    @ApiModelProperty(value="模糊查询")
    private String q;

    @ApiModelProperty(value="报警状态 true-报警")
    private Boolean alarmStatus;
    @ApiModelProperty(value="token")
    private String token;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(value="场景Id")
    private Long groupId;
    @ApiModelProperty(value="转让用户手机号")
    private String phone;
    @ApiModelProperty(value="版本")
    private String version;
}