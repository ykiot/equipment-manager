package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * vb_template
 * 变量模版表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "TemplateDTO", description = "变量模版表")
public class TemplateDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private Long id;
    @NotBlank(message = "模型名称不能为空")
    @ApiModelProperty(value = "模型名称")
    private String modelName;
    @ApiModelProperty(value = "变量数")
    private Integer variableCount;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


    @ApiModelProperty(value = "变量")
    private List<VariableDTO> vbVariableDTOS;
    @ApiModelProperty(value = "变量ids，作用于变量配置-删除变量id的存储")
    private List<Long> variableIds;
    @ApiModelProperty(value = "模板ids，作用于多选")
    private List<Long> templateIds;
    @ApiModelProperty(value = "关联设备")
    private List<DeviceDTO> deviceDTOList;

    @ApiModelProperty(value = "模糊查询")
    private String q;
    @ApiModelProperty(value = "颜色")
    private String colour;
}