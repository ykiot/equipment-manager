package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_configuration_role
 * 组态权限表
 *
 * @author lmx
 * @since 2024-04-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ConfigurationRole", description = "设备权限表")
public class ConfigurationRoleDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "组态权限id")
    private Long id;
    @ApiModelProperty(value = "组态管理员id")
    private Long adminId;
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "组态id")
    private Long configurationId;
    @ApiModelProperty(value = "权限类型")
    private String roleType;
    @ApiModelProperty(value = "共享状态（0-共享中、1-已失效）")
    private String status;
    @ApiModelProperty(value = "过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expiredTime;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新")
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    @ApiModelProperty(value = "账户手机号")
    private String phone;
    @ApiModelProperty(value = "被分享人名称")
    private String userName;
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    private Date endTime;
    @ApiModelProperty(value = "组态id")
    private String configurationName;
}