package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 用户设备权限
 *
 * @author lmx
 * @since 2023-11-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UserDeviceRoleDTO", description = "用户设备权限")
public class UserDeviceRoleDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备id")
    private Long deviceId;
    @ApiModelProperty(value = "权限类型 0-仅查看、1-允许控制、3-可管理、4-设备创建者")
    private String roleType;
    @ApiModelProperty(value = "过期时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expiredTime;
}