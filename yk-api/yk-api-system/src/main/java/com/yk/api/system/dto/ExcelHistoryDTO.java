package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_excel_history
 * 文件记录表
 *
 * @author lmx
 * @since 2023-11-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "ExcelHistory", description = "文件记录表")
public class ExcelHistoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "报表id")
    private Long id;
    @ApiModelProperty(value = "报表名称")
    private String name;
    @ApiModelProperty(value = "数据源")
    private String dataSource;
    @ApiModelProperty(value = "数据维度")
    private String dataRadius;
    @ApiModelProperty(value = "数据开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;
    @ApiModelProperty(value = "数据结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endTime;
    @ApiModelProperty(value = "状态 0-未开始 1-生成中 2-成功 3-失败")
    private String status;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @ApiModelProperty(value = "地址")
    private String url;
}