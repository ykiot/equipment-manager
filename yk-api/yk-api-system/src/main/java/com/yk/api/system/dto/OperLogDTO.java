package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yk.common.excel.core.DateConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_oper_log
 * 操作日志记录
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "OperLogDTO", description = "操作日志记录")
public class OperLogDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日志主键")
    private Long id;

    @ApiModelProperty(value = "模块标题")
    private String title;
    @ApiModelProperty(value = "业务类型（0其它 1新增 2修改 3删除 4设备上线 5设备下线）")
    private Integer businessType;
    @ApiModelProperty(value = "方法名称")
    private String method;
    @ApiModelProperty(value = "请求方式")
    private String requestMethod;
    @ApiModelProperty(value = "操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;
    @ExcelProperty(value = "操作人员", index = 2)
    @ApiModelProperty(value = "操作人员")
    private String operName;
    @ApiModelProperty(value = "部门名称")
    private String deptName;
    @ApiModelProperty(value = "请求URL")
    private String operUrl;
    @ExcelProperty(value = "IP", index = 4)
    @ApiModelProperty(value = "主机地址")
    private String operIp;
    @ApiModelProperty(value = "操作地点")
    private String operLocation;
    @ApiModelProperty(value = "请求参数")
    private String operParam;
    @ApiModelProperty(value = "返回参数")
    private String jsonResult;
    @ApiModelProperty(value = "操作状态（0正常 1异常）")
    private Integer status;
    @ApiModelProperty(value = "错误消息")
    private String errorMsg;
    @ExcelProperty(value = "操作时间", index = 5, converter = DateConverter.class)
    @ApiModelProperty(value = "操作时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ExcelProperty(value = "操作内容", index = 3)
    @ApiModelProperty(value="操作内容")
    private String message;
    @ApiModelProperty(value="设备id")
    private Long deviceId;

    @ApiModelProperty(value = "查询类型（0-操作日志， 1-上下线）")
    private Integer type;
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endTime;
    @ExcelProperty(value = "类型", index = 1)
    @ApiModelProperty(value = "类型")
    private String typeStr;
    @ApiModelProperty(value = "token")
    private String token;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;
}