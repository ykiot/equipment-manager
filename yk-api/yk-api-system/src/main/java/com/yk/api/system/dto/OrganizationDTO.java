package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_organization
 * 平台信息
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "OrganizationDTO", description = "平台信息")
public class OrganizationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "")
    private Long id;
    @ApiModelProperty(value = "平台图片")
    private String logo;
    @ApiModelProperty(value = "公司名称")
    private String companyName;
    @ApiModelProperty(value = "平台名称")
    private String platformName;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;


}