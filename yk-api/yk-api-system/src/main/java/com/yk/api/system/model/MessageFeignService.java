package com.yk.api.system.model;


import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


/**
 * 消息
 *
 * @author lmx
 * @since 2024-04-15
 */
@FeignClient(contextId = "MessageFeignService", value = "yk-system")
public interface MessageFeignService {

    /**
     * 系统通知消息
     *
     * @param title   消息标题
     * @param userId  用户ID
     * @param content 消息内容
     * @param type    消息类型 0-系统消息 1-共享消息
     */
    @PostMapping("/message/systemMessage")
    Result<Void> systemMessage(@RequestParam("title") String title,
                                      @RequestParam("userId") Long userId,
                                      @RequestParam("content") String content,
                                      @RequestParam("type") String type);
}