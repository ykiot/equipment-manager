package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * tb_group_configuration
 * 场景-组态
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GroupConfigurationDTO", description = "场景-组态")
public class GroupConfigurationDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "场景id")
    private Long groupId;
    @ApiModelProperty(value = "组态id")
    private Long configurationId;


}