package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * sys_announcement
 * 公告
 *
 * @author lmx
 * @since 2024-01-04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Announcement", description = "公告")
public class AnnouncementDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公告id")
    private Long id;
    @ApiModelProperty(value = "公告标题")
    private String title;
    @ApiModelProperty(value = "公告内容")
    private String content;
    @ApiModelProperty(value = "图片")
    private String url;
    @ApiModelProperty(value = "是否启用 0-启用 1-停用")
    private String status;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "更新者")
    private String updatedBy;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "ids")
    private List<Long> ids;
    @ApiModelProperty(value = "跳转链接")
    private String link;
}