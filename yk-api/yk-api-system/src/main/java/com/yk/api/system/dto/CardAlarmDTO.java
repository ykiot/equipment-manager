package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_card_alarm
 * 流量卡信息表
 *
 * @author lmx
 * @since 2024-02-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CardAlarm", description = "流量卡信息表")
public class CardAlarmDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Long id;
    @ApiModelProperty(value = "物联网卡号")
    private String cardno;
    @ApiModelProperty(value = "使用流量(-1表示未知，单位KB，时间当月)")
    private String used;
    @ApiModelProperty(value = "	剩余流量(单位KB)")
    private String surplus;
    @ApiModelProperty(value = "报警内容")
    private String alarmMessage;
    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "0-报警 1-恢复")
    private String status;
    @ApiModelProperty(value = "卡作用设备")
    private String gateWayName;
}