package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 文件信息
 *
 * @author lmx
 * @date 2023/10/18 10:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "FileDTO", description = "文件信息")
public class FileDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件名称")
    private String name;
    @ApiModelProperty(value = "文件地址")
    private String url;
}
