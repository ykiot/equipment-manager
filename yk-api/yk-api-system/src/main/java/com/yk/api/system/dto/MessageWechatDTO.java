package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_message_wechat
 * 消息
 *
 * @author lmx
 * @since 2024-04-15
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MessageWechat", description = "消息")
public class MessageWechatDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息id")
    private Long id;
    @ApiModelProperty(value = "公众号id")
    private String targetId;
    @ApiModelProperty(value = "消息标题")
    private String title;
    @ApiModelProperty(value = "消息内容")
    private String content;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    public MessageWechatDTO(String targetId, String title, String content, Date createdAt) {
        this.targetId = targetId;
        this.title = title;
        this.content = content;
        this.createdAt = createdAt;
    }
}