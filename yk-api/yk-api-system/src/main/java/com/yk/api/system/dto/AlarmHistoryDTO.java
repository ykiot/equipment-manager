package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yk.common.excel.core.DateConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * tb_alarm_history
 * 报警历史表
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "AlarmHistoryDTO", description = "报警历史表DTO")
public class AlarmHistoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "报警记录id")
    private Long id;
    @ApiModelProperty(value = "设备id")
    private Long deviceId;
    @ApiModelProperty(value = "变量id")
    private Long variableId;
    @ApiModelProperty(value = "报警规则id")
    private Long alarmId;
    @ExcelProperty(value = "报警值", index = 4)
    @ApiModelProperty(value = "报警值")
    private String alarmValue;
    @ApiModelProperty(value = "恢复值")
    private String recoverValue;
    @ExcelProperty(value = "报警时间", index = 6, converter = DateConverter.class)
    @ApiModelProperty(value = "报警时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date alarmAt;
    @ExcelProperty(value = "恢复时间", index = 7, converter = DateConverter.class)
    @ApiModelProperty(value = "恢复时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date recoverAt;
    @ApiModelProperty(value = "是否恢复 false-正常")
    private Boolean alarmState;
    @ExcelProperty(value = "状态", index = 1)
    @ApiModelProperty(value = "状态")
    private String alarmStateStr;
    @ApiModelProperty(value = "是否消音 true消音")
    private Boolean status;

    @ApiModelProperty(value = "报警信息")
    private String message;
    @ApiModelProperty(value = "推送类型 0-公众号 1-短信 2-播报")
    private String informType;
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    @ExcelProperty(value = "设备名称", index = 2)
    @ApiModelProperty(value = "设备名称")
    private String deviceName;
    @ExcelProperty(value = "变量", index = 3)
    @ApiModelProperty(value = "变量名称")
    private String variableName;
    @ApiModelProperty(value = "触发条件")
    private ConditionDTO rule;
    @ExcelProperty(value = "触发条件", index = 5)
    @ApiModelProperty(value = "触发条件-字符串")
    private String ruleStr;

    @ApiModelProperty(value = "设备ids")
    private List<Long> deviceIdList;
    @ApiModelProperty(value = "token")
    private String token;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;

    @ApiModelProperty(value = "场景ID")
    private Long groupId;

    public AlarmHistoryDTO(Boolean alarmState) {
        this.alarmState = alarmState;
    }

    public AlarmHistoryDTO(Boolean alarmState, String message) {
        this.alarmState = alarmState;
        this.message = message;
    }
}