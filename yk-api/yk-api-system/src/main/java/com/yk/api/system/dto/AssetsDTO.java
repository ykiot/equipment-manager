package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_assets
 * 资产
 *
 * @author lmx
 * @since 2023-11-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "AssetsDTO", description = "资产")
public class AssetsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "归属客户")
    private Long customerId;
    @ApiModelProperty(value = "网关id")
    private Long gatewayId;
    @ApiModelProperty(value = "网关编号")
    @ExcelProperty(value = "网关编号", index = 3)
    private String gatewayNumber;
    @ApiModelProperty(value = "网关主题")
    @ExcelProperty(value = "网关主题", index = 6)
    private String topic;
    @ApiModelProperty(value = "网关密码")
    @ExcelProperty(value = "网关密码", index = 7)
    private String password;
    @ApiModelProperty(value = "售出时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ExcelProperty(value = "出售时间", index = 0)
    @DateTimeFormat(value = "yyyy-MM-dd HH:mm:ss")
    private Date saleTime;
    @ApiModelProperty(value = "创建时间=入库时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    @ExcelProperty(value = "入库时间", index = 10)
    @DateTimeFormat(value = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "是否添加到平台（0代表添加 2代表不添加）")
    @ExcelProperty(value = "是否添加到平台", index = 8)
    private String toPc;
    @ApiModelProperty(value = "状态 0-已出售1-未出售2-返厂维修")
    @ExcelProperty(value = "状态", index = 9)
    private String status;

    @ApiModelProperty(value = "物联网卡号")
    @ExcelProperty(value = "流量卡号", index = 4)
    private String ccId;
    @ApiModelProperty(value = "客户-物联网卡号")
    private String clientCcId;
    @ApiModelProperty(value = "归属客户名称")
    @ExcelProperty(value = "归属客户", index = 5)
    private String customerName;
    @ApiModelProperty(value = "添加网关的人")
    private String gatewayCreatedName;
    @ApiModelProperty(value = "添加网关的人")
    private Long gatewayCreatedId;
    @ApiModelProperty(value="网关型号")
    @ExcelProperty(value = "产品型号", index = 1)
    private String model;
    @ApiModelProperty(value="备注")
    private String bz;
    @ApiModelProperty(value="版本")
    @ExcelProperty(value = "产品版本", index = 2)
    private String version;
}