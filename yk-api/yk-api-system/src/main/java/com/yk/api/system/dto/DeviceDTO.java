package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * tb_device
 * 设备表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
@ApiModel(value = "DeviceDTO", description = "设备表")
public class DeviceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备id")
    private Long id;
    @ApiModelProperty(value = "网关id")
    private Long gatewayId;
    @ApiModelProperty(value = "场景id")
    private Long groupId;
    @ApiModelProperty(value = "变量模版id")
    private Long templateId;
    @ExcelProperty(value = "设备名称", index = 2)
    @ApiModelProperty(value = "设备名称")
    private String deviceName;
    @ApiModelProperty(value = "从机地址")
    private String slaveAddress;
    @ApiModelProperty(value = "图片")
    private String url;
    @ApiModelProperty(value = "备注")
    private String notes;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

    @ApiModelProperty(value="模糊查询")
    private String q;
    @ApiModelProperty(value="设备状态 0-离线 1-在线")
    private Boolean status;
    @ApiModelProperty(value = "需要关联场景ids")
    private List<Long> groupIds;

    @ApiModelProperty(value = "所属场景")
    private List<String> groupNameList;
    @ExcelProperty(value = "关联网关", index = 4)
    @ApiModelProperty(value = "关联网关")
    private String gatewayName;
    @ExcelProperty(value = "关联变量模版", index = 5)
    @ApiModelProperty(value = "关联变量模版")
    private String templateName;
    @ExcelProperty(value = "设备地址", index = 6)
    @ApiModelProperty(value = "所属地址")
    private String address;
    @ApiModelProperty(value = "设备所属人")
    private String adminName;
    @ApiModelProperty(value="物联网卡号")
    private String cardNumber;
    @ApiModelProperty(value="分享人数")
    private Long shareNum;
    @ApiModelProperty(value="变量id，控制设备使用")
    private Long variableId;
    @ApiModelProperty(value = "控制参数")
    private String param;
    @ExcelProperty(value = "状态", index = 1)
    @ApiModelProperty(value="设备状态字符串")
    private String statusStr;
    @ExcelProperty(value = "所属场景", index = 3)
    @ApiModelProperty(value = "所属场景字符串")
    private String groupNameStr;

    @ApiModelProperty(value = "设备IDs")
    private List<Long> deviceIdList;
    @ApiModelProperty(value = "token")
    private String token;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;
    @ApiModelProperty(value = "排序")
    private Integer sort;
}