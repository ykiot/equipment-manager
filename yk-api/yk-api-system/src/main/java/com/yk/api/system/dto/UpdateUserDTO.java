package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * UpdateUserDTO
 * 用户更新DTO
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UpdateUserDTO", description = "用户更新DTO")
public class UpdateUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Long id;
    @ApiModelProperty(value = "用户账号")
    private String userName;
    @ApiModelProperty(value = "用户账号")
    private String password;
    @ApiModelProperty(value="帐号状态（0正常 1停用）")
    private String status;
    @ApiModelProperty(value = "手机号码")
    private String phone;
    @ApiModelProperty(value = "旧手机号码")
    private String oldPhone;
}