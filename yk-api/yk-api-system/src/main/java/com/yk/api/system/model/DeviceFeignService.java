package com.yk.api.system.model;

import com.yk.api.system.dto.AlarmDTO;
import com.yk.api.system.dto.AlarmHistoryDTO;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.api.system.dto.GatewayDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 网关-设备服务
 *
 * @author lmx
 * @date 2023/10/13 16:56
 */
@FeignClient(contextId = "deviceFeignService", value = "yk-system")
public interface DeviceFeignService {

    String GATEWAY = "/gateWay";
    String DEVICE = "/device";
    String ALARM = "/alarm";
    String ALARM__HISTORY = "/alarmHistory";

    /**
     * 查询所有在线的设备IDs
     *
     * @return 设备IDs
     */
    @PostMapping(DEVICE + "/selectByOnlineDeviceId")
    Result<List<Long>> selectByOnlineDeviceId();

    /**
     * 根据网关编号查询网关信息
     *
     * @param number 网关编号
     * @return 网关信息
     */
    @PostMapping(GATEWAY + "/selectByNum")
    Result<GatewayDTO> selectByNum(@RequestParam("number") String number);

    /**
     * 根据网关编号-设备编号查询
     *
     * @param gateWayId 网关id
     * @param address   从机地址
     * @return 设备信息
     */
    @PostMapping(DEVICE + "/selectByGateWayIdAndDeviceNum")
    Result<List<DeviceDTO>> selectByGateWayNumAndDeviceNum(@RequestParam("gateWayId") Long gateWayId, @RequestParam("address") String address);

    /**
     * 根据设备id查询
     *
     * @param deviceId 设备id
     * @return 设备信息
     */
    @PostMapping(DEVICE + "/selectById")
    Result<DeviceDTO> selectById(@RequestParam("deviceId") Long deviceId);

    /**
     * 根据模板/设备/变量查询规则
     *
     * @param dto 查询条件
     * @return 报警规则
     */
    @PostMapping(ALARM + "/selectByAlarm")
    Result<List<AlarmDTO>> selectByAlarm(@RequestBody AlarmDTO dto);

    /**
     * 报警信息处理
     *
     * @param param
     * @return
     */
    @PostMapping(ALARM__HISTORY + "/dealWithAlarmMessage")
    Result<Void> dealWithAlarmMessage(@RequestBody AlarmHistoryDTO param);

}
