package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_camera
 * 摄像头
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CameraDTO", description = "摄像头")
public class CameraDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "摄像头id")
    private Long id;
    @ApiModelProperty(value = "摄像头名称")
    private String name;
    @ApiModelProperty(value = "摄像头地址")
    private String address;
    @ApiModelProperty(value = "AppKey")
    private String appkey;
    @ApiModelProperty(value = "Secret")
    private String secret;
    @ApiModelProperty(value = "设备序列号")
    private String number;
    @ApiModelProperty(value = "通道")
    private String thoroughfare;
    @ApiModelProperty(value = "验证码")
    private String code;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

}