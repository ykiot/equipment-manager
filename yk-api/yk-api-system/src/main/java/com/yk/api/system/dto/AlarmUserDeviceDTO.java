package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_alarm_user_device
 * 报警接收配置表
 *
 * @author lmx
 * @since 2024-02-27
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AlarmUserDevice", description = "报警接收配置表")
public class AlarmUserDeviceDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    private Long id;
    @ApiModelProperty(value = "报警规则id")
    private Long alarmId;
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "设备id")
    private Long deviceId;
    @ApiModelProperty(value = "变量id")
    private Long variableId;
    @ApiModelProperty(value = "推送类型 0-公众号 1-短信 2-播报")
    private String informType;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新")
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;


}