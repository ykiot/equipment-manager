package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_user_wx
 * 用户-微信关联表
 *
 * @author lmx
 * @since 2023-11-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UserWx", description = "用户-微信关联表")
public class UserWxDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    private Long id;
    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "小程序-openid")
    private String xOpenId;
    @ApiModelProperty(value = "公众号-openid")
    private String gOpenId;
    @ApiModelProperty(value = "微信开放平台-id")
    private String unionId;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


}