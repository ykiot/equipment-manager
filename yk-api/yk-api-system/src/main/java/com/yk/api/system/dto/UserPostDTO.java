package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * sys_user_post
 * 用户与岗位关联表
 *
 * @author lmx
 * @since 2023-10-24
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "UserPostDTO", description = "用户与岗位关联表")
public class UserPostDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    private Long userId;
    @ApiModelProperty(value = "岗位ID")
    private Long postId;


}