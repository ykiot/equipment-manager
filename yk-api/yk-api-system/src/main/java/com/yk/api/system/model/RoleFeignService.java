package com.yk.api.system.model;

import com.yk.common.core.domain.LoginUser;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lmx
 * @description 权限服务
 * @date 2023/10/13 16:56
 */
@FeignClient(contextId = "RoleFeignService", value = "yk-system")
public interface RoleFeignService {

    /**
     * 通过用户名或手机号查询用户信息（带权限校验）
     *
     * @param username 用户名
     * @return 用户信息
     */
    @GetMapping("/sys/role/info")
    Result<LoginUser> getUserInfo(@RequestParam("username") String username);
}
