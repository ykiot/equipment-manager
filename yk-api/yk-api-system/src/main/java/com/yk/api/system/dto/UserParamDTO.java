package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 用户操作DTO
 *
 * @author lmx
 * @date 2023/10/17 14:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel("用户操作DTO")
public class UserParamDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户ids
     */
    @ApiModelProperty("用户ids")
    private List<Long> userIds;
}
