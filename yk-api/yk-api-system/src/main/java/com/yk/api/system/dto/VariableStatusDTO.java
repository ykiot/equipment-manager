package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 变量状态dto
 * @author lmx
 * @date 2023/11/21 14:55
 */
@ApiModel(description = "变量状态dto")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VariableStatusDTO {

    @ApiModelProperty(value = "状态值")
    private Integer statusName;

    @ApiModelProperty(value = "含义")
    private String statusValue;

    @ApiModelProperty(value = "颜色")
    private String colour;
}
