package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * vb_variable_template
 * 变量模版表
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "VariableTemplateDTO", description = "变量模版表")
public class VariableTemplateDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "变量id")
    private Long variableId;
    @ApiModelProperty(value = "模版id")
    private Long templateId;


}