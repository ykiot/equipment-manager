package com.yk.api.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 消息
 *
 * @author lmx
 * @since 2023-12-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "MessageCountDTO", description = "消息统计DTO")
public class MessageCountDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统消息数量")
    private Integer systemNum;
    @ApiModelProperty(value = "分享消息数量")
    private Integer shareNum;
}