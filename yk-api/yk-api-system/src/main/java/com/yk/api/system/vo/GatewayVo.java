package com.yk.api.system.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * GatewayVO
 * 网关
 *
 * @author lmx
 * @since 2023-10-24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GatewayVO", description = "网关信息")
@ExcelIgnoreUnannotated
public class GatewayVo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "网关名称")
    @ExcelProperty(value = "网关名称", index = 1)
    private String name;
    @ApiModelProperty(value = "网关编号")
    @ExcelProperty(value = "网关编号", index = 2)
    private String number;
}