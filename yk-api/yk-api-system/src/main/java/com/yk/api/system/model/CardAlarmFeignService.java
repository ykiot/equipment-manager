package com.yk.api.system.model;

import com.yk.api.system.dto.CardAlarmDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * 流量卡记录服务
 *
 * @author thinglinks
 */
@FeignClient(contextId = "cardAlarmFeignService", value = "yk-system")
public interface CardAlarmFeignService {

    /**
     * 保存流量卡报警推送信息
     *
     * @param dto 流量卡记录信息表
     * @return 成功失败信息
     */
    @PostMapping("/cardAlarm/save")
    Result<Void> saveCardAlarm(@RequestBody CardAlarmDTO dto);
}
