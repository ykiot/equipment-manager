package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * sys_message
 * 消息
 *
 * @author lmx
 * @since 2023-12-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Message", description = "消息")
public class MessageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息id")
    private Long id;
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @ApiModelProperty(value = "消息标题")
    private String title;
    @ApiModelProperty(value = "消息内容")
    private String content;
    @ApiModelProperty(value = "报警类型 0-系统消息 1-共享消息")
    private String type;
    @ApiModelProperty(value = "共享人")
    private Long shareId;
    @ApiModelProperty(value = "被共享人")
    private Long sharedId;
    @ApiModelProperty(value = "是否已读 ture-已读")
    private Boolean status;
    @ApiModelProperty(value = "是否接收 true-接收")
    private Boolean receiveStatus;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "更新时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;

    @ApiModelProperty(value = "账户手机号")
    private String phone;
    @ApiModelProperty(value = "设备id")
    private Long deviceId;
    @ApiModelProperty(value = "设备id")
    private List<Long> ids;
    @ApiModelProperty(value = "开始时间")
    private Date startTime;
    @ApiModelProperty(value = "结束时间")
    private Date endTime;
    @ApiModelProperty(value = "目标类型 0-系统 1-设备 2-组态")
    private String targetType;
}