package com.yk.api.system.model;

import com.yk.api.system.dto.FileDTO;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

/**
 * 文件服务
 *
 * @author thinglinks
 */
@FeignClient(contextId = "fileFeignService", value = "yk-system")
public interface FileFeignService {
    /**
     * 上传文件
     *
     * @param file 文件信息
     * @return 结果
     */
    @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Result<FileDTO> upload(@RequestPart(value = "file") MultipartFile file);
}
