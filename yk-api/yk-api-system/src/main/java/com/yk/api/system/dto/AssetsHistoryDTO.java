package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_assets_history
 * 资产历史记录
 *
 * @author lmx
 * @since 2024-01-04
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AssetsHistory", description = "资产历史记录")
public class AssetsHistoryDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "归属客户")
    private Long customerId;
    @ApiModelProperty(value = "网关id")
    private Long gatewayId;
    @ApiModelProperty(value = "网关编号")
    private String gatewayNumber;
    @ApiModelProperty(value = "网关主题")
    private String topic;
    @ApiModelProperty(value = "网关密码")
    private String password;
    @ApiModelProperty(value = "售出时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date saleTime;
    @ApiModelProperty(value = "回收原因")
    private String remark;
    @ApiModelProperty(value = "旧创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date oldCreatedAt;
    @ApiModelProperty(value = "旧创建人")
    private Long oldCreatedBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    @ApiModelProperty(value = "资产id")
    private Long assetsId;
}