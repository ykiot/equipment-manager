package com.yk.api.system.model;

import com.yk.api.system.dto.RegisterUserDTO;
import com.yk.api.system.dto.UpdateUserDTO;
import com.yk.common.core.domain.LoginUser;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lmx
 * @description 用户服务
 * @date 2023/10/13 16:56
 */
@FeignClient(contextId = "userFeignService", value = "yk-system")
public interface UserFeignService {

    /**
     * 通过用户名或手机号查询用户信息
     *
     * @param username 用户名
     * @return 用户信息
     */
    @GetMapping("/sys/user/info")
    Result<LoginUser> getUserInfo(@RequestParam("username") String username);

    /**
     * 通过用户名或手机号查询用户信息
     *
     * @param userId 用户ID
     * @return 用户信息
     */
    @GetMapping("/sys/user/infoById")
    Result<LoginUser> getUserInfoById(@RequestParam("userId") Long userId);

    /**
     * 注册用户信息
     *
     * @param dto 用户信息
     * @return 用户信息
     */
    @PostMapping("/sys/user/register")
    Result<LoginUser> registerUserInfo(@RequestBody RegisterUserDTO dto);

    /**
     * 密码修改
     *
     * @param dto 用户信息
     * @return 是否成功
     */
    @PostMapping("/sys/user/forgotAndUpdatePassword")
    Result<Boolean> forgotAndUpdatePassword(@RequestBody UpdateUserDTO dto);

    /**
     * 手机号修改
     *
     * @param dto 用户信息
     * @return 是否成功
     */
    @PostMapping("/sys/user/updatePhone")
    Result<Boolean> updatePhone(@RequestBody UpdateUserDTO dto);

    /**
     * 通过手机号查询用户信息
     *
     * @param phone 手机号
     * @return 用户信息
     */
    @GetMapping("/sys/user/infoByPhone")
    Result<LoginUser> getUserInfoByPhone(@RequestParam("phone") String phone);

    /**
     * 通过微信openid查询用户信息
     *
     * @param openId openid
     * @return 用户信息
     */
    @GetMapping("/sys/user/infoByOpenId")
    Result<LoginUser> getUserInfoByOpenId(@RequestParam("openId") String openId);
}
