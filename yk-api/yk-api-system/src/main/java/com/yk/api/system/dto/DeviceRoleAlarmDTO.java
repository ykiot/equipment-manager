package com.yk.api.system.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 报警接收配置DTO
 *
 * @author lmx
 * @since 2023-11-08
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "DeviceRoleAlarmDTO", description = "报警接收配置DTO")
public class DeviceRoleAlarmDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "触发器Id")
    private Long alarmId;
    @ApiModelProperty(value = "触发器名称")
    private String alarmName;
    @ApiModelProperty(value = "设备名")
    private String deviceName;
    @ApiModelProperty(value = "设备Id")
    private Long deviceId;
    @ApiModelProperty(value = "变量名")
    private String variableName;
    @ApiModelProperty(value = "变量Id")
    private Long variableId;
    @ApiModelProperty(value = "报警推送项")
    private String informType;
    @ApiModelProperty(value = "用户报警推送项")
    private String userInformType;
    @ApiModelProperty(value = "创建人Id")
    private Long userId;
    @ApiModelProperty(value = "创建人姓名")
    private String userName;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;

    @ApiModelProperty(value = "设备ids")
    private List<Long> deviceIds;
    @ApiModelProperty(value = "模版ids")
    private List<Long> tempIds;
    @ApiModelProperty(value = "模糊查询")
    private String q;
    @ApiModelProperty(value = "4-仅自己 3-与我分享")
    private Integer type;
    @ApiModelProperty(value = "用户关联触发器Id")
    private Long userAlarmId;
}