package com.yk.api.system.dto;

import cn.hutool.json.JSONObject;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 变量dto
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description = "变量dto")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VariableDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    private Long id;
    @ApiModelProperty(value = "变量名称")
    private String name;
    @ApiModelProperty(value = "变量类型 0-模拟量 1-开关量")
    private String type;
    @ApiModelProperty(value = "变量地址")
    private String address;
    @ApiModelProperty(value = "变量值")
    private String value;
    @ApiModelProperty(value = "变量单位")
    private String unit;
    @ApiModelProperty(value = "设备id")
    private String deviceId;
    @ApiModelProperty(value = "代表状态")
    private List<VariableStatusDTO> indicatesStatus;
    @ApiModelProperty(value = "数据修正")
    private String formula;
    @ApiModelProperty(value = "写数据数据修正")
    private String writeFormula;
    @ApiModelProperty(value = "小数位")
    private Integer decimal;
    @ApiModelProperty(value = "是否展示 0-不展示 1-展示")
    private String show;
    @ApiModelProperty(value = "是否存储 0-不存储 1-存储")
    private String storage;
    @ApiModelProperty(value = "是否控制 0-不控制 1-控制")
    private String control;
    @ApiModelProperty(value = "创建人")
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "读取最大值")
    private Float readMax;
    @ApiModelProperty(value = "写入最大值")
    private Float writeMax;
    @ApiModelProperty(value = "读取最小值")
    private Float readMin;
    @ApiModelProperty(value = "写入最小值")
    private Float writeMin;

    @ApiModelProperty(value = "排序")
    private Integer sort;
    @ApiModelProperty(value = "ids")
    private List<Long> ids;
    @ApiModelProperty(value = "模版ID")
    private Long templateId;

    @ApiModelProperty(value = "数据类型 0-普通变量 1-差值变量 2-内部变量")
    private String dataType;

    @ApiModelProperty(value = "计算类型 0-周期 1-实时")
    private String computeType;

    @ApiModelProperty(value = "周期频率 0-日 1-周 2-月")
    private String cycleFrequency;

    @ApiModelProperty(value = "引用变量")
    private String parentId;

    @ApiModelProperty(value = "引用变量地址")
    private String parentAddress;

    @ApiModelProperty(value = "引用变量名称")
    private String parentName;

    @ApiModelProperty(value = "内部变量")
    private JSONObject interior;

    @ApiModelProperty(value = "是否复位 0-否 2-是")
    private String reset;

    @ApiModelProperty(value = "复位时间（秒）")
    private Integer resetTime;

    @ApiModelProperty(value = "复位指令")
    private String resetCommand;
}