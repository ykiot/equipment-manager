package com.yk.api.system.dto;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_customer_info
 * 客户信息
 *
 * @author lmx
 * @since 2023-11-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CustomerInfoDTO", description = "客户信息")
@ExcelIgnoreUnannotated
public class CustomerInfoDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    private Long id;
    @ApiModelProperty(value = "公司名")
    @ExcelProperty(value = "公司名称", index = 0)
    private String companyName;
    @ApiModelProperty(value = "联系人")
    @ExcelProperty(value = "联系人", index = 1)
    private String contactPerson;
    @ApiModelProperty(value = "联系人手机号")
    @ExcelProperty(value = "联系人手机号", index = 2)
    private String phone;
    @ApiModelProperty(value = "省")
    @ExcelProperty(value = "省", index = 3)
    private String province;
    @ApiModelProperty(value = "市")
    @ExcelProperty(value = "市", index = 4)
    private String city;
    @ApiModelProperty(value = "详细地址")
    @ExcelProperty(value = "详细地址", index = 5)
    private String address;
    @ApiModelProperty(value = "备注")
    @ExcelProperty(value = "备注", index = 6)
    private String remarks;
    @ApiModelProperty(value = "创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


}