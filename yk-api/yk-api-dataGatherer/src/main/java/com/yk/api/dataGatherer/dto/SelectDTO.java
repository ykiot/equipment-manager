package com.yk.api.dataGatherer.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 查询所需入参对象
 *
 * @author lmx
 * @date 2023/10/27 14:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "查询所需入参对象")
public class SelectDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 库名
     */
    @ApiModelProperty(value = "库名")
    private String dataBaseName;

    /**
     * 表名
     */
    @ApiModelProperty(value = "表名 设备id")
    private String tableName;

    /**
     * 变量id
     */
    @ApiModelProperty(value = "变量id")
    private List<Long> variableIds;

    /**
     * 字段值
     */
    @ApiModelProperty(value = "字段值")
    private List<String> fieldNames;

    /**
     * 开始时间
     */
    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    private String startTime;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    private String endTime;

    /**
     * 字段值
     */
    @ApiModelProperty(value = "设备ids")
    private List<Long> deviceIds;

    /**
     * token
     */
    @ApiModelProperty(value = "token")
    private String token;
}
