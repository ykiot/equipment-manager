package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @author lmx
 * @date 2023/11/16 15:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "自定义报表查询DTO")
public class CustomDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("报表名称")
    private String excelName;

    @ApiModelProperty("模板id")
    private Long templateId;

    @ApiModelProperty("设备参数")
    private List<DeviceParamDTO> devices;

    @ApiModelProperty("开始时间")
    private String startTime;

    @ApiModelProperty("结束时间")
    private String endTime;

    @ApiModelProperty("数据维度")
    private List<VariableParamDTO> variables;

    @ApiModelProperty("创建人")
    private Long createdBy;

    @ApiModelProperty("记录ID")
    private Long excelHistoryId;
}
