package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 超级表的结构信息
 *
 * @author lmx
 * @date 2023/10/27 14:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "超级表的结构信息")
public class DescribeDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字段名")
    private String field;

    @ApiModelProperty(value = "字段类型")
    private String type;

    @ApiModelProperty(value = "字段类型大小")
    private String length;

    @ApiModelProperty(value = "备注 标签这里为TAG")
    private String note;
}
