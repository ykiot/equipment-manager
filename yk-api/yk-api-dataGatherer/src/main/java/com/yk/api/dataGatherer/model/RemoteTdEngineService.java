package com.yk.api.dataGatherer.model;

import com.yk.api.dataGatherer.dto.*;
import com.yk.common.core.domain.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

/**
 * 时序性数据库TdEngine服务
 *
 * @author lmx
 * @date 2023/10/27 14:43
 */
@FeignClient(contextId = "remoteTdEngineService", value = "yk-dataGatherer")
public interface RemoteTdEngineService {

    /**
     * 创建tdEngine数据库
     *
     * @param databaseName 库名
     * @return
     */
    @PostMapping("/dataOperation/createDb")
    Result<?> createDataBase(@RequestParam("databaseName") String databaseName);

    /**
     * 创建超级表
     *
     * @param superTableDto
     * @return
     */
    @PostMapping("/dataOperation/createSTb")
    Result<?> createSuperTable(@Validated @RequestBody SuperTableDTO superTableDto);

    /**
     * 创建超级表的子表
     *
     * @param tableDto
     * @return
     */
    @PostMapping("/dataOperation/createTb")
    Result<?> createTable(@Validated @RequestBody TableDTO tableDto);

    /**
     * 删除超级表的子表
     *
     * @param tableDto
     * @return
     */
    @PostMapping("/dataOperation/delTable")
    Result<?> delTable(@RequestBody TableDTO tableDto);

    /**
     * 插入数据
     *
     * @param tableDto
     * @return
     */
    @PostMapping("/dataOperation/insertData")
    Result<?> insertData(@Validated @RequestBody TableDTO tableDto);

    /**
     * 查询最新数据
     *
     * @param selectDto
     * @return
     */
    @PostMapping("/dataOperation/getLastData")
    Result<?> getLastData(@Validated @RequestBody SelectDTO selectDto);

    /**
     * 查询最新的数据带标签
     *
     * @param tagsSelectDTO
     * @return
     */
    @PostMapping("/dataOperation/getLastDataByTags")
    Result<Map<String, Map<String, Object>>> getLastDataByTags(@Validated @RequestBody TagsSelectDTO tagsSelectDTO);

    /**
     * 添加列字段
     *
     * @param superTableDto
     * @return
     */
    @PostMapping("/dataOperation/addColumnForSuperTable")
    Result<?> addColumnForSuperTable(@RequestBody SuperTableDTO superTableDto);

    /**
     * 删除列字段
     *
     * @param superTableDto
     * @return
     */
    @PostMapping("/dataOperation/dropColumnForSuperTable")
    Result<?> dropColumnForSuperTable(@RequestBody SuperTableDTO superTableDto);

    /**
     * 查询自定义报表数据
     * @param customDTO
     * @return
     */
    @PostMapping("/dataOperation/getCustomReportData")
    Result<Map<String, List<Map<String, Object>>>> getCustomReportData(@RequestBody CustomDTO customDTO);
}
