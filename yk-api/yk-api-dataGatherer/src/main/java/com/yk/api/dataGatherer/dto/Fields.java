package com.yk.api.dataGatherer.dto;

import cn.hutool.json.JSONObject;
import com.yk.common.core.enums.DataTypeEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 建表的字段实体类
 *
 * @author lmx
 * @date 2023/10/27 14:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "建表的字段实体类")
public class Fields implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "字段名称")
    private String fieldName;
    @ApiModelProperty(value = "字段值")
    private Object fieldValue;
    @ApiModelProperty(value = "字段数据类型")
    private DataTypeEnum dataType;
    @ApiModelProperty(value = "字段字节大小")
    private Integer size;
    @ApiModelProperty(value = "变量名称")
    private String name;
    @ApiModelProperty(value = "是否存储")
    private Boolean storage;
    @ApiModelProperty(value = "是否展示")
    private Boolean show;
    @ApiModelProperty(value = "变量ID")
    private Long variableId;
    @ApiModelProperty(value = "0-模拟量 1-开关量")
    private String type;
    @ApiModelProperty(value = "计算公式")
    private String formula;
    @ApiModelProperty(value = "小数位")
    private Integer decimal;
    @ApiModelProperty(value = "变量单位")
    private String unit;
    @ApiModelProperty(value = "引用变量")
    private Long parentId;
    @ApiModelProperty(value = "引用变量")
    private String parentAddress;
    @ApiModelProperty(value = "数据类型 0-普通变量 1-差值变量 2-内部变量")
    private String variableDataType;
    @ApiModelProperty(value = "计算类型 0-周期 1-实时")
    private String computeType;
    @ApiModelProperty(value = "周期频率 0-日 1-周 2-月")
    private String cycleFrequency;
    @ApiModelProperty(value = "内部变量")
    private JSONObject interior;

    public Fields(Object fieldValue) {
        this.fieldValue = fieldValue;
    }

    public Fields(String fieldName, String dataType, Integer size) {
        this.fieldName = fieldName;
        //根据规则匹配字段数据类型
        switch (dataType.toLowerCase()) {
            case ("json"):
                this.dataType = DataTypeEnum.JSON;
                this.size = size;
                break;
            case ("string"):
            case ("nchar"):
                this.dataType = DataTypeEnum.NCHAR;
                this.size = size;
                break;
            case ("binary"):
                this.dataType = DataTypeEnum.BINARY;
                this.size = size;
                break;
            case ("int"):
                this.dataType = DataTypeEnum.INT;
                break;
            case ("long"):
            case ("bigint"):
                this.dataType = DataTypeEnum.BIGINT;
                break;
            case ("bool"):
                this.dataType = DataTypeEnum.BOOL;
                break;
            case ("decimal"):
                this.dataType = DataTypeEnum.DOUBLE;
                break;
            case ("timestamp"):
                if ("eventTime".equals(fieldName)) {
                    this.fieldName = "eventTime";
                }
                this.dataType = DataTypeEnum.TIMESTAMP;
                break;
            default:
        }
    }
}
