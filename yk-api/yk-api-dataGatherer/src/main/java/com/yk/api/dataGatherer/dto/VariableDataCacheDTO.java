package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 变量的缓存数据DTO
 *
 * @author lmx
 * @date 2023/10/27 14:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "变量的缓存数据DTO")
public class VariableDataCacheDTO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 变量名称
     */
    @ApiModelProperty(value = "变量值")
    private String value;

    /**
     * 小数位
     */
    @ApiModelProperty(value = "开始时间")
    private Date startDate;

    /**
     * 结束时间
     */
    @ApiModelProperty(value = "结束时间")
    private Date endDate;
}
