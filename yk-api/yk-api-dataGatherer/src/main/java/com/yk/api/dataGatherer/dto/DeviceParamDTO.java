package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lmx
 * @date 2023/11/16 16:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "设备参数")
public class DeviceParamDTO {

    @ApiModelProperty(value = "设备id")
    private String deviceId;

    @ApiModelProperty(value = "设备名称")
    private String deviceName;
}
