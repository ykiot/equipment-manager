package com.yk.api.dataGatherer.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 标签查询模型
 *
 * @author lmx
 * @date 2023/10/27 14:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "标签查询模型")
public class TagsSelectDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据库名称")
    private String dataBaseName;

    @ApiModelProperty(value = "超级表名称-模版id")
    private String stableName;

    @ApiModelProperty(value = "标签名称")
    private String tagsName;

    @ApiModelProperty(value = "开始时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    private String startTime;

    @ApiModelProperty(value = "结束时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    private String endTime;


    @ApiModelProperty(value = "设备ids")
    private List<String> deviceIds;

    @ApiModelProperty(value = "标签名称s")
    private List<String> tagsNames;
}
