package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * 创建超级表需要的入参的实体类
 *
 * @author lmx
 * @date 2023/10/27 14:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "创建超级表需要的入参的实体类")
public class SuperTableDTO extends BaseEntity {

    /**
     * 超级表的表结构（业务相关）
     * 第一个字段的数据类型必须为timestamp
     * 字符相关数据类型必须指定大小
     * 字段名称和字段数据类型不能为空
     */
    @ApiModelProperty(value = "普通变量的表结构")
    private List<Fields> ptFields;

    /**
     * 超级表的标签字段，可以作为子表在超级表里的标识
     * 字符相关数据类型必须指定大小
     * 字段名称和字段数据类型不能为空
     */
    @ApiModelProperty(value = "超级表的标签字段")
    private List<Fields> tagsFields;

    @ApiModelProperty(value = "差值变量的表结构")
    private List<Fields> czFields;

    @ApiModelProperty(value = "内部变量的表结构")
    private List<Fields> nbFields;

    @ApiModelProperty(value = "字段信息对象，超级表添加列时使用该属性")
    private Fields fields;
}
