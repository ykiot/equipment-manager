package com.yk.api.dataGatherer.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Optional;

/**
 * 超级表模型
 *
 * @author lmx
 * @date 2023/10/27 14:41
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductSuperTableModel implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss.SSS", timezone = "GMT+8")
    private Timestamp ts;

    private String superTableName;

    /**
     * columnsName,columnsProperty
     */
    private HashMap<Optional, Optional> columns;

    /**
     * tagsName,tagsProperty
     */
    private HashMap<Optional, Optional> tags;

}
