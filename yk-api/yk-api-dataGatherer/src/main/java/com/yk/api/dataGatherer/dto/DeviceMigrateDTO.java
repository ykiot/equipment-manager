package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author lmx
 * @date 2023/11/16 15:36
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "迁移设备数据DTO")
public class DeviceMigrateDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备id
     */
    private Long deviceId;

    /**
     * 变量模板id
     */
    private Long tempId;

    /**
     * 源变量模板id
     */
    private Long oldTempId;
}
