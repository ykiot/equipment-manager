package com.yk.api.dataGatherer.vo;

import com.yk.api.dataGatherer.dto.Fields;
import com.yk.common.core.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * 建表的字段实体类的vo类
 *
 * @author lmx
 * @date 2023/10/27 14:41
 */
@Data
public class FieldsVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 字段名称
     */
    private String fieldName;

    /**
     * 字段数据类型
     */
    private String dataType;

    /**
     * 字段字节大小
     */
    private Integer size;

    /**
     * @param fields 字段实体类
     * @return FieldsVo 字段实体vo类
     * @MethodDescription 字段实体类转为vo类
     * @author thinglinks
     * @Date 2021/12/28 13:48
     */
    public static FieldsVo fieldsTranscoding(Fields fields) throws SQLException {
        if (StringUtils.isBlank(fields.getFieldName())) {
            throw new SQLException("fieldName不能为 null");
        }
        FieldsVo fieldsVo = new FieldsVo();
        fieldsVo.setFieldName(fields.getFieldName());
        if (Objects.nonNull(fields.getDataType())){
            fieldsVo.setDataType(fields.getDataType().getDataType());
        }
        fieldsVo.setSize(fields.getSize());
        return fieldsVo;
    }

    /**
     * @param fieldsList 字段实体类集合
     * @return List<FieldsVo> 字段实体vo类集合
     * @MethodDescription 字段实体类集合转为vo类集合
     * @author thinglinks
     * @Date 2021/12/28 14:00
     */
    public static List<FieldsVo> fieldsTranscoding(List<Fields> fieldsList) throws SQLException {
        List<FieldsVo> fieldsVoList = new ArrayList<>();
        for (Fields fields : fieldsList) {
            fieldsVoList.add(fieldsTranscoding(fields));
        }
        return fieldsVoList;
    }
}
