package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * tdEngine的基础实体类
 *
 * @author lmx
 * @date 2023/10/27 14:40
 */
@Data
@ApiModel(description = "tdEngine的基础实体类")
public class BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 数据库名称
     */
    @ApiModelProperty(value = "数据库名称")
    private String dataBaseName;

    /**
     * 超级表名称
     */
    @ApiModelProperty(value = "超级表名称 模板id")
    private String superTableName;
}
