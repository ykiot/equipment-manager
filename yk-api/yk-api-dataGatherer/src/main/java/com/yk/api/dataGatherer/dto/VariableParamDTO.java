package com.yk.api.dataGatherer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author lmx
 * @date 2023/11/16 16:06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "变量参数")
public class VariableParamDTO {

    @ApiModelProperty(value = "变量名")
    private String name;

    @ApiModelProperty(value = "变量地址")
    private String url;
}
