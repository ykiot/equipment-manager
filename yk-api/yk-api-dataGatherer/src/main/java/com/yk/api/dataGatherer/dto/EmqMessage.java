package com.yk.api.dataGatherer.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * EMQX webhook消息
 * @author lmx
 * @date 2023/11/9 14:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmqMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 主题地址
     */
    private String peerhost;

    /**
     * 客户端id
     */
    private String clientid;

    /**
     * 消息内容
     */
    private String payload;

    /**
     * 主题
     */
    private String topic;
}
