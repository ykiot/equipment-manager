package com.yk.api.dataGatherer.dto;

import lombok.Data;

import java.util.List;

/**
 * 创建超级表的子表需要的入参的实体类
 *
 * @author lmx
 * @date 2023/10/27 14:42
 */
@Data
public class TableDTO extends BaseEntity {

    /**
     * 超级表普通列字段的值
     * 值需要与创建超级表时普通列字段的数据类型对应上
     */
    private List<Fields> schemaFieldValues;

    /**
     * 超级表标签字段的值
     * 值需要与创建超级表时标签字段的数据类型对应上
     */
    private List<Fields> tagsFieldValues;

    /**
     * 表名称
     */
    private String tableName;
}
