package com.yk.dataGatherer.vo;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.yk.api.system.dto.VariableStatusDTO;
import com.yk.common.excel.core.DateConverter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 变量VO
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description = "变量VO")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ExcelIgnoreUnannotated
public class VariableVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "变量Id")
    private String variableId;
    @ApiModelProperty(value = "变量名称")
    @ExcelProperty(value = "变量名称", index = 4)
    private String name;
    @ApiModelProperty(value = "变量类型 0-模拟量 1-开关量")
    private String type;
    @ApiModelProperty(value = "变量地址")
    @ExcelProperty(value = "变量地址", index = 5)
    private String address;
    @ApiModelProperty(value = "变量值")
    @ExcelProperty(value = "变量值", index = 2)
    private String value;
    @ApiModelProperty(value = "变量单位")
    @ExcelProperty(value = "单位", index = 3)
    private String unit;
    @ApiModelProperty(value = "设备id")
    private String deviceId;
    @ApiModelProperty(value = "代表状态")
    private List<VariableStatusDTO> indicatesStatus;
    @ApiModelProperty(value = "时间")
    @ExcelProperty(value = "数据时间", index = 1, converter = DateConverter.class)
    private Date ts;
    @ApiModelProperty(value = "设备名称")
    @ExcelProperty(value = "设备名称", index = 6)
    private String deviceName;
    @ExcelProperty(value = "序号", index = 0)
    @ApiModelProperty(value = "序号")
    private Integer index;
}