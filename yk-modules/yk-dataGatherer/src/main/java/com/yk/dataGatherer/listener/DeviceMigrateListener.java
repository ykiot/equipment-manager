package com.yk.dataGatherer.listener;

import com.yk.api.dataGatherer.dto.DeviceMigrateDTO;
import com.yk.common.core.constant.TdEngIneConstants;
import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.dataGatherer.mapper.TdEngineMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.Objects;

/**
 * 自定义报表监听器
 *
 * @author lmx
 * @date 2023/12/26 11:30
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DeviceMigrateListener {

    @Value("${spring.datasource.dynamic.datasource.master.dbName}")
    private String masterDataBaseName;
    private final TdEngineMapper tdEngineMapper;

    @RabbitListener(queues = QueueConstants.DEVICE_MIGRATE)
    @RabbitHandler
    public void onMessage(DeviceMigrateDTO dto) {
        try {
            Long deviceId = dto.getDeviceId();
            Long tempId = dto.getTempId();
            Long oldTempId = dto.getOldTempId();
            if (Objects.isNull(deviceId) || Objects.isNull(tempId) || Objects.isNull(oldTempId)) {
                return;
            }
            String tableName = TdEngIneConstants.TABLE_PREFIX + deviceId;
            String copyTableName = tableName + TdEngIneConstants.DEVICE_COPY;

            String sTableName = TdEngIneConstants.STABLE_PREFIX + oldTempId;
            String newSTableName = TdEngIneConstants.STABLE_PREFIX + tempId;

            // 获取创建表的sql
            Map<String, String> map = tdEngineMapper.getCreateTableSql(masterDataBaseName, tableName);
            String createTableSql = map.get("Create Table");
            String createTableCopySql = createTableSql
                    .replace("`" + tableName + "`", masterDataBaseName + ".`" + copyTableName + "`")
                    .replace("`" + sTableName + "`", masterDataBaseName + ".`" + newSTableName + "`");
            // 创建一个备份表
            tdEngineMapper.createTableCopy(createTableCopySql);
            // 迁移数据到备份表
            tdEngineMapper.deviceMigrate(masterDataBaseName, tableName, copyTableName);
            // 删除原表
            tdEngineMapper.delTableByName(masterDataBaseName, tableName);
            // 重新创建源表，并重新关联超级表
            String createTableOldSql = createTableSql
                    .replace("`" + tableName + "`", masterDataBaseName + ".`" + tableName + "`")
                    .replace("`" + sTableName + "`", masterDataBaseName + ".`" + newSTableName + "`");
            tdEngineMapper.createTableCopy(createTableOldSql);
            // 迁移备份表数据到源表
            tdEngineMapper.deviceMigrate(masterDataBaseName, copyTableName, tableName);
            // 删除备份表
            tdEngineMapper.delTableByName(masterDataBaseName, copyTableName);
        }catch (Exception e) {
            log.error("数据迁移异常", e.getMessage());
        }
    }
}
