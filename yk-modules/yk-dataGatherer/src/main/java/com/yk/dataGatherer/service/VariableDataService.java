package com.yk.dataGatherer.service;


/**
 * 多例。模拟量，开关量控制
 *
 * @author lmx
 * @date 2023/10/31 10:33
 */
public interface VariableDataService {


    /**
     * @param data    变量原始值
     * @param formula 变量公式
     * @param decimal 小数位数
     * @return
     */
    String dealWith(String data, String formula, Integer decimal);
}
