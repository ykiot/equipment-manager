package com.yk.dataGatherer.service;


import com.yk.api.dataGatherer.dto.DescribeDTO;
import com.yk.api.dataGatherer.dto.SelectDTO;
import com.yk.api.dataGatherer.dto.TableDTO;
import com.yk.api.dataGatherer.dto.TagsSelectDTO;
import com.yk.api.dataGatherer.vo.FieldsVo;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * TdEngine业务层
 *
 * @author lmx
 * @date 2023/10/27 15:30
 */
public interface TdEngineService {

    /**
     * 初始数据库
     *
     * @param dataBaseName 数据库名称
     */
    void createDateBase(String dataBaseName);

    /**
     * 创建超级表
     *
     * @param schemaFields   字段集合
     * @param tagsFields     tag字段集合
     * @param dataBaseName   数据库名称
     * @param superTableName 超级表名称
     */
    void createSuperTable(List<FieldsVo> schemaFields, List<FieldsVo> tagsFields, String dataBaseName, String superTableName);

    /**
     * 创建子表
     *
     * @param tableDto 入参的实体类
     */
    void createTable(TableDTO tableDto);

    /**
     * 删除子表
     *
     * @param tableDto 入参的实体类
     */
    void delTable(TableDTO tableDto);

    /**
     * 子表插入数据（可自动建表 参考https://docs.taosdata.com/taos-sql/insert/#%E6%8F%92%E5%85%A5%E8%AE%B0%E5%BD%95%E6%97%B6%E8%87%AA%E5%8A%A8%E5%BB%BA%E8%A1%A8）
     *
     * @param tableDto 入参的实体类
     */
    void insertData(TableDTO tableDto);

    /**
     * 根据时间查询
     *
     * @param selectDto 入参的实体类
     * @return 历史数据集合
     */
    List<Map<String, Object>> selectByTimesTamp(SelectDTO selectDto);

    /**
     * 根据变量和时间戳查询历史数据
     *
     * @param selectDto 入参的实体类
     * @return 历史数据集合
     */
    List<Map<String, Object>> selectByVariableAndTime(SelectDTO selectDto);

    /**
     * 分页查询
     *
     * @return 历史数据集合
     */
    List<Map<String, Object>> pageByVariableAndTime(Long pageNum, Long pageSize, SelectDTO selectDto);

    /**
     * 超级表新增列
     *
     * @param superTableName 超级表名称
     * @param fieldsVo       字段对象
     */
    void addColumnForSuperTable(String superTableName, FieldsVo fieldsVo);

    /**
     * 超级表查询列名
     *
     * @param dataBaseName   数据库名称
     * @param superTableName 超级表名称
     * @return 超级表的结构信息
     */
    List<DescribeDTO> getColumn(String dataBaseName, String superTableName);

    /**
     * 超级表删除列
     *
     * @param superTableName 超级表名称
     * @param fieldsVo       字段对象
     */
    void dropColumnForSuperTable(String superTableName, FieldsVo fieldsVo);

    /**
     * 根据时间统计数量
     *
     * @param selectDto 入参的实体类
     * @return 数量
     */
    Long getCountByTimesTamp(SelectDTO selectDto);

    /**
     * 最新一条历史数据
     *
     * @param selectDto 入参的实体类
     * @return 历史数据
     */
    Map<String, Object> getLastData(SelectDTO selectDto);

    /**
     * 检查表是否存在
     *
     * @param dataBaseName 数据库名称
     * @param tableName    可以为超级表名或普通表名
     * @return 1存在 0不存在
     */
    void checkTableExists(String dataBaseName, String tableName);

    /**
     * 根据超级表查询包含Tags的最新数据集合
     *
     * @param tagsSelectDTO Tags的入参对象
     * @return 历史数据集合
     */
    Map<String, Map<String, Object>> getLastDataByTags(TagsSelectDTO tagsSelectDTO);

    /**
     * 根据Tags-(deviceId)查询动态数据列
     *
     * @param tagsSelectDTO Tags的入参对象
     * @return 历史数据集合
     */
    List<Map<String, Object>> getDynamicDataByDeviceId(TagsSelectDTO tagsSelectDTO);

    /**
     * 根据时间范围删除历史数据
     *
     * @param selectDto 参数
     */
    void delDataByTime(SelectDTO selectDto);

    /**
     * 根据时间范围查询历史数据最早一条
     *
     * @param parentAddress 列名
     * @param dataBaseName  库名
     * @param tableName     表名
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @return 数据值
     */
    String selectLatestDataByTime(String parentAddress, String dataBaseName, String tableName, Date startTime, Date endTime);
}
