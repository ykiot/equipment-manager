package com.yk.dataGatherer.task;

import com.yk.api.dataGatherer.dto.SelectDTO;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.api.system.model.DeviceFeignService;
import com.yk.api.system.model.MessageFeignService;
import com.yk.common.core.constant.CacheConstants;
import com.yk.common.core.constant.NumberConstant;
import com.yk.common.core.constant.TdEngIneConstants;
import com.yk.common.core.domain.Result;
import com.yk.common.redis.service.RedisService;
import com.yk.dataGatherer.mapper.TdEngineMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;

import java.util.List;
import java.util.Map;

/**
 * 定时任务查询流量卡数据
 *
 * @author lmx
 * @date 2023/10/16 16:52
 */
@Slf4j
@RequiredArgsConstructor
//@Component
public class DeviceHistoryTask {

    private final TdEngineMapper tdEngineMapper;
    private final RedisService redisService;
    private final DeviceFeignService deviceFeignService;
    private final MessageFeignService messageFeignService;
    @Value("${spring.datasource.dynamic.datasource.master.dbName}")
    private String masterDataBaseName;
    @Scheduled(cron = "0 0 0/1 * * ? ")
    public void task() {
        try {
            Result<List<Long>> result = deviceFeignService.selectByOnlineDeviceId();
            if (Result.isError(result) || Result.isCollNull(result)) {
               return;
            }
            // 存储限制条数
            int size = 100000;
            List<Long> ids = result.getData();
            ids.forEach(id -> {
                SelectDTO selectDto = new SelectDTO();
                // 数据库名称
                selectDto.setDataBaseName(masterDataBaseName);
                // 表名称
                String tableName = TdEngIneConstants.TABLE_PREFIX + id;
                selectDto.setTableName(tableName);
                Map<String, Long> map = tdEngineMapper.getCountByTimestamp(selectDto);
                if (map == null) {
                    return;
                }
                Long count = map.get("count");
                if (count >= size){
                    // 发送通知
                    Result<DeviceDTO> resultD = deviceFeignService.selectById(id);
                    if (resultD.isSuccess() && resultD.getData() != null){
                        DeviceDTO data = resultD.getData();
                        messageFeignService.systemMessage("历史数据存储不足",
                                data.getCreatedBy(),
                                "您名下的设备[" + data.getDeviceName()+ "]数据已满100000条，请及时清理",
                                NumberConstant.ZERO_STR);
                    }
                    // 存值redis
                    redisService.setCacheObject(CacheConstants.DEVICE_HISTORY_KEY + tableName, Boolean.TRUE);
                }else {
                    redisService.setCacheObject(CacheConstants.DEVICE_HISTORY_KEY + tableName, Boolean.FALSE);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
