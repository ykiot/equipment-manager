package com.yk.dataGatherer.vo;

import cn.hutool.core.date.DateUtil;
import com.yk.api.system.dto.VariableDTO;
import com.yk.common.core.constant.TdEngIneConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * 实时数据VO
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description = "实时数据VO")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LastDataVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备id")
    private Long deviceId;

    @ApiModelProperty(value = "时间")
    private Date ts;

    @ApiModelProperty(value = "数据Map")
    private Map<String, String> lastData;

    @ApiModelProperty(value = "数据对象")
    private List<VariableVO> variableData;

    public LastDataVO(Date ts, List<VariableDTO> dataList) {
        this.ts = ts;
        Map<String, String> dataMap = dataList.stream().collect(Collectors.toMap(VariableDTO::getAddress, VariableDTO::getValue));
        dataMap.remove(TdEngIneConstants.TS);
        this.lastData = dataMap;
    }

    public LastDataVO(Long deviceId, Object ts, List<VariableVO> variableData) {
        this.deviceId = deviceId;
        this.ts = DateUtil.parseDateTime(ts.toString());
        this.variableData = variableData;
    }
}