package com.yk.dataGatherer.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.yk.common.core.constant.NumberConstant;
import com.yk.dataGatherer.service.VariableDataService;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlScript;
import org.apache.commons.jexl3.MapContext;
import org.apache.commons.jexl3.internal.Engine;

import java.util.Objects;


/**
 * 模拟量
 *
 * @author lmx
 * @date 2023/10/31 10:33
 */
public class AnalogImpl implements VariableDataService {


    @Override
    public String dealWith(String variableValue, String formula, Integer decimal) {
        // 默认保留2位小数
        if (Objects.isNull(decimal)){
            decimal = NumberConstant.TWO;
        }
        Double correctionValue = Double.parseDouble(StringUtils.isEmpty(variableValue) ? "0" : variableValue);
        if (StrUtil.isEmpty(formula)){
            variableValue = String.format("%." + decimal + "f", (correctionValue));
            return abnormalData(variableValue);
        }
        try {
            // 例：a*X+b；X代表变量，a b代表常数； 可用符合：+ - * / （）
            JexlEngine engine = new Engine();
            JexlContext context = new MapContext();
            context.set("X", correctionValue);
            JexlScript script = engine.createScript(formula);
            double result = Double.parseDouble(script.execute(context).toString());
            variableValue = String.format("%." + decimal + "f", (result));
        } catch (Exception e) {
            variableValue = "NaN";
        }
        return abnormalData(variableValue);
    }

    /**
     * 异常数据处理
     */
    private String abnormalData(String variableValue){
        if ("-0".equals(variableValue)){
            variableValue = "0";
        }
        return variableValue;
    }
}