package com.yk.dataGatherer.listener;

import com.yk.api.dataGatherer.dto.TableDTO;
import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.common.redis.service.RedisService;
import com.yk.dataGatherer.service.TdEngineService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * mqtt消息监听
 *
 * @author lmx
 * @date 2023/10/21 16:31
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class DeviceHistoryQueueListener {

    private final TdEngineService tdEngineService;
    private final RedisService redisService;

    @Value("${spring.datasource.dynamic.datasource.master.dbName}")
    private String masterDataBaseName;

    /**
     * 历史数据入库
     */
    @RabbitListener(queues = QueueConstants.DEVICE_HISTORY)
    @RabbitHandler
    public void onMessage(TableDTO tableDto) {
        if (Objects.isNull(tableDto)) {
            return;
        }
        try {
            /*Boolean cache = redisService.getCacheObject(CacheConstants.DEVICE_HISTORY_KEY + tableDto.getTableName());
            if (Objects.nonNull(cache) && cache){
                return;
            }*/
            tableDto.setDataBaseName(masterDataBaseName);
            tdEngineService.insertData(tableDto);
        } catch (Exception e) {
            log.error("queues-DEVICE_HISTORY-insertDataError：{}", e.getMessage());
        }
    }
}