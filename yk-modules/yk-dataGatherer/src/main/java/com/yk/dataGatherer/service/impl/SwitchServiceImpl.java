package com.yk.dataGatherer.service.impl;


import com.yk.common.core.constant.MqttConstants;
import com.yk.dataGatherer.service.VariableDataService;

/**
 * 开关量
 *
 * @author lmx
 * @date 2023/10/31 10:33
 */
public class SwitchServiceImpl implements VariableDataService {

    @Override
    public String dealWith(String variableValue, String formula, Integer decimal) {
        //开关量,必须是0,1，或者true,false
        if (variableValue.equalsIgnoreCase(MqttConstants.TRUE)) {
            variableValue = "1";
        } else if (variableValue.equalsIgnoreCase(MqttConstants.FALSE)) {
            variableValue = "0";
        }
        return variableValue;
    }
}