package com.yk.dataGatherer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 数据采集服务
 *
 * @author lmx
 * @date 2023/10/19 18:03
 */
@EnableFeignClients("com.yk.api")
@EnableDiscoveryClient
@SpringBootApplication
public class DataGathererApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataGathererApplication.class, args);
    }
}