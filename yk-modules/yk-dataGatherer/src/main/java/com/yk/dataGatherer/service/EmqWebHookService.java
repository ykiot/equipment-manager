package com.yk.dataGatherer.service;

import cn.hutool.json.JSONObject;
import com.yk.api.system.dto.GatewayDTO;

/**
 * EmqWebHookService
 *
 * @author lmx
 * @date 2023/11/9 17:19
 */
public interface EmqWebHookService {

    /**
     * 数据解析
     *
     * @param mqttStatus 操作类型
     * @param data       数据
     * @param gateway    网关
     * @param clientId   客户端id
     */
    void fetchData(String mqttStatus, JSONObject data, GatewayDTO gateway, String clientId);

}
