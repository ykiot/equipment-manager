package com.yk.dataGatherer.listener;

import com.yk.common.core.dto.SendWebSocketDTO;
import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.dataGatherer.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * webSocket发送监听
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
@Component
public class SendWebSocketListener {

	@RabbitListener(queues = QueueConstants.SEND_WEBSOCKET)
	@RabbitHandler
	public void onMessage(SendWebSocketDTO dto) {
		WebSocketServer.sendMessage(dto.getId(), dto.getMsg());
	}
}