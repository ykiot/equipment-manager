package com.yk.dataGatherer.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 变量数据VO
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description = "变量数据VO")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VariableDataVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "变量Id")
    private String variableId;
    @ApiModelProperty(value = "时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date ts;
    @ApiModelProperty(value = "变量数据集合")
    private List<VariableVO> variableVOList;

    public VariableDataVO(String variableId, List<VariableVO> variableVOList) {
        this.variableId = variableId;
        this.variableVOList = variableVOList;
    }

    public VariableDataVO(Date ts, List<VariableVO> variableVOList) {
        this.ts = ts;
        this.variableVOList = variableVOList;
    }
}