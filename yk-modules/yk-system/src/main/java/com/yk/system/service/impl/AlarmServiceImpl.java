package com.yk.system.service.impl;

import com.yk.common.core.utils.LoginHelper;
import com.yk.system.entity.Alarm;
import com.yk.system.mapper.AlarmMapper;
import com.yk.system.service.AlarmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * 报警规则表 服务类
 *
 * @author lmx
 * @since 2023-11-21
 */
@Service
public class AlarmServiceImpl extends ServiceImpl<AlarmMapper, Alarm> implements AlarmService {

    @Override
    public Alarm selectById(Long id) {
        return baseMapper.getById(id);
    }

    @Override
    public Integer getPageNum(Long id, Long pageSize) {
        Long userId = LoginHelper.getLoginUserId();
        String pageNum = baseMapper.getPageNum(id, pageSize, userId);
        BigDecimal number = new BigDecimal(pageNum);
        return number.intValue();
    }

    @Override
    public List<Alarm> getByInformType(String informType) {
        return baseMapper.getByInformType(informType);
    }
}