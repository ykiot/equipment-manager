package com.yk.system.mapper;

import com.yk.system.entity.Announcement;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 公告 Mapper 接口
 * @author lmx
 * @since 2024-01-04
 */
@Mapper
public interface AnnouncementMapper extends BaseMapper<Announcement>{

}