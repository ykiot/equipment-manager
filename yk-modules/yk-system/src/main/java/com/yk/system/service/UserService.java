package com.yk.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.UserDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.system.entity.User;

import java.util.List;


/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface UserService extends IService<User> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<User> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(List<User> list);

    /**
     * 校验用户名称是否唯一
     *
     * @param user 用户信息
     * @return 结果 true：唯一  false：不唯一
     */
    boolean checkUserNameUnique(User user);

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户信息
     * @return 结果 true：唯一  false：不唯一
     */
    boolean checkPhoneUnique(User user);

    /**
     * 通过手机号检验用户，成功后返回用户
     *
     * @param phone 手机号
     * @return 用户信息
     */
    User checkPhoneByUser(String phone);

    /**
     * 校验用户是否允许操作
     *
     * @param user 用户信息
     */
    void checkUserAllowed(User user);

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户ID
     * @return 结果
     */
    int deleteUserByIds(List<Long> userIds);

    /**
     * 通过用户名查询用户
     *
     * @param userName 用户名
     * @return 用户对象信息
     */
    User selectUserByUserName(String userName);

    /**
     * 通过手机号查询用户
     *
     * @param phone 手机号
     * @return 用户对象信息
     */
    User selectUserByPhone(String phone);

    /**
     * 分页查询用户信息
     *
     * @param pageParam 分页参数
     * @return 用户信息集合信息
     */
    IPage<UserDTO> queryPage(BasePageQuery<UserDTO> pageParam);

}
