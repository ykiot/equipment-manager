package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * sys_card_alarm
 * 流量卡信息表
 *
 * @author lmx
 * @since 2024-02-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_card_alarm")
@ApiModel(value = "CardAlarm", description = "流量卡信息表")
public class CardAlarm {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "物联网卡号")
    @TableField("cardno")
    private String cardno;
    @ApiModelProperty(value = "使用流量(-1表示未知，单位KB，时间当月)")
    @TableField("used")
    private String used;
    @ApiModelProperty(value = "	剩余流量(单位KB)")
    @TableField("surplus")
    private String surplus;
    @ApiModelProperty(value = "报警内容")
    @TableField("alarm_message")
    private String alarmMessage;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    private Date createdAt;
    @ApiModelProperty(value = "0-报警 1-恢复")
    @TableField("status")
    private String status;
}