package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.ExcelHistory;

/**
 * 文件记录表 服务类
 *
 * @author lmx
 * @since 2023-11-20
 */
public interface ExcelHistoryService extends IService<ExcelHistory> {

    /**
     * 保存Excel记录
     *
     * @param name       报表名称
     * @param dataSource 数据源
     * @param dataRadius 数据维度
     * @param startTime  开始时间
     * @param endTime    结束时间
     * @param createdBy  创建人
     * @param status     状态 0-未开始 1-生成中 2-成功
     * @return
     */
    ExcelHistory saveExcelHistory(String name, String dataSource, String dataRadius, String startTime,
                                  String endTime, Long createdBy, String status);
}