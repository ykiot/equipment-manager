package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.AlarmUserDevice;

/**
 * 报警接收配置表 服务类
 * @author lmx
 * @since 2024-02-27
 */
public interface AlarmUserDeviceService extends IService<AlarmUserDevice>{

}