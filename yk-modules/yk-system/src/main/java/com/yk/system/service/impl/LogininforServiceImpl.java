package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Logininfor;
import com.yk.system.mapper.LogininforMapper;
import com.yk.system.service.LogininforService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:34
 */
@Service
public class LogininforServiceImpl extends ServiceImpl<LogininforMapper, Logininfor> implements LogininforService {

    @Override
    public int updateBatch(List<Logininfor> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Logininfor> list) {
        return baseMapper.batchInsert(list);
    }
}
