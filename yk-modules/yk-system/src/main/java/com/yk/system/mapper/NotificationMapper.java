package com.yk.system.mapper;

import com.yk.system.entity.Notification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 系统通知 Mapper 接口
 * @author lmx
 * @since 2024-01-05
 */
@Mapper
public interface NotificationMapper extends BaseMapper<Notification>{

}