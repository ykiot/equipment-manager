package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_announcement
 * 公告
 *
 * @author lmx
 * @since 2024-01-04
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_announcement")
@ApiModel(value = "Announcement", description = "公告")
public class Announcement implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "公告id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "公告标题")
    @TableField(value = "title")
    private String title;
    @ApiModelProperty(value = "公告内容")
    @TableField(value = "content")
    private String content;
    @ApiModelProperty(value = "图片")
    @TableField(value = "url")
    private String url;
    @ApiModelProperty(value = "是否启用 0-启用 1-停用")
    @TableField(value = "status")
    private String status;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "更新者")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private String updatedBy;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField(value = "del_flag")
    private String delFlag;
    @ApiModelProperty(value = "排序")
    @TableField(value = "sort")
    private Integer sort;
    @ApiModelProperty(value = "跳转链接")
    @TableField(value = "link")
    private String link;


}