package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Alarm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 报警规则表 Mapper 接口
 *
 * @author lmx
 * @since 2023-11-21
 */
@Mapper
public interface AlarmMapper extends BaseMapper<Alarm> {

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       网关ID
     * @param pageSize 条数
     * @param userId   用户ID
     * @return
     */
    String getPageNum(@Param("id") Long id, @Param("pageSize") Long pageSize, @Param("userId") Long userId);

    /**
     * 根据播报类型查询
     *
     * @param informType 播报类型
     * @return
     */
    List<Alarm> getByInformType(@Param("informType") String informType);

    /**
     * 根据ID查询（不过滤删除的数据）
     *
     * @param id
     * @return
     */
    Alarm getById(@Param("id") Long id);
}