package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_video
 * 摄像头
 *
 * @author lmx
 * @since 2023-11-07
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_video")
@ApiModel(value = "Video", description = "摄像头")
public class Video implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "关联场景")
    @TableField("group_id")
    private Long groupId;
    @ApiModelProperty(value = "摄像头名称")
    @TableField("name")
    private String name;
    @ApiModelProperty(value = "摄像头地址")
    @TableField("address")
    private String address;
    @ApiModelProperty(value = "AppKey")
    @TableField("appkey")
    private String appkey;
    @ApiModelProperty(value = "Secret")
    @TableField("secret")
    private String secret;
    @ApiModelProperty(value = "设备序列号")
    @TableField("number")
    private String number;
    @ApiModelProperty(value = "通道")
    @TableField("passage")
    private String passage;
    @ApiModelProperty(value = "验证码")
    @TableField("code")
    private String code;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新人")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField("del_flag")
    private String delFlag;


}