package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.AlarmHistoryDTO;
import com.yk.system.entity.AlarmHistory;
import com.yk.system.vo.AlarmDeviceStatisticsChartsVO;
import com.yk.system.vo.AlarmDeviceStatisticsVO;
import com.yk.system.vo.AlarmGroupStatisticsVO;

import java.util.List;

/**
 * 报警历史表 服务类
 *
 * @author lmx
 * @since 2023-11-21
 */
public interface AlarmHistoryService extends IService<AlarmHistory> {

    /**
     * 报警信息处理
     *
     * @param param
     */
    void dealWithAlarmMessage(AlarmHistoryDTO param);

    /**
     * 根据设备按天统计报警数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> statisticsByDevice(AlarmHistoryDTO dto);

    /**
     * 小程序按小时统计
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> statisticsHourByDevice(AlarmHistoryDTO dto);

    /**
     * 小程序统计饼图-统计设备
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsChartsVO> statisticsByDevice2Charts(AlarmHistoryDTO dto);

    /**
     * 小程序统计饼图-统计设备下变量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsChartsVO> statisticsByDeviceId2Charts(AlarmHistoryDTO dto);

    /**
     * 小程序按天统计设备、报警数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> app2statisticsByDevice(AlarmHistoryDTO dto);

    /**
     * 根据场景ID统计各设备报警数量
     *
     * @param dto 参数
     * @return 返回值
     */
    List<AlarmGroupStatisticsVO> statisticsByGroup(AlarmHistoryDTO dto);
}