package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Post;
import com.yk.system.mapper.PostMapper;
import com.yk.system.service.PostService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
public class PostServiceImpl extends ServiceImpl<PostMapper, Post> implements PostService {

    @Override
    public int updateBatch(List<Post> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Post> list) {
        return baseMapper.batchInsert(list);
    }
}
