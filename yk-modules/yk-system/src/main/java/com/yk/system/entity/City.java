package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 城市
 * @author lmx
 * @date 2023/10/24 9:02
 */
@ApiModel(description="城市")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ct_city")
public class City implements Serializable {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="")
    private Long id;

    /**
     * 城市名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="城市名称")
    private String name;

    /**
     * 省id
     */
    @TableField(value = "province_id")
    @ApiModelProperty(value="省id")
    private Integer provinceId;

    private static final long serialVersionUID = 1L;
}