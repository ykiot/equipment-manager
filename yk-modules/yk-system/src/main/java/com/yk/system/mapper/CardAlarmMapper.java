package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.CardAlarm;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 流量卡信息表 Mapper 接口
 * @author lmx
 * @since 2024-02-21
 */
@Mapper
public interface CardAlarmMapper extends BaseMapper<CardAlarm>{

    /**
     * 查询最后一条记录
     * @param cardno
     */
    CardAlarm selectLast(@Param("cardno") String cardno);
}