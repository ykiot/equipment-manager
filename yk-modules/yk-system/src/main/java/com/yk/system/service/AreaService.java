package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Area;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16$ 17:22$
 */
public interface AreaService extends IService<Area>{

    /**
     * 批量更新
     * @param list
     * @return
     */
    int updateBatch(List<Area> list);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int batchInsert(List<Area> list);
}
