package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.UserRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:42
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRole> {
    int updateBatch(List<UserRole> list);

    int batchInsert(@Param("list") List<UserRole> list);
}