package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 设备编号生成
 * @author lmx
 * @date 2023/11/10 11:24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GenerateNumberVO", description = "设备编号生成")
public class GenerateNumberVO {

    @ApiModelProperty(value = "编号")
    private String number;
    @ApiModelProperty(value = "密码")
    private String password;
}
