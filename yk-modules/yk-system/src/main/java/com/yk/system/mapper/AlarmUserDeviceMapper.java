package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.AlarmUserDevice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 报警接收配置表 Mapper 接口
 *
 * @author lmx
 * @since 2024-02-27
 */
@Mapper
public interface AlarmUserDeviceMapper extends BaseMapper<AlarmUserDevice> {

    /**
     * 根据用户ID和设备ID查询报警接收配置
     *
     * @param userId     用户id
     * @param deviceId   设备id
     * @param variableId 变量id
     * @param alarmId    报警规则id
     * @return 报警接收配置
     */
    AlarmUserDevice selectByUserAndDevice(@Param("userId") Long userId,
                                          @Param("deviceId") Long deviceId,
                                          @Param("variableId") Long variableId,
                                          @Param("alarmId") Long alarmId);
}