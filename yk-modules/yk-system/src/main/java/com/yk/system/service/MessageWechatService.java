package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.MessageWechat;

/**
 * 消息 服务类
 * @author ${context.author}
 * @since 2024-04-15
 */
public interface MessageWechatService extends IService<MessageWechat>{

}