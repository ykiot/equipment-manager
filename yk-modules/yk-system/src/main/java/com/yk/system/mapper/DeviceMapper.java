package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.system.entity.Device;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface DeviceMapper extends BaseMapper<Device> {
    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Device> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Device> list);

    /**
     * 查询未关联场景的设备
     *
     * @param userId 用户id
     * @return
     */
    List<DeviceDTO> selectNoGroup2Device(@Param("userId") Long userId);

    /**
     * 查询未关联场景的设备
     *
     * @param param 参数
     * @return
     */
    List<DeviceDTO> selectNoGroup2DeviceByParam(@Param("query") DeviceDTO param);

    /**
     * 分页查询
     *
     * @param objectPage
     * @param param
     * @return
     */
    IPage<DeviceDTO> queryPage(Page<Device> objectPage, @Param("query") DeviceDTO param);

    /**
     * 查询当前用户可查看的设备
     *
     * @param userId 用户ID
     * @return
     */
    List<DeviceDTO> selectRole2Device(@Param("userId") Long userId);

    /**
     * 查询列表
     *
     * @param param 参数
     * @return 列表
     */
    List<DeviceDTO> selectDeviceList(@Param("query") DeviceDTO param);
}