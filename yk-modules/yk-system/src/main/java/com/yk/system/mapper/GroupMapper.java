package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.GroupDTO;
import com.yk.system.entity.Group;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface GroupMapper extends BaseMapper<Group> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Group> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Group> list);

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       网关ID
     * @param pageSize 条数
     * @param userId   用户ID
     * @return
     */
    String getPageNum(@Param("id") Long id, @Param("pageSize") Long pageSize, @Param("userId") Long userId);

    /**
     * app查询列表
     *
     * @param dto
     * @return
     */
    List<GroupDTO> appList(@Param("query") GroupDTO dto);
}