package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.api.system.dto.GroupDTO;
import com.yk.system.entity.GroupDevice;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/11/24 17:22
 */
@Mapper
public interface GroupDeviceMapper extends BaseMapper<GroupDevice> {
    /**
     * 批量增加
     *
     * @param list 数据集
     * @return 受影响的行数
     */
    int batchInsert(@Param("list") List<GroupDevice> list);

    /**
     * 根据场景id查询设备
     *
     * @param groupId 场景Id
     * @param userId  用户Id
     * @return 场景关联设备信息
     */
    List<DeviceDTO> listByGroupId(@Param("groupId") Long groupId, @Param("userId") Long userId);

    /**
     * 根据设备id查询场景
     *
     * @param deviceId 设备Id
     * @return 场景关联设备信息
     */
    List<GroupDTO> listByDeviceId(@Param("deviceId") Long deviceId, @Param("userId") Long userId);

    /**
     * 根据参数查询设备列表
     *
     * @param param 参数
     * @return
     */
    List<DeviceDTO> listByGroupIdByParam(@Param("query") DeviceDTO param);

    /**
     * 根据 id 获取按特定顺序排列的最后一个 GroupDevice 对象
     *
     * @param id 要检索的 GroupDevice 对象的 ID
     * @return 具有指定 ID 的 GroupDevice 对象，如果它存在且是按特定顺序的最后一个
     */
    GroupDevice getLastByOrderDesc(@Param("groupId") Long id,@Param("userId") Long userId);
}