package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.CardAlarm;
import com.yk.system.mapper.CardAlarmMapper;
import com.yk.system.service.CardAlarmService;
import org.springframework.stereotype.Service;

/**
 * 流量卡信息表 服务类
 * @author lmx
 * @since 2024-02-21
 */
@Service
public class CardAlarmServiceImpl extends ServiceImpl<CardAlarmMapper, CardAlarm> implements CardAlarmService {

}