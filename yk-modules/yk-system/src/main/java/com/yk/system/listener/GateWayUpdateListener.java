package com.yk.system.listener;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yk.common.core.constant.NumberConstant;
import com.yk.common.core.dto.GateWayUpdateDTO;
import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.system.entity.Device;
import com.yk.system.entity.Gateway;
import com.yk.system.entity.OperLog;
import com.yk.system.service.DeviceService;
import com.yk.system.service.GatewayService;
import com.yk.system.service.OperLogService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * 网关信息更新监听
 *
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class GateWayUpdateListener {

    private final GatewayService gateWayService;
    private final DeviceService deviceService;
    private final OperLogService operLogService;

    @RabbitListener(queues = QueueConstants.GATEWAY_UPDATE)
    @RabbitHandler
    public void onMessage(GateWayUpdateDTO dto) {
        if (Objects.isNull(dto) || Objects.isNull(dto.getId())) {
            return;
        }
        Gateway gateway = gateWayService.getById(dto.getId());
        if (Objects.isNull(gateway)){
            return;
        }
        // 状态
        if (Objects.nonNull(dto.getStatus())) {
            if (dto.getStatus().equals(NumberConstant.ONE)) {
                gateway.setStatus(Boolean.TRUE);
            } else {
                gateway.setStatus(Boolean.FALSE);
            }
        }
        // 卡号
        if (StrUtil.isNotEmpty(dto.getCcId())) {
            gateway.setCardNumber(dto.getCcId());
        }
        // 客户端id
        if (StrUtil.isNotEmpty(dto.getClientId())) {
            gateway.setClientId(dto.getClientId());
            if (Objects.isNull(dto.getStatus())) {
                gateway.setStatus(Boolean.TRUE);
            }
        }
        gateWayService.updateById(gateway);
    }

    /**
     * 离线状态更新
     */
    @RabbitListener(queues = QueueConstants.GATEWAY_OFFLINE_UPDATE)
    @RabbitHandler
    public void onMessageOfflineUpdate(GateWayUpdateDTO dto) {
        if (Objects.isNull(dto) || StrUtil.isEmpty(dto.getClientId())) {
            return;
        }
        LambdaQueryWrapper<Gateway> lambda = new LambdaQueryWrapper<>();
        lambda.eq(Gateway::getClientId, dto.getClientId());
        Gateway gateway = gateWayService.getOne(lambda);
        if (Objects.isNull(gateway)){
            return;
        }
        gateway.setStatus(Boolean.FALSE);
        gateWayService.updateById(gateway);

        // 关联的所有在线设备全部设置离线
        List<Device> list = deviceService.list(new LambdaQueryWrapper<Device>()
                .eq(Device::getGatewayId, gateway.getId())
                .eq(Device::getStatus, Boolean.TRUE));
        if (CollUtil.isEmpty(list)){
            return;
        }
        list.forEach(it -> {
            it.setStatus(Boolean.FALSE);
            deviceService.updateById(it);
            // 下线日志
            OperLog operLog = new OperLog();
            operLog.setDeviceId(it.getId());
            operLog.setOperName(it.getDeviceName());
            operLog.setStatus(NumberConstant.ZERO);
            operLog.setCreatedAt(new Date());
            operLog.setCreatedBy(1L);
            operLog.setTitle("设备下线");
            operLog.setBusinessType(NumberConstant.FIVE);
            operLog.setMessage(it.getDeviceName() + "设备下线");
            operLogService.save(operLog);
        });
    }
}