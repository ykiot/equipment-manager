package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.AlarmHistoryDTO;
import com.yk.system.entity.AlarmHistory;
import com.yk.system.vo.AlarmDeviceStatisticsChartsVO;
import com.yk.system.vo.AlarmDeviceStatisticsVO;
import com.yk.system.vo.AlarmGroupStatisticsVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

/**
 * 报警历史表 Mapper 接口
 *
 * @author lmx
 * @since 2023-11-21
 */
@Mapper
public interface AlarmHistoryMapper extends BaseMapper<AlarmHistory> {

    /**
     * 根据设备按天统计数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> statisticsByDevice(AlarmHistoryDTO dto);

    /**
     * 小程序按天统计数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> app2statisticsByDevice(AlarmHistoryDTO dto);

    /**
     * 小程序按小时统计
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsVO> statisticsHourByDevice(AlarmHistoryDTO dto);

    /**
     * 小程序根据按天统计设备报警数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsChartsVO> statisticsByDevice2Charts(AlarmHistoryDTO dto);

    /**
     * 小程序根据设备按天统计变量报警数量
     *
     * @param dto
     * @return
     */
    List<AlarmDeviceStatisticsChartsVO> statisticsByDeviceId2Charts(AlarmHistoryDTO dto);

    /**
     * 根据时间、设备统计数量
     *
     * @param dto
     * @return
     */
    Integer countByDevice(AlarmHistoryDTO dto);

    /**
     * 根据场景ID统计各设备报警数量
     *
     * @param deviceIdList 设备ID列表
     * @param startTime    开始时间
     * @param endTime      结束时间
     * @return
     */
    List<AlarmGroupStatisticsVO> statisticsByGroup(@Param("deviceIdList") List<Long> deviceIdList, @Param("startTime") Date startTime, @Param("endTime") Date endTime);
}