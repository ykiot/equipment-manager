
package com.yk.system.convert;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import com.yk.api.system.dto.AlarmDTO;
import com.yk.api.system.dto.ConditionDTO;
import com.yk.system.entity.Alarm;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * 设备表 对象转换
 *
 * @author lmx
 * @since 2023-11-21
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AlarmConvert {

    /**
     * dto to entity
     *
     * @param dto dto
     * @return entity
     */
    Alarm dto2Entity(AlarmDTO dto);

    /**
     * entity to dto
     *
     * @param entity entity
     * @return dto
     */
    AlarmDTO entity2Dto(Alarm entity);

    /**
     * update entity
     *
     * @param dto    dto
     * @param entity entity
     */
    void updateEntity(AlarmDTO dto, @MappingTarget Alarm entity);

    /**
     * str to list
     *
     * @param src src
     * @return list
     */
    default List<String> str2List(String src) {
        if (StrUtil.isEmpty(src)) {
            return CollUtil.newArrayList();
        }
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    /**
     * list to str
     *
     * @param src src
     * @return str
     */
    default String list2Str(List<String> src) {
        if (CollUtil.isEmpty(src)) {
            return null;
        }
        return String.join(",", src);
    }

    /**
     * conditionDTO to str
     * @param dto
     * @return
     */
    default String co2Str(ConditionDTO dto) {
        if (Objects.isNull(dto)) {
            return null;
        }
        return JSONUtil.toJsonStr(dto);
    }

    /**
     * str to conditionDTO
     * @param src
     * @return
     */
    default ConditionDTO str2Co(String src) {
        if (StrUtil.isEmpty(src)) {
            return null;
        }
        return JSONUtil.toBean(src, ConditionDTO.class);
    }

}
