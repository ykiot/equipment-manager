
package com.yk.system.convert;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.yk.api.system.dto.DeviceRoleDTO;
import com.yk.system.entity.DeviceRole;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;

/**
 * 设备权限表 对象转换
 *
 * @author lmx
 * @since 2023-11-08
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface DeviceRoleConvert {

    /**
     * dto to entity
     *
     * @param dto dto
     * @return entity
     */
    DeviceRole dto2Entity(DeviceRoleDTO dto);

    /**
     * entity to dto
     *
     * @param entity entity
     * @return dto
     */
    DeviceRoleDTO entity2Dto(DeviceRole entity);

    /**
     * update entity
     *
     * @param dto    dto
     * @param entity entity
     */
    void updateEntity(DeviceRoleDTO dto, @MappingTarget DeviceRole entity);

    /**
     * str to list
     *
     * @param src src
     * @return list
     */
    default List<String> str2List(String src) {
        if (StrUtil.isEmpty(src)) {
            return CollUtil.newArrayList();
        }
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    /**
     * list to str
     *
     * @param src src
     * @return str
     */
    default String list2Str(List<String> src) {
        if (CollUtil.isEmpty(src)) {
            return null;
        }
        return String.join(",", src);
    }

}
