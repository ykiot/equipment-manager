package com.yk.system.controller;

import cn.hutool.core.date.DateUtil;
import com.yk.common.core.domain.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.dromara.x.file.storage.core.FileInfo;
import org.dromara.x.file.storage.core.FileStorageService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

/**
 * 文件存储 yk-system
 * FileHistoryServiceImpl 自动保存记录
 * @author lmx
 * @date 2023/10/24 17:45
 */
@Api(tags = "文件存储")
@RestController
@RequiredArgsConstructor
@RequestMapping("/file")
public class FileController {

    private final FileStorageService fileStorageService;

    /**
     * 上传文件
     */
    @PostMapping(value = "/upload")
    @ApiOperation("上传文件")
    public Result<FileInfo> upload(@RequestPart MultipartFile file) {
        FileInfo fileInfo = fileStorageService
                .of(file)
                .setPath(DateUtil.today() + "/")
                .upload();
        return Result.data(fileInfo);
    }

    /**
     * 上传文件，成功返回文件 url
     */
    @PostMapping(value = "/upload2")
    @ApiOperation("上传文件，成功返回文件 url")
    public Result<String> upload2(@RequestPart MultipartFile file) {
        FileInfo fileInfo = fileStorageService
                .of(file)
                .setPath(DateUtil.today() + "/")
                .upload();
        if (Objects.isNull(fileInfo)) {
            return Result.fail("上传失败！");
        }
        return Result.ok(fileInfo.getUrl());
    }

    /**
     * 上传图片，压缩图片
     */
    @PostMapping("/upload-image")
    @ApiOperation("上传图片，压缩图片")
    public Result<String> uploadImage(@RequestPart MultipartFile file) {
        FileInfo fileInfo = fileStorageService.of(file)
                .setPath(DateUtil.today() + "/")
                .image(img -> img.size(1000, 1000))
                .thumbnail(th -> th.size(200, 200))
                .upload();
        if (Objects.isNull(fileInfo)) {
            return Result.fail("上传失败！");
        }
        return Result.data(fileInfo.getUrl());
    }

    /**
     * 下载文件
     */
    @PostMapping("/download")
    @ApiOperation("下载文件")
    public void download(@RequestParam String url, HttpServletResponse response) {
        try (ServletOutputStream out = response.getOutputStream()) {
            response.setHeader("Content-Disposition", "attachment; filename = file");
            fileStorageService.download(url).outputStream(out);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 删除图片
     */
    @PostMapping("/delete")
    @ApiOperation("删除图片")
    public Result<String> delete(@RequestParam String url) {
        boolean delete = fileStorageService.delete(url);
        if (!delete) {
            return Result.fail("删除失败！");
        }
        return Result.ok("删除成功！");
    }
}
