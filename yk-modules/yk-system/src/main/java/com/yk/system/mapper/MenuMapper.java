package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:29
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {
    int updateBatch(List<Menu> list);

    int batchInsert(@Param("list") List<Menu> list);
}