package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Variable;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface VariableMapper extends BaseMapper<Variable> {

    /**
     * 批量更新
     *
     * @param list 变量列表
     * @return
     * @throws Exception
     */
    int updateBatch(List<Variable> list) throws Exception;

    /**
     * 批量插入
     *
     * @param list 变量列表
     * @return
     * @throws Exception
     */
    int batchInsert(@Param("list") List<Variable> list) throws Exception;


    /**
     * 根据模板id查询变量列表
     *
     * @param templateId 模版id
     * @return
     */
    List<Variable> queryDtoByTemplateId(@Param("templateId") Long templateId);

    /**
     * 根据模板id查询变量列表
     *
     * @param templateId 模版id
     * @return
     */
    List<Variable> queryByTemplateId(@Param("templateId") Long templateId);

    /**
     * 根据ids查询变量列表 忽略删除条件
     *
     * @param variableIds 变量ids
     * @return 变量列表
     */
    List<Variable> getByIdsIgnoreDelete(@Param("variableIds") List<Long> variableIds);
}