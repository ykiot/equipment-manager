package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.GroupConfiguration;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface GroupConfigurationService extends IService<GroupConfiguration> {


    /**
     * 批量插入
     * @param list 数据集
     * @return 受影响的行数
     */
    int batchInsert(List<GroupConfiguration> list);

    /**
     * 根据组id查询组配置数量
     * @param groupId 场景id
     * @return 数量
     */
    Integer countByGroupId(Long groupId);

    /**
     * 根据场景id查询组配置
     * @param groupId 场景id
     * @return 场景关联组态信息
     */
    GroupConfiguration selectByGroupId(Long groupId);

    /**
     * 根据组态id查询组配置
     * @param configurationId 组态id
     * @return 场景关联组态信息
     */
    GroupConfiguration selectByConfigurationId(Long configurationId);
}
