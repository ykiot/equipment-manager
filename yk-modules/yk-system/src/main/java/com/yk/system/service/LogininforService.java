package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Logininfor;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:34
 */
public interface LogininforService extends IService<Logininfor>{


    int updateBatch(List<Logininfor> list);

    int batchInsert(List<Logininfor> list);
}
