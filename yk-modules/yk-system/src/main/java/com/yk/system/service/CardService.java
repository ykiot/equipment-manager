package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Card;

/**
 * 流量卡信息表 服务类
 * @author lmx
 * @since 2024-02-20
 */
public interface CardService extends IService<Card>{

    /**
     * 保存流量卡
     * @param ccId 流量卡号
     * @param type 流量卡类型 0-管理端 1-客户端
     * @return 流量卡信息
     */
    Card saveCard(String ccId, String type);

    /**
     * 调用api更新流量卡数据
     * 接口文档：         http://hywx.xjict.com/jk/
     * data		        订单对象
     * cardno		    物联网卡号
     * used		        使用流量(-1表示未知，单位KB，时间当月)
     * surplus		    剩余流量(单位KB)
     * state		    物联网卡状态
     * ispick	        官方实名状态0未知1未实名2已实名
     * c_status		    是否认证1未认证 2 认证 3补录 4补录待审核
     * expired_at		卡有效期
     * canused		    卡可使用量
     * message		    物联网卡状态描述
     * gprs_status		开机状态 0未知 1开机 2关机
     * power_status		在线状态 0未知 1在线 2离线
     * display_status	1待激活 2正常 3异常 4预销户 5销户
     * jingtime		    静默期时间
     * offthenet		达量断网1是2否 单独断网a是b否
     * shouchong_time   首充时间
     */
    void searchCard(Card card);
}