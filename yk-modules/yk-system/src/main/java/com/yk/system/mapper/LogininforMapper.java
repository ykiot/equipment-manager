package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Logininfor;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:23
 */
@Mapper
public interface LogininforMapper extends BaseMapper<Logininfor> {
    int updateBatch(List<Logininfor> list);

    int batchInsert(@Param("list") List<Logininfor> list);
}