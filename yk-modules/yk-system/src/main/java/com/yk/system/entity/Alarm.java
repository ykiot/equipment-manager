package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 报警规则表
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_alarm")
@ApiModel(value = "Alarm", description = "报警规则表")
public class Alarm implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "触发器名称")
    @TableField(value = "name")
    private String name;
    @ApiModelProperty(value = "触发器类型 0-设备 1-变量模版")
    @TableField(value = "type")
    private String type;
    @ApiModelProperty(value = "触发对象")
    @TableField(value = "target_id")
    private Long targetId;
    @ApiModelProperty(value = "变量id")
    @TableField(value = "variable_id")
    private Long variableId;
    @ApiModelProperty(value = "触发条件")
    @TableField(value = "rule")
    private String rule;
    @ApiModelProperty(value = "是否推送 0-是 1-否")
    @TableField(value = "inform")
    private String inform;
    @ApiModelProperty(value = "推送类型 0-公众号 1-短信 2-播报")
    @TableField(value = "inform_type")
    private String informType;
    @ApiModelProperty(value = "是否启用 true-启用")
    @TableField(value = "enable")
    private Boolean enable;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField(value = "del_flag")
    private String delFlag;

    @ApiModelProperty(value = "自定义消息")
    @TableField(value = "message")
    private String message;
}