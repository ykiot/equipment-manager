package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.GroupCamera;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface GroupCameraMapper extends BaseMapper<GroupCamera> {
    int updateBatch(List<GroupCamera> list);

    int batchInsert(@Param("list") List<GroupCamera> list);
}