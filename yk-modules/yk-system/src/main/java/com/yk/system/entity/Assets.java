package com.yk.system.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_assets
 * 资产
 *
 * @author lmx
 * @since 2023-11-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yk_assets")
@ApiModel(value = "Assets", description = "资产")
public class Assets implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "归属客户")
    @TableField("customer_id")
    private Long customerId;
    @ApiModelProperty(value = "网关id")
    @TableField(value = "gateway_id", updateStrategy = FieldStrategy.IGNORED)
    private Long gatewayId;
    @ApiModelProperty(value = "网关编号")
    @TableField("gateway_number")
    private String gatewayNumber;
    @ApiModelProperty(value = "网关主题")
    @TableField("topic")
    private String topic;
    @ApiModelProperty(value = "网关密码")
    @TableField("password")
    private String password;
    @ApiModelProperty(value = "售出时间")
    @TableField("sale_time")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date saleTime;
    @ApiModelProperty(value = "创建时间=入库时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "更新人")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField("del_flag")
    private String delFlag;

    @ApiModelProperty(value = "是否添加到平台（0代表添加 2代表不添加）")
    @TableField("to_pc")
    private String toPc;
    @ApiModelProperty(value = "0-已出售1-未出售2-返厂维修")
    @TableField("status")
    private String status;
    @ApiModelProperty(value = "物联网卡号")
    @TableField("cc_id")
    private String ccId;
    @TableField(value = "model")
    @ApiModelProperty(value="网关型号")
    @ExcelProperty(value = "网关型号")
    private String model;
    @TableField(value = "bz")
    @ApiModelProperty(value="备注")
    private String bz;
    @TableField(value = "version")
    @ApiModelProperty(value="版本")
    private String version;
}