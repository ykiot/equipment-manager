package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.VideoDTO;
import com.yk.system.entity.Video;

import java.util.List;

/**
 * 摄像头 服务类
 *
 * @author lmx
 * @since 2023-11-07
 */
public interface VideoService extends IService<Video> {

    /**
     * 根据场景id查询
     *
     * @param groupId 场景ID
     * @return 摄像头集合
     */
    List<VideoDTO> listByGroupId(Long groupId);

    /**
     * token 获取
     *
     * @param videoDTOS
     */
    void getToken(List<VideoDTO> videoDTOS);

    /**
     * token 获取
     *
     * @param dto
     */
    void getToken(VideoDTO dto);

    /**
     * 根据参数查询列表
     *
     * @param param 参数
     * @return
     */
    List<VideoDTO> listByParam(VideoDTO param);
}