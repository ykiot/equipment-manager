package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.VariableTemplate;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface VariableTemplateService extends IService<VariableTemplate> {


    /**
     * 批量更新
     *
     * @param list
     * @return
     * @throws Exception
     */
    int updateBatch(List<VariableTemplate> list) throws Exception;

    /**
     * 新增 批量插入
     *
     * @param list
     * @return
     * @throws Exception
     */
    int batchInsert(List<VariableTemplate> list) throws Exception;

}
