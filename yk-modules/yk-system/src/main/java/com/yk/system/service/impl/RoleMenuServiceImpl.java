package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.RoleMenu;
import com.yk.system.mapper.RoleMenuMapper;
import com.yk.system.service.RoleMenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuMapper, RoleMenu> implements RoleMenuService {

    @Override
    public int updateBatch(List<RoleMenu> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<RoleMenu> list) {
        return baseMapper.batchInsert(list);
    }
}
