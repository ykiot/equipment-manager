package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.AlarmUserDevice;
import com.yk.system.mapper.AlarmUserDeviceMapper;
import com.yk.system.service.AlarmUserDeviceService;
import org.springframework.stereotype.Service;

/**
 * 报警接收配置表 服务类
 *
 * @author lmx
 * @since 2024-02-27
 */
@Service
public class AlarmUserDeviceServiceImpl extends ServiceImpl<AlarmUserDeviceMapper, AlarmUserDevice>
        implements AlarmUserDeviceService {

}