package com.yk.system.controller;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.ExcelHistoryDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.common.core.domain.PageResult;
import com.yk.common.core.domain.Result;
import com.yk.common.core.utils.LoginHelper;
import com.yk.system.convert.ExcelHistoryConvert;
import com.yk.system.entity.ExcelHistory;
import com.yk.system.entity.FileHistory;
import com.yk.system.mapper.FileHistoryMapper;
import com.yk.system.service.ExcelHistoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.dromara.x.file.storage.core.FileStorageService;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;


/**
 * 文件记录表 yk-system
 *
 * @author lmx
 * @since 2023-11-20
 */
@Api(tags = "文件记录表")
@RestController
@RequiredArgsConstructor
@RequestMapping("/excelHistory")
public class ExcelHistoryController {

    private final ExcelHistoryService excelHistoryService;
    private final FileHistoryMapper fileHistoryMapper;
    private final ExcelHistoryConvert excelHistoryConvert;
    private final FileStorageService fileStorageService;

    @GetMapping("/getById/{id}")
    @ApiOperation("文件记录表-查询单个")
    public Result<ExcelHistory> getById(@PathVariable(value = "id") Long id) {
        return Result.data(excelHistoryService.getById(id));
    }

    @GetMapping("/deleteById/{id}")
    @ApiOperation("文件记录表-删除")
    public Result<Boolean> deleteById(@PathVariable(value = "id") Long id) {
        excelHistoryService.removeById(id);
        FileHistory fileHistory = fileHistoryMapper.selectByObjectId(id);
        if (Objects.nonNull(fileHistory)){
            fileStorageService.delete(fileHistory.getUrl());
        }
        return Result.ok();
    }

    @PostMapping("/page")
    @ApiOperation("文件记录表-分页查询")
    public PageResult<ExcelHistoryDTO> page(@RequestBody BasePageQuery<ExcelHistoryDTO> pageParam) {
        ExcelHistoryDTO param = pageParam.getParam();
        LambdaQueryWrapper<ExcelHistory> lambda = new QueryWrapper<ExcelHistory>().lambda();
        buildCondition(lambda, param);
        IPage<ExcelHistoryDTO> page = excelHistoryService.page(new Page<>(pageParam.getPageNum(), pageParam.getPageSize()), lambda)
                .convert(excelHistoryConvert::entity2Dto);
        if (CollUtil.isNotEmpty(page.getRecords())){
            page.getRecords().forEach(it -> {
                FileHistory fileHistory = fileHistoryMapper.selectByObjectId(it.getId());
                if (Objects.nonNull(fileHistory)){
                    it.setUrl(fileHistory.getUrl());
                }
            });
        }
        return PageResult.success(page.getRecords(), page.getTotal());
    }

    /**
     * 构造查询条件
     */
    private void buildCondition(LambdaQueryWrapper<ExcelHistory> lambda, ExcelHistoryDTO param) {
        lambda.eq(ExcelHistory::getCreatedBy, LoginHelper.getLoginUserId());
        if (Objects.nonNull(param)) {
            lambda.between(Objects.nonNull(param.getStartTime()), ExcelHistory::getCreatedAt, param.getStartTime(), param.getEndTime());
        }
        lambda.orderByDesc(ExcelHistory::getCreatedAt);
    }


}