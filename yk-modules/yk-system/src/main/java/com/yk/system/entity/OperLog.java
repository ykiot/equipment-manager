package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 操作日志记录
 * @author lmx
 * @date 2023/10/24 9:30
 */
@ApiModel(description="操作日志记录")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_oper_log")
public class OperLog implements Serializable {
    /**
     * 日志主键
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="日志主键")
    private Long id;

    /**
     * 设备id 0：代表操作日志 具体设备ID：设备上下线日志
     */
    @TableField(value = "device_id")
    @ApiModelProperty(value="设备id")
    private Long deviceId;

    /**
     * 模块标题
     */
    @TableField(value = "title")
    @ApiModelProperty(value="模块标题")
    private String title;

    /**
     * 业务类型（0其它 1新增 2修改 3删除 4设备上线 5设备下线）
     */
    @TableField(value = "business_type")
    @ApiModelProperty(value="业务类型（0其它 1新增 2修改 3删除 4设备上线 5设备下线 7网关转让 8控制下发）")
    private Integer businessType;

    /**
     * 方法名称
     */
    @TableField(value = "`method`")
    @ApiModelProperty(value="方法名称")
    private String method;

    /**
     * 请求方式
     */
    @TableField(value = "request_method")
    @ApiModelProperty(value="请求方式")
    private String requestMethod;

    /**
     * 操作类别（0其它 1后台用户 2手机端用户）
     */
    @TableField(value = "operator_type")
    @ApiModelProperty(value="操作类别（0其它 1后台用户 2手机端用户）")
    private Integer operatorType;

    /**
     * 操作人员
     */
    @TableField(value = "oper_name")
    @ApiModelProperty(value="操作人员")
    private String operName;

    /**
     * 部门名称
     */
    @TableField(value = "dept_name")
    @ApiModelProperty(value="部门名称")
    private String deptName;

    /**
     * 请求URL
     */
    @TableField(value = "oper_url")
    @ApiModelProperty(value="请求URL")
    private String operUrl;

    /**
     * 主机地址
     */
    @TableField(value = "oper_ip")
    @ApiModelProperty(value="主机地址")
    private String operIp;

    /**
     * 操作地点
     */
    @TableField(value = "oper_location")
    @ApiModelProperty(value="操作地点")
    private String operLocation;

    /**
     * 请求参数
     */
    @TableField(value = "oper_param")
    @ApiModelProperty(value="请求参数")
    private String operParam;

    /**
     * 返回参数
     */
    @TableField(value = "json_result")
    @ApiModelProperty(value="返回参数")
    private String jsonResult;

    /**
     * 操作状态（0正常 1异常）
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="操作状态（0正常 1异常）")
    private Integer status;

    /**
     * 错误消息
     */
    @TableField(value = "error_msg")
    @ApiModelProperty(value="错误消息")
    private String errorMsg;

    /**
     * 操作内容
     */
    @TableField(value = "message")
    @ApiModelProperty(value="操作内容")
    private String message;

    /**
     * 操作时间
     */
    @TableField(value = "created_at",fill = FieldFill.INSERT)
    @ApiModelProperty(value="操作时间")
    private Date createdAt;

    /**
     * 创建人 1：上下线日志 具体值：操作日志
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建人")
    private Long createdBy;

    private static final long serialVersionUID = 1L;
}