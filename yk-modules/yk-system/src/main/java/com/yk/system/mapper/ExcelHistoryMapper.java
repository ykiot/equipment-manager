package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.ExcelHistory;
import org.apache.ibatis.annotations.Mapper;

/**
 * 文件记录表 Mapper 接口
 * @author lmx
 * @since 2023-11-20
 */
@Mapper
public interface ExcelHistoryMapper extends BaseMapper<ExcelHistory> {

}