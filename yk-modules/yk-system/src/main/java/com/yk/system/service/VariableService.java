package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Variable;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface VariableService extends IService<Variable> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Variable> list) throws Exception;

    /**
     * 根据模板id查询变量列表
     *
     * @param templateId 模版id
     * @return
     */
    List<Variable> queryDtoByTemplateId(Long templateId);
}
