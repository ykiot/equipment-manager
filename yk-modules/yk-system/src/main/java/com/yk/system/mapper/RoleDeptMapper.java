package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.RoleDept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:34
 */
@Mapper
public interface RoleDeptMapper extends BaseMapper<RoleDept> {
    int updateBatch(List<RoleDept> list);

    int batchInsert(@Param("list") List<RoleDept> list);
}