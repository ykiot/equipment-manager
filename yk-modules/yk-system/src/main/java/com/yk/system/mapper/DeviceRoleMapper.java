package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.DeviceRoleAlarmDTO;
import com.yk.api.system.dto.DeviceRoleDTO;
import com.yk.system.entity.Device;
import com.yk.system.entity.DeviceRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 设备权限表 Mapper 接口
 *
 * @author lmx
 * @since 2023-11-08
 */
@Mapper
public interface DeviceRoleMapper extends BaseMapper<DeviceRole> {


    /**
     * 小程序根据时间分页-管理权限查询
     *
     * @param deviceId 设备ID
     */
    List<DeviceRoleDTO> pageByTime(@Param("deviceId") Long deviceId);

    /**
     * 根据id查询忽略删除未删除
     */
    DeviceRole getByIdIgnoreDel(@Param("id") Long id);

    /**
     * 根据id更新已删除
     */
    void updateById2Del(DeviceRole deviceRole);

    /**
     * 分页查询-报警接收配置
     */
    IPage<DeviceRoleAlarmDTO> page2Alarm(Page<Object> objectPage, @Param("query") DeviceRoleAlarmDTO param);

    /**
     * 根据用户、权限类型查询设备
     *
     * @param userId 用户id
     * @param types  权限类型
     * @return 设备列表
     */
    List<Device> selectDeviceByUserIdAndType(@Param("userId") Long userId, @Param("types") List<Integer> types);

    /**
     * 批量更新
     *
     * @param ds 设备权限列表
     */
    void updateBatch(List<DeviceRole> ds);

    /**
     * 根据设备id查询设备权限
     *
     * @param deviceId 设备id
     * @return 设备权限列表
     */
    List<DeviceRole> selectByDeviceId(@Param("deviceId") Long deviceId);
}