package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.api.system.dto.GroupDTO;
import com.yk.system.entity.GroupDevice;

import java.util.List;

/**
 * @author lmx
 * @date 2023/11/24 17:22
 */
public interface GroupDeviceService extends IService<GroupDevice> {

    /**
     * 批量增加
     *
     * @param list 数据集
     * @return 影响行数
     */
    int batchInsert(List<GroupDevice> list);

    /**
     * 根据场景id查询设备
     *
     * @param groupId 场景id
     * @param userId  用户id
     * @return 场景关联设备信息
     */
    List<DeviceDTO> listByGroupId(Long groupId, Long userId);

    /**
     * 根据场景id查询设备
     *
     * @param param 参数
     * @return 设备列表
     */
    List<DeviceDTO> listByGroupIdByParam(DeviceDTO param);

    /**
     * 根据设备id查询场景
     *
     * @param deviceId 设备id
     * @return 场景关联设备信息
     */
    List<GroupDTO> listByDeviceId(Long deviceId);

    /**
     * 根据设备id查询场景
     *
     * @param deviceId 设备id
     * @param userId   用户id
     * @return 场景关联设备信息
     */
    List<GroupDTO> listByDeviceIdAndUserId(Long deviceId, Long userId);

    /**
     * 删除旧关联，重新关联设备
     *
     * @param id       场景id
     * @param userId   用户id
     * @param groupIds 场景id集合
     */
    void relevancyDevice(Long id, Long userId, List<Long> groupIds);
}
