package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Post;

import java.util.List;
    /**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface PostService extends IService<Post>{


    int updateBatch(List<Post> list);

    int batchInsert(List<Post> list);

}
