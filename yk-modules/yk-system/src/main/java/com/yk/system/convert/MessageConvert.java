
package com.yk.system.convert;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.yk.api.system.dto.MessageDTO;
import com.yk.system.entity.Message;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;

/**
 * 消息 对象转换
 * @author lmx
 * @since 2023-12-16
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface MessageConvert {

    /**
     * dto to entity
     *
     * @param dto dto
     * @return entity
     */
    Message dto2Entity(MessageDTO dto);

    /**
     * entity to dto
     *
     * @param entity entity
     * @return dto
     */
    MessageDTO entity2Dto(Message entity);

    /**
     * update entity
     *
     * @param dto    dto
     * @param entity entity
     */
    void updateEntity(MessageDTO dto, @MappingTarget Message entity);

    /**
     * str to list
     *
     * @param src src
     * @return list
     */
    default List<String> str2List(String src) {
        if (StrUtil.isEmpty(src)) {
            return CollUtil.newArrayList();
        }
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    /**
     * list to str
     *
     * @param src src
     * @return str
     */
    default String list2Str(List<String> src) {
        if (CollUtil.isEmpty(src)) {
            return null;
        }
        return String.join(",", src);
    }

}
