package com.yk.system.listener;

import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.system.entity.Card;
import com.yk.system.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 设备信息更新监听
 *
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class SearchCardListener {

    private final CardService cardService;

    @RabbitListener(queues = QueueConstants.SEARCH_CARD)
    @RabbitHandler
    public void onMessage(Card card) {
        cardService.searchCard(card);
    }
}