package com.yk.system.mapper;

import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.UserWx;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户-微信关联表 Mapper 接口
 * @author lmx
 * @since 2023-11-29
 */
@Mapper
public interface UserWxMapper extends BaseMapper<UserWx>{

    /**
     * 根据openId查询用户信息
     * @param openId
     * @return
     */
    UserDTO selectUserByOpenId(@Param("openId") String openId);
}