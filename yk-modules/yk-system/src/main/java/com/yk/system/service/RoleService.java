package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.Role;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:54
 */
public interface RoleService extends IService<Role> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Role> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(List<Role> list);

    /**
     * 获取用户、权限
     *
     * @param username
     * @return
     */
    UserDTO getUserInfo(String username);
}
