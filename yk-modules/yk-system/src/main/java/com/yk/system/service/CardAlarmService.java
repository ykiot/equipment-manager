package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.CardAlarm;

/**
 * 流量卡信息表 服务类
 * @author lmx
 * @since 2024-02-21
 */
public interface CardAlarmService extends IService<CardAlarm>{

}