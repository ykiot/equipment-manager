package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 系统访问记录
 * @author lmx
 * @date 2023/10/24 9:23
 */
@ApiModel(description="系统访问记录")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_logininfor")
public class Logininfor implements Serializable {
    /**
     * 访问ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="访问ID")
    private Long id;

    /**
     * 用户账号
     */
    @TableField(value = "user_name")
    @ApiModelProperty(value="用户账号")
    private String userName;

    /**
     * 登录IP地址
     */
    @TableField(value = "ipaddr")
    @ApiModelProperty(value="登录IP地址")
    private String ipaddr;

    /**
     * 登录地点
     */
    @TableField(value = "login_location")
    @ApiModelProperty(value="登录地点")
    private String loginLocation;

    /**
     * 浏览器类型
     */
    @TableField(value = "browser")
    @ApiModelProperty(value="浏览器类型")
    private String browser;

    /**
     * 操作系统
     */
    @TableField(value = "os")
    @ApiModelProperty(value="操作系统")
    private String os;

    /**
     * 登录状态（0成功 1失败）
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="登录状态（0成功 1失败）")
    private String status;

    /**
     * 提示消息
     */
    @TableField(value = "msg")
    @ApiModelProperty(value="提示消息")
    private String msg;

    /**
     * 访问时间
     */
    @TableField(value = "login_time")
    @ApiModelProperty(value="访问时间")
    private Date loginTime;

    private static final long serialVersionUID = 1L;
}