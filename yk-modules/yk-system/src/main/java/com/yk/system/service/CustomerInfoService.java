package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.CustomerInfoDTO;
import com.yk.system.entity.CustomerInfo;
import com.yk.system.vo.CustomerInfoRdStatisticsVO;

import java.util.List;

/**
 * 客户信息 服务类
 * @author lmx
 * @since 2023-11-10
 */
public interface CustomerInfoService extends IService<CustomerInfo>{

    /**
     * 检查属性
     * @param dto
     */
    void check(CustomerInfoDTO dto);

    /**
     * 客户地区分布统计
     * @return
     */
    List<CustomerInfoRdStatisticsVO> rdStatistics();

    /**
     * 根据手机号查询公司名称
     * @param phone 手机号
     * @return 公司名称
     */
    String getCompanyNameByPhone(String phone);
}