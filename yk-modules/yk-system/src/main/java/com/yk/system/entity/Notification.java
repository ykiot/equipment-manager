package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_notification
 * 系统通知
 *
 * @author lmx
 * @since 2024-01-05
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_notification")
@ApiModel(value = "Notification", description = "系统通知")
public class Notification implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "通知id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "通知标题")
    @TableField(value = "title")
    private String title;
    @ApiModelProperty(value = "通知内容")
    @TableField(value = "content")
    private String content;
    @ApiModelProperty(value = "发送时间")
    @TableField(value = "send_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date sendAt;
    @ApiModelProperty(value = "状态 0-未发送 1-已发送 2-发送失败")
    @TableField(value = "status")
    private String status;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField(value = "del_flag")
    private String delFlag;


}