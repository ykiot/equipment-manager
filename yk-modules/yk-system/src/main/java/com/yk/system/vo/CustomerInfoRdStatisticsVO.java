package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 客户地区分布统计
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CustomerInfoRdStatisticsVO", description = "客户地区分布统计VO")
public class CustomerInfoRdStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "地区名称")
    private String name;
    @ApiModelProperty(value = "数量")
    private Integer number;
}