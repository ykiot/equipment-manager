package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.DeviceUserDetails;

/**
 * 用户和角色关联表 服务类
 * @author lmx
 * @since 2024-06-18
 */
public interface DeviceUserDetailsService extends IService<DeviceUserDetails>{

}