package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 变量
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description = "变量")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "vb_variable")
public class Variable implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键id")
    private Long id;

    /**
     * 变量名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value = "变量名称")
    private String name;

    /**
     * 变量类型 0-模拟量 1-开关量
     */
    @TableField(value = "`type`")
    @ApiModelProperty(value = "变量类型 0-模拟量 1-开关量")
    private String type;

    /**
     * 数据类型 0-普通变量 1-差值变量
     */
    @TableField(value = "data_type")
    @ApiModelProperty(value = "数据类型 0-普通变量 1-差值变量 2-内部变量")
    private String dataType;

    /**
     * 计算类型 0-周期 1-实时
     */
    @TableField(value = "compute_type")
    @ApiModelProperty(value = "计算类型 0-周期 1-实时")
    private String computeType;

    /**
     * 周期频率 0-日 1-周 2-月
     */
    @TableField(value = "cycle_frequency")
    @ApiModelProperty(value = "周期频率 0-日 1-周 2-月")
    private String cycleFrequency;

    /**
     * 引用变量
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(value = "引用变量")
    private Long parentId;

    /**
     * 引用变量地址
     */
    @TableField(value = "parent_address")
    @ApiModelProperty(value = "引用变量地址")
    private String parentAddress;

    /**
     * 变量地址
     */
    @TableField(value = "address")
    @ApiModelProperty(value = "变量地址")
    private String address;

    /**
     * 变量单位
     */
    @TableField(value = "unit")
    @ApiModelProperty(value = "变量单位")
    private String unit;

    /**
     * 代表状态
     */
    @TableField(value = "Indicates_status")
    @ApiModelProperty(value = "代表状态")
    private String indicatesStatus;

    /**
     * 采集数据修正
     */
    @TableField(value = "formula")
    @ApiModelProperty(value = "采集数据修正")
    private String formula;

    /**
     * 写数据数据修正
     */
    @TableField(value = "write_formula")
    @ApiModelProperty(value = "写数据数据修正")
    private String writeFormula;

    /**
     * 小数位
     */
    @TableField(value = "`decimal`")
    @ApiModelProperty(value = "小数位")
    private Integer decimal;

    /**
     * 是否展示
     */
    @TableField(value = "`show`")
    @ApiModelProperty(value = "是否展示 0-不展示 1-展示")
    private String show;

    /**
     * 是否存储 0-不存储 1-存储
     */
    @TableField(value = "`storage`")
    @ApiModelProperty(value = "是否存储 0-不存储 1-存储")
    private String storage;

    /**
     * 是否控制
     */
    @TableField(value = "control")
    @ApiModelProperty(value = "是否控制 0-不控制 1-控制")
    private String control;

    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    /**
     * 创建人
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    /**
     * 更新人
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value = "更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    private String delFlag;

    @TableField(value = "read_max")
    @ApiModelProperty(value = "读取最大值")
    private Float readMax;

    @TableField(value = "write_max")
    @ApiModelProperty(value = "写入最大值")
    private Float writeMax;

    @TableField(value = "read_min")
    @ApiModelProperty(value = "读取最小值")
    private Float readMin;

    @TableField(value = "write_min")
    @ApiModelProperty(value = "写入最小值")
    private Float writeMin;

    @TableField(value = "interior")
    @ApiModelProperty(value = "内部变量")
    private String interior;

    @TableField(value = "reset")
    @ApiModelProperty(value = "是否复位 0-否 2-是")
    private String reset;

    @TableField(value = "reset_time")
    @ApiModelProperty(value = "复位时间（秒）")
    private Integer resetTime;

    @TableField(value = "reset_command")
    @ApiModelProperty(value = "复位指令")
    private String resetCommand;
}