package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Dept;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:15
 */
@Mapper
public interface DeptMapper extends BaseMapper<Dept> {
    int updateBatch(List<Dept> list);

    int batchInsert(@Param("list") List<Dept> list);
}