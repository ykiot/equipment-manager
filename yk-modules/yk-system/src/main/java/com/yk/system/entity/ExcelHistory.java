package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_excel_history
 * 文件记录表
 *
 * @author lmx
 * @since 2023-11-20
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_excel_history")
@ApiModel(value = "ExcelHistory", description = "文件记录表")
public class ExcelHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "报表id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "报表名称")
    @TableField("name")
    private String name;
    @ApiModelProperty(value = "数据源")
    @TableField("data_source")
    private String dataSource;
    @ApiModelProperty(value = "数据维度")
    @TableField("data_radius")
    private String dataRadius;
    @ApiModelProperty(value = "数据开始时间")
    @TableField("start_time")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date startTime;
    @ApiModelProperty(value = "数据结束时间")
    @TableField("end_time")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date endTime;
    @ApiModelProperty(value = "状态 0-未开始 1-生成中 2-成功 3-失败")
    @TableField("status")
    private String status;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;

}