package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Configuration;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author lmx
 * @date 2023/10/24 9:44
 */
@Mapper
public interface ConfigurationMapper extends BaseMapper<Configuration> {
    int updateBatch(List<Configuration> list);

    int batchInsert(@Param("list") List<Configuration> list);
}