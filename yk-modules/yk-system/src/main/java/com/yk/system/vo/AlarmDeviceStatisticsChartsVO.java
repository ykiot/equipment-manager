package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 报警-根据设备按天统计统计-饼图VO
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AlarmDeviceStatisticsChartsVO", description = "根据设备按天统计统计-饼图VO")
public class AlarmDeviceStatisticsChartsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备ID")
    private Long deviceId;
    @ApiModelProperty(value = "变量ID")
    private Long variableId;
    @ApiModelProperty(value = "设备名称")
    private String deviceName;
    @ApiModelProperty(value = "变量名称")
    private String variableName;
    @ApiModelProperty(value = "报警次数")
    private Integer alarmCount;
    @ApiModelProperty(value = "总报警次数")
    private Integer count;
    @ApiModelProperty(value = "占比")
    private String percentage;
}