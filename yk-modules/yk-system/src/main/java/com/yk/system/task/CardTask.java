package com.yk.system.task;

import com.yk.system.service.CardService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 定时任务查询流量卡数据
 *
 * @author lmx
 * @date 2023/10/16 16:52
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class CardTask {

    private final CardService cardService;

    @Scheduled(cron = "0 0 0/1 * * ? ")
    public void task() {
        try {
            cardService.searchCard(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
