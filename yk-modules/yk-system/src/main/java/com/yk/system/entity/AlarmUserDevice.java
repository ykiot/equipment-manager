package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


/**
 * tb_alarm_user_device
 * 报警接收配置表
 *
 * @author lmx
 * @since 2024-02-27
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_alarm_user_device")
@ApiModel(value = "AlarmUserDevice", description = "报警接收配置表")
public class AlarmUserDevice {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "报警规则id")
    @TableField("alarm_id")
    private Long alarmId;
    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Long userId;
    @ApiModelProperty(value = "设备id")
    @TableField("device_id")
    private Long deviceId;
    @ApiModelProperty(value = "变量id")
    @TableField("variable_id")
    private Long variableId;
    @ApiModelProperty(value = "推送类型 0-公众号 1-短信 2-播报")
    @TableField("inform_type")
    private String informType;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;


}