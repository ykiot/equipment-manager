package com.yk.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 设备控制DTO
 *
 * @author lmx
 * @date 2023/12/8 13:34
 */
@Data
@ApiModel(value = "控制设备参数")
public class ControlDTO {
    @ApiModelProperty(value = "设备ID")
    @NotNull(message = "设备ID不能为空")
    private Long deviceId;
    @ApiModelProperty(value = "变量ID")
    @NotNull(message = "变量ID不能为空")
    private Long variableId;
    @ApiModelProperty(value = "控制参数")
    @NotBlank(message = "控制参数不能为空")
    @Digits(integer = 10, fraction = 4, message = "最多10位整数和4位小数")
    private String param;
}
