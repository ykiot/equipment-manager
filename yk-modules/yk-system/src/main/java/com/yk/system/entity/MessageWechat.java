package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * sys_message_wechat
 * 消息
 *
 * @author lmx
 * @since 2024-04-15
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_message_wechat")
@ApiModel(value = "MessageWechat", description = "消息")
public class MessageWechat implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "公众号id")
    @TableField(value = "target_id")
    private String targetId;
    @ApiModelProperty(value = "消息标题")
    @TableField(value = "title")
    private String title;
    @ApiModelProperty(value = "消息内容")
    @TableField(value = "content")
    private String content;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;


}