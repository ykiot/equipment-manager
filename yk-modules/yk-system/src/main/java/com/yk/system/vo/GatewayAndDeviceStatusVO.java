package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 网关、设备状态VO
 *
 * @author lmx
 * @date 2023/11/8 16:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GatewayAndDeviceStatusVO", description = "网关、设备状态VO")
public class GatewayAndDeviceStatusVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("网关集合")
    private List<GlobalStatusVO> gateWayList;
    @ApiModelProperty("设备集合")
    private List<GlobalStatusVO> deviceList;
}
