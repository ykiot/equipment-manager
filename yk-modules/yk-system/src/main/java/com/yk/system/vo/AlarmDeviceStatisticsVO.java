package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 报警-根据设备按天统计统计VO
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AlarmDeviceStatisticsVO", description = "根据设备按天统计统计VO")
public class AlarmDeviceStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "日期")
    private String time;
    @ApiModelProperty(value = "恢复次数")
    private Integer recoverCount;
    @ApiModelProperty(value = "报警次数")
    private Integer alarmCount;
}