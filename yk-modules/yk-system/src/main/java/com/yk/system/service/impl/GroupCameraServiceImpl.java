package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.GroupCamera;
import com.yk.system.mapper.GroupCameraMapper;
import com.yk.system.service.GroupCameraService;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Service
public class GroupCameraServiceImpl extends ServiceImpl<GroupCameraMapper, GroupCamera> implements GroupCameraService {

    @Override
    public int updateBatch(List<GroupCamera> list) {
        return baseMapper.updateBatch(list);
    }
    @Override
    public int batchInsert(List<GroupCamera> list) {
        return baseMapper.batchInsert(list);
    }
}
