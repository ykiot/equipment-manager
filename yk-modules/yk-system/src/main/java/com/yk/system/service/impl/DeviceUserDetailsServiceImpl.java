package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.DeviceUserDetails;
import com.yk.system.mapper.DeviceUserDetailsMapper;
import com.yk.system.service.DeviceUserDetailsService;
import org.springframework.stereotype.Service;

/**
 * 用户和角色关联表 服务类
 * @author lmx
 * @since 2024-06-18
 */
@Service
public class DeviceUserDetailsServiceImpl extends ServiceImpl<DeviceUserDetailsMapper, DeviceUserDetails>
        implements DeviceUserDetailsService {

}