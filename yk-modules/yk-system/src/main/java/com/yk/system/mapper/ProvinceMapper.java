package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Province;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:05
 */
@Mapper
public interface ProvinceMapper extends BaseMapper<Province> {

    /**
     * 批量更新
     * @param list
     * @return
     */
    int updateBatch(List<Province> list);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Province> list);
}