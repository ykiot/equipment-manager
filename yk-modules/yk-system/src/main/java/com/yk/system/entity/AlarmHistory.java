package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_alarm_history
 * 报警历史表
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_alarm_history")
@ApiModel(value = "AlarmHistory", description = "报警历史表")
public class AlarmHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "报警记录id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "设备id")
    @TableField(value = "device_id")
    private Long deviceId;
    @ApiModelProperty(value = "变量id")
    @TableField(value = "variable_id")
    private Long variableId;
    @ApiModelProperty(value = "报警规则id")
    @TableField(value = "alarm_id")
    private Long alarmId;
    @ApiModelProperty(value = "报警值")
    @TableField(value = "alarm_value")
    private String alarmValue;
    @ApiModelProperty(value = "恢复值")
    @TableField(value = "recover_value")
    private String recoverValue;
    @ApiModelProperty(value = "报警时间")
    @TableField(value = "alarm_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date alarmAt;
    @ApiModelProperty(value = "恢复时间")
    @TableField(value = "recover_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date recoverAt;
    @ApiModelProperty(value = "是否恢复 false恢复")
    @TableField(value = "alarm_state")
    private Boolean alarmState;
    @ApiModelProperty(value = "是否消音 true消音")
    @TableField(value = "status")
    private Boolean status;
    @ApiModelProperty(value = "报警信息")
    @TableField(value = "message")
    private String message;

}