package com.yk.system.controller;

import com.yk.api.system.dto.AlarmUserDeviceDTO;
import com.yk.common.core.domain.Result;
import com.yk.common.core.utils.LoginHelper;
import com.yk.system.convert.AlarmUserDeviceConvert;
import com.yk.system.entity.AlarmUserDevice;
import com.yk.system.service.AlarmUserDeviceService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;


/**
 * 报警接收配置表
 *
 * @author lmx
 * @since 2024-02-27
 */
@Api(tags = "报警接收配置表")
@RestController
@RequiredArgsConstructor
@RequestMapping("/alarmUserDevice")
public class AlarmUserDeviceController {

    private final AlarmUserDeviceService alarmUserDeviceService;
    private final AlarmUserDeviceConvert alarmUserDeviceConvert;

    @GetMapping("/getById/{id}")
    @ApiOperation("报警接收配置表-查询单个")
    public Result<AlarmUserDevice> getById(@PathVariable(value = "id") Long id) {
        return Result.data(alarmUserDeviceService.getById(id));
    }

    @PostMapping("/save")
    @ApiOperation("报警接收配置表-新增")
    public Result<Boolean> save(@RequestBody @Validated AlarmUserDeviceDTO dto) {
        Long userId = LoginHelper.getLoginUserId();
        dto.setUserId(userId);
        return Result.data(alarmUserDeviceService.save(alarmUserDeviceConvert.dto2Entity(dto)));
    }

    @PostMapping("/update")
    @ApiOperation("报警接收配置表-修改")
    public Result<Long> updateById(@RequestBody @Validated AlarmUserDeviceDTO dto) {
        AlarmUserDevice alarmUserDevice = alarmUserDeviceConvert.dto2Entity(dto);
        if (Objects.nonNull(alarmUserDevice.getId())) {
            alarmUserDevice.setUserId(null);
            alarmUserDeviceService.updateById(alarmUserDevice);
        } else {
            Long userId = LoginHelper.getLoginUserId();
            alarmUserDevice.setUserId(userId);
            alarmUserDeviceService.save(alarmUserDevice);
        }
        return Result.data(alarmUserDevice.getId());
    }

    @GetMapping("/deleteById/{id}")
    @ApiOperation("报警接收配置表-删除")
    public Result<Boolean> deleteById(@PathVariable(value = "id") Long id) {
        return Result.data(alarmUserDeviceService.removeById(id));
    }
}