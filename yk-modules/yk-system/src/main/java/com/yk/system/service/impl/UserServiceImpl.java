package com.yk.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.api.system.dto.UserDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.common.core.enums.UserStatus;
import com.yk.common.core.exception.ServiceException;
import com.yk.system.entity.User;
import com.yk.system.entity.UserPost;
import com.yk.system.entity.UserRole;
import com.yk.system.mapper.UserMapper;
import com.yk.system.mapper.UserPostMapper;
import com.yk.system.mapper.UserRoleMapper;
import com.yk.system.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    private final UserRoleMapper sysUserRoleMapper;
    private final UserPostMapper sysUserPostMapper;


    @Override
    public int updateBatch(List<User> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<User> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public boolean checkUserNameUnique(User user) {
        LambdaQueryWrapper<User> lamdba = new LambdaQueryWrapper<>();
        lamdba.eq(User::getUserName, user.getUserName());
        lamdba.ne(ObjectUtil.isNotNull(user.getId()), User::getId, user.getId());
        return !baseMapper.exists(lamdba);
    }

    @Override
    public boolean checkPhoneUnique(User user) {
        if (Objects.isNull(user) || Objects.isNull(user.getPhone())){
            throw new ServiceException("手机号码为空");
        }
        LambdaQueryWrapper<User> lambda = new LambdaQueryWrapper<>();
        lambda.eq(User::getPhone, user.getPhone());
        lambda.ne(ObjectUtil.isNotNull(user.getId()), User::getId, user.getId());
        return !baseMapper.exists(lambda);
    }

    @Override
    public User checkPhoneByUser(String phone) {
        LambdaQueryWrapper<User> lambda = new LambdaQueryWrapper<>();
        lambda.select(User::getPhone, User::getStatus, User::getId, User::getPassword);
        lambda.eq(User::getPhone, phone);
        User user = baseMapper.selectOne(lambda);
        if (ObjectUtil.isNull(user)) {
            throw new ServiceException("用户不存在");
        }
        if (UserStatus.DISABLE.getCode().equals(user.getStatus())) {
            throw new ServiceException("用户已停用");
        }
        checkUserAllowed(user);
        return user;
    }

    @Override
    public void checkUserAllowed(User user) {
        if (ObjectUtil.isNotNull(user.getId()) && user.isAdmin()) {
            throw new ServiceException("不允许操作超级管理员用户");
        }
    }

    @Override
    public int deleteUserByIds(List<Long> ids) {
        for (Long userId : ids) {
            checkUserAllowed(new User(userId));
        }
        // 删除用户与角色关联
        sysUserRoleMapper.delete(new LambdaQueryWrapper<UserRole>().in(UserRole::getUserId, ids));
        // 删除用户与岗位表
        sysUserPostMapper.delete(new LambdaQueryWrapper<UserPost>().in(UserPost::getUserId, ids));
        return baseMapper.deleteBatchIds(ids);
    }

    @Override
    public User selectUserByUserName(String userName) {
        return baseMapper.selectUserByUserName(userName);
    }

    @Override
    public User selectUserByPhone(String phone) {
        return baseMapper.selectUserByPhone(phone);
    }

    @Override
    public IPage<UserDTO> queryPage(BasePageQuery<UserDTO> pageParam) {
        return baseMapper.queryPage(new Page<>(pageParam.getPageNum(), pageParam.getPageSize()), pageParam.getParam());
    }

}
