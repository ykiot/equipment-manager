package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.OperLog;
import com.yk.system.mapper.OperLogMapper;
import com.yk.system.service.OperLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:52
 */
@Service
public class OperLogServiceImpl extends ServiceImpl<OperLogMapper, OperLog> implements OperLogService {

    @Override
    public int updateBatch(List<OperLog> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<OperLog> list) {
        return baseMapper.batchInsert(list);
    }
}
