package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.UserPost;
import com.yk.system.mapper.UserPostMapper;
import com.yk.system.service.UserPostService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
public class UserPostServiceImpl extends ServiceImpl<UserPostMapper, UserPost> implements UserPostService {

    @Override
    public int updateBatch(List<UserPost> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<UserPost> list) {
        return baseMapper.batchInsert(list);
    }
}
