package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.RoleDept;
import com.yk.system.mapper.RoleDeptMapper;
import com.yk.system.service.RoleDeptService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
public class RoleDeptServiceImpl extends ServiceImpl<RoleDeptMapper, RoleDept> implements RoleDeptService {

    @Override
    public int updateBatch(List<RoleDept> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<RoleDept> list) {
        return baseMapper.batchInsert(list);
    }
}
