package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.MessageDTO;
import com.yk.system.entity.Message;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 消息 Mapper 接口
 *
 * @author lmx
 * @since 2023-12-16
 */
@Mapper
public interface MessageMapper extends BaseMapper<Message> {

    /**
     * 批量更新
     *
     * @param list 变量列表
     * @return
     */
    int updateBatch(List<Message> list);

    /**
     * 批量插入
     *
     * @param list 变量列表
     * @return
     */
    int batchInsert(@Param("list") List<Message> list);

    /**
     * 小程序分页查询消息-特殊分页
     *
     * @param param
     * @return
     */
    List<Message> pageByTime(MessageDTO param);

    /**
     * 小程序分页查询消息-总数
     *
     * @param param
     * @return
     */
    Integer countPageByTime(MessageDTO param);
}