package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_message
 * 消息
 *
 * @author lmx
 * @since 2023-12-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_message")
@ApiModel(value = "Message", description = "消息")
public class Message implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消息id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "用户id")
    @TableField(value = "user_id")
    private Long userId;
    @ApiModelProperty(value = "消息标题")
    @TableField(value = "title")
    private String title;
    @ApiModelProperty(value = "消息内容")
    @TableField(value = "content")
    private String content;
    @ApiModelProperty(value = "消息类型 报警类型 0-系统消息 1-共享消息")
    @TableField(value = "type")
    private String type;
    @ApiModelProperty(value = "共享人")
    @TableField(value = "share_id")
    private Long shareId;
    @ApiModelProperty(value = "被共享人")
    @TableField(value = "shared_id")
    private Long sharedId;
    @ApiModelProperty(value = "是否已读 ture-已读")
    @TableField(value = "status")
    private Boolean status;
    @ApiModelProperty(value = "是否接收 true-接收")
    @TableField(value = "receive_status")
    private Boolean receiveStatus;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "被共享人")
    @TableField(value = "device_role_id")
    private Long deviceRoleId;
    @ApiModelProperty(value = "目标类型 0-系统 1-设备 2-组态")
    @TableField(value = "target_type")
    private String targetType;
    @ApiModelProperty(value = "目标Id")
    @TableField(value = "target_id")
    private Long targetId;
}