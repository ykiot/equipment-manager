package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Variable;
import com.yk.system.mapper.VariableMapper;
import com.yk.system.service.VariableService;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Service
public class VariableServiceImpl extends ServiceImpl<VariableMapper, Variable> implements VariableService {

    @Override
    public int updateBatch(List<Variable> list) throws Exception{
        return baseMapper.updateBatch(list);
    }
    @Override
    public List<Variable> queryDtoByTemplateId(Long templateId) {
        return baseMapper.queryDtoByTemplateId(templateId);
    }
}
