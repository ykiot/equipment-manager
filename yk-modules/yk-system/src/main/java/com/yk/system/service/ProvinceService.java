package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Province;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:33
 */
public interface ProvinceService extends IService<Province>{


    int updateBatch(List<Province> list);

    int batchInsert(List<Province> list);
}
