package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:33
 */
@Mapper
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Role> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Role> list);

    /**
     * 获取用户信息、权限
     *
     * @param username 用户名
     * @return User
     */
    UserDTO getUserInfo(@Param("username") String username);
}