
package com.yk.system.convert;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.yk.api.system.dto.VariableDTO;
import com.yk.api.system.dto.VariableStatusDTO;
import com.yk.system.entity.Variable;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * 变量 对象转换
 * @author lmx
 * @since 2023-10-24
 */
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface VariableConvert {

    /**
     * dto to entity
     *
     * @param dto dto
     * @return entity
     */
    Variable dto2Entity(VariableDTO dto);

    /**
     * entity to dto
     *
     * @param entity entity
     * @return dto
     */
    VariableDTO entity2Dto(Variable entity);

    /**
     * update entity
     *
     * @param dto    dto
     * @param entity entity
     */
    void updateEntity(VariableDTO dto, @MappingTarget Variable entity);

    /**
     * str to list
     *
     * @param src src
     * @return list
     */
    default List<String> str2List(String src) {
        if (StrUtil.isEmpty(src)) {
            return CollUtil.newArrayList();
        }
        String[] split = src.split(",");
        List<String> result = Arrays.asList(split);
        return result;
    }

    /**
     * list to str
     *
     * @param src src
     * @return str
     */
    default String list2Str(List<String> src) {
        if (CollUtil.isEmpty(src)) {
            return null;
        }
        return String.join(",", src);
    }

    /**
     * json to str
     *
     * @param src src
     * @return str
     */
    default String json2Str(JSONObject src) {
        if (Objects.isNull(src)) {
            return null;
        }
        return src.toString();
    }

    /**
     * str to json
     *
     * @param src src
     * @return str
     */
    default JSONObject str2Json(String src) {
        if (StrUtil.isEmpty(src)) {
            return null;
        }
        return new JSONObject(src);
    }

    /**
     * str to VariableStatusDTO
     *
     * @param src 参数
     * @return 选项
     */
    default List<VariableStatusDTO> va2List(String src) {
        if(StrUtil.isBlank(src)){
            return null;
        }
        return JSONUtil.toList(src, VariableStatusDTO.class);
    }

    /**
     * VariableStatusDTO to str
     *
     * @param src 参数
     * @return 选项
     */
    default String list2Va(List<VariableStatusDTO> src) {
        if(CollUtil.isEmpty(src)){
            return null;
        }
        return JSONUtil.toJsonStr(src);
    }
}
