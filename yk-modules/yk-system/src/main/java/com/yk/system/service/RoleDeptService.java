package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.RoleDept;

import java.util.List;


/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface RoleDeptService extends IService<RoleDept> {


    int updateBatch(List<RoleDept> list);

    int batchInsert(List<RoleDept> list);

}
