package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 首页统计DTO
 *
 * @author lmx
 * @date 2023/11/8 16:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "IndexCountDTO", description = "首页统计DTO")
public class IndexCountVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("网关数量")
    private Long gatewayCount;
    @ApiModelProperty("设备数量")
    private Long deviceCount;
    @ApiModelProperty("摄像头数量")
    private Long videoCount;
    @ApiModelProperty("组态数量")
    private Long configurationCount;
    @ApiModelProperty("设备在线率")
    private String onlineCount;
    @ApiModelProperty("设备报警数")
    private Long alarmCount;
    @ApiModelProperty("存储型变量")
    private Long variableCount;
    @ApiModelProperty("设备模型")
    private Long templateCount;
}
