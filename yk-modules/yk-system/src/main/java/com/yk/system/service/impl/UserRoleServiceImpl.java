package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.UserRole;
import com.yk.system.mapper.UserRoleMapper;
import com.yk.system.service.UserRoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Override
    public int updateBatch(List<UserRole> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<UserRole> list) {
        return baseMapper.batchInsert(list);
    }
}
