package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 场景-设备关联
 *
 * @author lmx
 * @date 2023/11/24 17:22
 */
@ApiModel(description = "场景-设备关联")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_group_device")
public class GroupDevice implements Serializable {
    /**
     * 场景id
     */
    @TableField(value = "group_id")
    @ApiModelProperty(value = "场景id")
    private Long groupId;

    /**
     * 场景名称
     */
    @TableField(value = "device_id")
    @ApiModelProperty(value = "场景名称")
    private Long deviceId;

    /**
     * 创建用户
     */
    @TableField(value = "created_by")
    @ApiModelProperty(value = "创建用户")
    private Long createdBy;

    /**
     * 排序
     */
    @TableField(value = "sort")
    @ApiModelProperty(value = "排序")
    private Integer sort;

    private static final long serialVersionUID = 1L;
}