package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Menu;
import com.yk.system.mapper.MenuMapper;
import com.yk.system.service.MenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:52
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements MenuService {

    @Override
    public int updateBatch(List<Menu> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Menu> list) {
        return baseMapper.batchInsert(list);
    }
}
