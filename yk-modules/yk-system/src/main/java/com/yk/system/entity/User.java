package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yk.common.core.constant.UserConstants;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户信息表
 * @author lmx
 * @date 2023/10/24 9:36
 */
@ApiModel(description="用户信息表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_user")
public class User implements Serializable {
    /**
     * 用户ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="用户ID")
    private Long id;

    /**
     * 部门ID
     */
    @TableField(value = "dept_id")
    @ApiModelProperty(value="部门ID")
    private Long deptId;

    /**
     * 用户账号
     */
    @TableField(value = "user_name")
    @ApiModelProperty(value="用户账号")
    private String userName;

    /**
     * 用户昵称
     */
    @TableField(value = "nick_name")
    @ApiModelProperty(value="用户昵称")
    private String nickName;

    /**
     * 手机号码
     */
    @TableField(value = "phone")
    @ApiModelProperty(value="手机号码")
    private String phone;

    /**
     * 用户性别（0男 1女 2未知）
     */
    @TableField(value = "sex")
    @ApiModelProperty(value="用户性别（0男 1女 2未知）")
    private String sex;

    /**
     * 头像地址
     */
    @TableField(value = "avatar")
    @ApiModelProperty(value="头像地址")
    private String avatar;

    /**
     * 密码
     */
    @TableField(value = "`password`")
    @ApiModelProperty(value="密码")
    @JsonIgnore
    private String password;

    /**
     * 帐号状态（0正常 1停用）
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="帐号状态（0正常 1停用）")
    private String status;

    /**
     * 创建者
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建者")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间")
    private Date createdAt;

    /**
     * 更新者
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新者")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value="删除标志（0代表存在 2代表删除）")
    private String delFlag;


    @TableField(value = "corporation_name")
    @ApiModelProperty(value="公司名称")
    private String corporationName;

    @TableField(value = "region")
    @ApiModelProperty(value="区域")
    private String region;

    @TableField(value = "address")
    @ApiModelProperty(value="地址")
    private String address;

    @TableField(value = "profession")
    @ApiModelProperty(value="行业")
    private String profession;

    @TableField(value = "steer")
    @ApiModelProperty(value="0-未执行 2-已执行")
    private String steer;

    private static final long serialVersionUID = 1L;

    public User(Long userId) {
        this.id = userId;
    }

    /**
     * 是否管理员
     */
    public boolean isAdmin() {
        return UserConstants.ADMIN_ID.equals(this.id);
    }
}