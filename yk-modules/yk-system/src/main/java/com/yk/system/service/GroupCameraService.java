package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.GroupCamera;

import java.util.List;
    /**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface GroupCameraService extends IService<GroupCamera>{


    int updateBatch(List<GroupCamera> list);

    int batchInsert(List<GroupCamera> list);

}
