package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_user_wx
 * 用户-微信关联表
 *
 * @author lmx
 * @since 2023-11-29
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_user_wx")
@ApiModel(value = "UserWx", description = "用户-微信关联表")
public class UserWx implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "用户ID")
    @TableField(value = "user_id")
    private Long userId;
    @ApiModelProperty(value = "小程序-openid")
    @TableField(value = "x_open_id")
    private String xOpenId;
    @ApiModelProperty(value = "公众号-openid")
    @TableField(value = "g_open_id")
    private String gOpenId;
    @ApiModelProperty(value = "微信开放平台-id")
    @TableField(value = "union_id")
    private String unionId;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;

}