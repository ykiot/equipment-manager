package com.yk.system.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.ExcelHistory;
import com.yk.system.mapper.ExcelHistoryMapper;
import com.yk.system.service.ExcelHistoryService;
import org.springframework.stereotype.Service;

/**
 * 文件记录表 服务类
 *
 * @author lmx
 * @since 2023-11-20
 */
@Service
public class ExcelHistoryServiceImpl extends ServiceImpl<ExcelHistoryMapper, ExcelHistory> implements ExcelHistoryService {

    @Override
    public ExcelHistory saveExcelHistory(String name, String dataSource, String dataRadius, String startTime,
                                         String endTime, Long createdBy, String status) {
        ExcelHistory excelHistory = new ExcelHistory();
        excelHistory.setName(name);
        excelHistory.setDataSource(dataSource);
        excelHistory.setDataRadius(dataRadius);
        excelHistory.setStartTime(DateUtil.parseDateTime(startTime));
        excelHistory.setEndTime(DateUtil.parseDateTime(endTime));
        excelHistory.setCreatedBy(createdBy);
        excelHistory.setStatus(status);
        baseMapper.insert(excelHistory);
        return excelHistory;
    }
}