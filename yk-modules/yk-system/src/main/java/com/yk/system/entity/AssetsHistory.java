package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_assets_history
 * 资产历史记录
 *
 * @author lmx
 * @since 2024-01-04
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yk_assets_history")
@ApiModel(value = "AssetsHistory", description = "资产历史记录")
public class AssetsHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "归属客户")
    @TableField(value = "customer_id")
    private Long customerId;
    @ApiModelProperty(value = "网关id")
    @TableField(value = "gateway_id")
    private Long gatewayId;
    @ApiModelProperty(value = "网关编号")
    @TableField(value = "gateway_number")
    private String gatewayNumber;
    @ApiModelProperty(value = "网关主题")
    @TableField(value = "topic")
    private String topic;
    @ApiModelProperty(value = "网关密码")
    @TableField(value = "password")
    private String password;
    @ApiModelProperty(value = "售出时间")
    @TableField(value = "sale_time")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date saleTime;
    @ApiModelProperty(value = "备注")
    @TableField(value = "remark")
    private String remark;
    @ApiModelProperty(value = "旧创建时间")
    @TableField(value = "old_created_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date oldCreatedAt;
    @ApiModelProperty(value = "旧创建人")
    @TableField(value = "old_created_by")
    private Long oldCreatedBy;
    @ApiModelProperty(value = "旧资产id")
    @TableField(value = "old_assets_id")
    private Long oldAssetsId;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by")
    private Long createdBy;
}