package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.UserPost;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:40
 */
@Mapper
public interface UserPostMapper extends BaseMapper<UserPost> {
    int updateBatch(List<UserPost> list);

    int batchInsert(@Param("list") List<UserPost> list);
}