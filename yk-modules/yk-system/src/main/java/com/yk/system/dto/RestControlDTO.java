package com.yk.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 设备复位控制DTO
 *
 * @author lmx
 * @date 2023/12/8 13:34
 */
@Data
@ApiModel(value = "设备复位控制")
public class RestControlDTO {

    @ApiModelProperty(value = "设备ID")
    private String deviceId;
    @ApiModelProperty(value = "主题")
    private String topic;
    @ApiModelProperty(value = "变量地址")
    private String address;
    @ApiModelProperty(value = "控制参数")
    private String param;
}
