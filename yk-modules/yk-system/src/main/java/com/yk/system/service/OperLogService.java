package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.OperLog;

import java.util.List;
    /**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:52
 */
public interface OperLogService extends IService<OperLog>{


    int updateBatch(List<OperLog> list);

    int batchInsert(List<OperLog> list);

}
