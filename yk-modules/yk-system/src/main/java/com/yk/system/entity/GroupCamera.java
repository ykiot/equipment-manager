package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 场景-摄像头
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description="场景-摄像头")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_group_camera")
public class GroupCamera implements Serializable {
    /**
     * 主键id
     */
    @TableField(value = "group_id")
    @ApiModelProperty(value="主键id")
    private Long groupId;

    /**
     * 摄像头
     */
    @TableField(value = "camera_id")
    @ApiModelProperty(value="摄像头")
    private Long cameraId;

    private static final long serialVersionUID = 1L;
}