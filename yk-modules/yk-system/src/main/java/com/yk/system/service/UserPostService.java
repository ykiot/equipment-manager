package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.UserPost;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface UserPostService extends IService<UserPost> {


    int updateBatch(List<UserPost> list);

    int batchInsert(List<UserPost> list);

}
