package com.yk.system.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 5.0、api实体
 *
 * @author lmx
 * @date 2023/11/11 10:55
 */
@Data
@AllArgsConstructor
public class MqttApiMessageDTO {
    private String payload_encoding;
    private Integer qos;
    private Boolean retain;
    private String topic;
    private String payload;

    public MqttApiMessageDTO() {
        this.payload_encoding = "plain";
        this.qos = 0;
        this.retain = Boolean.FALSE;
    }
}
