package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.GroupConfiguration;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface GroupConfigurationMapper extends BaseMapper<GroupConfiguration> {

    /**
     * 批量插入
     *
     * @param list 数据集
     * @return 插入数量
     */
    int batchInsert(@Param("list") List<GroupConfiguration> list);

    /**
     * 根据场景ID查询数量
     *
     * @param groupId 场景id
     * @return 数量
     */
    Integer countByGroupId(@Param("groupId") Long groupId);

    /**
     * 根据场景ID查询
     *
     * @param groupId 场景id
     * @return 场景关联组态信息
     */
    GroupConfiguration selectByGroupId(@Param("groupId") Long groupId);

    /**
     * 根据组态ID查询
     *
     * @param configurationId 组态ID
     * @return 场景关联组态信息
     */
    GroupConfiguration selectByConfigurationId(@Param("configurationId") Long configurationId);
}