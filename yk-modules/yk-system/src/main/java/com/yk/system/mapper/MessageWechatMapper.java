package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.MessageWechat;
import org.apache.ibatis.annotations.Mapper;

/**
 * 消息 Mapper 接口
 * @author ${context.author}
 * @since 2024-04-15
 */
@Mapper
public interface MessageWechatMapper extends BaseMapper<MessageWechat>{

}