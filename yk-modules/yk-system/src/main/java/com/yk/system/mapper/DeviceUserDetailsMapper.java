package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.DeviceUserDetails;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户和角色关联表 Mapper 接口
 * @author lmx
 * @since 2024-06-18
 */
@Mapper
public interface DeviceUserDetailsMapper extends BaseMapper<DeviceUserDetails>{

}