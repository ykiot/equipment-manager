package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:36
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

    /**
     * 批量更新
     * @param list
     * @return
     */
    int updateBatch(List<User> list);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<User> list);

    /**
     * 根据用户名/或手机号查询
     * @param userName
     * @return
     */
    User selectUserByUserName(@Param("userName") String userName);

    /**
     * 根据手机号查询
     * @param phone
     * @return
     */
    User selectUserByPhone(@Param("phone") String phone);

    /**
     * 分页查询
     * @param objectPage
     * @param param
     * @return
     */
    IPage<UserDTO> queryPage(Page<Object> objectPage, @Param("query")UserDTO param);
}