package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 部门表
 * @author lmx
 * @date 2023/10/24 9:15
 */
@ApiModel(description="部门表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_dept")
public class Dept implements Serializable {
    /**
     * 部门id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="部门id")
    private Long id;

    /**
     * 父部门id
     */
    @TableField(value = "parent_id")
    @ApiModelProperty(value="父部门id")
    private Long parentId;

    /**
     * 部门名称
     */
    @TableField(value = "dept_name")
    @ApiModelProperty(value="部门名称")
    private String deptName;

    /**
     * 负责人
     */
    @TableField(value = "leader")
    @ApiModelProperty(value="负责人")
    private String leader;

    /**
     * 联系电话
     */
    @TableField(value = "phone")
    @ApiModelProperty(value="联系电话")
    private String phone;

    /**
     * 部门状态（0正常 1停用）
     */
    @TableField(value = "`status`")
    @ApiModelProperty(value="部门状态（0正常 1停用）")
    private String status;

    /**
     * 显示顺序
     */
    @TableField(value = "order_num")
    @ApiModelProperty(value="显示顺序")
    private Integer orderNum;

    /**
     * 创建者
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建者")
    private String createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间")
    private Date createdAt;

    /**
     * 更新者
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新者")
    private String updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value="删除标志（0代表存在 2代表删除）")
    private String delFlag;

    private static final long serialVersionUID = 1L;
}