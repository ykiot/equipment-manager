package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.Role;
import com.yk.system.mapper.RoleMapper;
import com.yk.system.service.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:54
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public int updateBatch(List<Role> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Role> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public UserDTO getUserInfo(String username) {
        return baseMapper.getUserInfo(username);
    }
}
