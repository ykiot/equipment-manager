package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * tb_device_role
 * 设备权限表
 *
 * @author lmx
 * @since 2023-11-08
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_device_role")
@ApiModel(value = "DeviceRole", description = "设备权限表")
public class DeviceRole implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备权限id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "设备管理员id")
    @TableField("admin_id")
    private Long adminId;
    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private Long userId;
    @ApiModelProperty(value = "设备id")
    @TableField("device_id")
    private Long deviceId;
    @ApiModelProperty(value = "权限类型 RoleTypeEnum")
    @TableField("role_type")
    private String roleType;
    @ApiModelProperty(value = "过期时间")
    @TableField("expired_time")
    private Date expiredTime;
    @ApiModelProperty(value = "共享状态（0-共享中、1-已失效）")
    @TableField("status")
    private String status;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.INSERT_UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除） 除了删除还有一层含义，分享设备时该值为2，接收设备后才为1")
    @TableField("del_flag")
    private String delFlag;


}