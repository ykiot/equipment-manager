package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.City;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16$ 17:30$
 */
public interface CityService extends IService<City>{


    int updateBatch(List<City> list);

    int batchInsert(List<City> list);

}
