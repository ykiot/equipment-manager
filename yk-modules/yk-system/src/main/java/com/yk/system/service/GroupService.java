package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.GroupDTO;
import com.yk.system.entity.Group;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface GroupService extends IService<Group> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Group> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(List<Group> list);

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       场景id
     * @param pageSize 条数
     * @return
     */
    Integer getPageNum(Long id, Long pageSize);

    /**
     * app查询列表
     *
     * @param dto
     * @return
     */
    List<GroupDTO> appList(GroupDTO dto);
}
