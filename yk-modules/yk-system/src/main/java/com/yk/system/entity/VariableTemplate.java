package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 变量模版表
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description="变量模版表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "vb_variable_template")
public class VariableTemplate implements Serializable {
    /**
     * 变量id
     */
    @TableField(value = "variable_id")
    @ApiModelProperty(value="变量id")
    private Long variableId;

    /**
     * 模版id
     */
    @TableField(value = "template_id")
    @ApiModelProperty(value="模版id")
    private Long templateId;

    private static final long serialVersionUID = 1L;
}