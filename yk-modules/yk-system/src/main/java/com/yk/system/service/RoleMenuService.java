package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.RoleMenu;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface RoleMenuService extends IService<RoleMenu>{


    int updateBatch(List<RoleMenu> list);

    int batchInsert(List<RoleMenu> list);

}
