package com.yk.system.service.impl;

import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.UserWx;
import com.yk.system.mapper.UserWxMapper;
import com.yk.system.service.UserWxService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 用户-微信关联表 服务类
 *
 * @author lmx
 * @since 2023-11-29
 */
@Service
public class UserWxServiceImpl extends ServiceImpl<UserWxMapper, UserWx> implements UserWxService {

    @Override
    public UserDTO selectUserByOpenId(String openId) {
        return baseMapper.selectUserByOpenId(openId);
    }
}