package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 组态表
 * @author lmx
 * @date 2023/10/24 9:44
 */
@ApiModel(description="组态表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_configuration")
public class Configuration implements Serializable {
    /**
     * 主组态d
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="主组态d")
    private Long id;

    /**
     * 组态名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="组态名称")
    private String name;

    /**
     * 组态属性值
     */
    @TableField(value = "`value`")
    @ApiModelProperty(value="组态属性值")
    private String value;

    /**
     * 创建人
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间")
    private Date createdAt;

    /**
     * 更新人
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value="删除标志（0代表存在 2代表删除）")
    private String delFlag;

    private static final long serialVersionUID = 1L;
}