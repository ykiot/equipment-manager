package com.yk.system.service;

import com.yk.system.entity.Notification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 系统通知 服务类
 *
 * @author lmx
 * @since 2024-01-05
 */
public interface NotificationService extends IService<Notification> {

    /**
     * 保存系统通知
     *
     * @param notification 参数
     */
    void saveNotification(Notification notification);

    /**
     * 更新系统通知
     *
     * @param notification 参数
     */
    void updateNotification(Notification notification);

    /**
     * 消息发送
     *
     * @param id 参数
     */
    void sendMessage(String id);
}