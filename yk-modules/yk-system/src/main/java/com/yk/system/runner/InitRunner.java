package com.yk.system.runner;

import com.yk.system.service.GatewayService;
import com.yk.system.service.TemplateService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 初始化 system 模块对应业务数据
 *
 * @author lmx
 * @date 2023/10/16 16:52
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class InitRunner {

    private final TemplateService templateService;
    private final GatewayService gatewayService;

    @PostConstruct
    public void init() throws Exception{
        // 初始化变量模板
        templateService.initTempRedis();
        // 初始化网关
        gatewayService.initGateway();
    }

}
