package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * yk_customer_info
 * 客户信息
 *
 * @author lmx
 * @since 2023-11-10
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("yk_customer_info")
@ApiModel(value = "CustomerInfo", description = "客户信息")
public class CustomerInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "公司名")
    @TableField("company_name")
    private String companyName;
    @ApiModelProperty(value = "联系人")
    @TableField("contact_person")
    private String contactPerson;
    @ApiModelProperty(value = "联系人手机号")
    @TableField("phone")
    private String phone;
    @ApiModelProperty(value = "省")
    @TableField("province")
    private String province;
    @ApiModelProperty(value = "市")
    @TableField("city")
    private String city;
    @ApiModelProperty(value = "详细地址")
    @TableField("address")
    private String address;
    @ApiModelProperty(value = "备注")
    @TableField("remarks")
    private String remarks;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "创建人")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "更新人")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField("del_flag")
    private String delFlag;


}