package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.GroupConfiguration;
import com.yk.system.mapper.GroupConfigurationMapper;
import com.yk.system.service.GroupConfigurationService;
import org.springframework.stereotype.Service;

import java.util.List;
/**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Service
public class GroupConfigurationServiceImpl extends ServiceImpl<GroupConfigurationMapper, GroupConfiguration> implements GroupConfigurationService {

    @Override
    public int batchInsert(List<GroupConfiguration> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public Integer countByGroupId(Long groupId) {
        return baseMapper.countByGroupId(groupId);
    }

    @Override
    public GroupConfiguration selectByGroupId(Long groupId) {
        return baseMapper.selectByGroupId(groupId);
    }

    @Override
    public GroupConfiguration selectByConfigurationId(Long configurationId) {
        return baseMapper.selectByConfigurationId(configurationId);
    }
}
