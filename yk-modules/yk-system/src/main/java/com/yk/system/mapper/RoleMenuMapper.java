package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.RoleMenu;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
@Mapper
public interface RoleMenuMapper extends BaseMapper<RoleMenu> {
    int updateBatch(List<RoleMenu> list);

    int batchInsert(@Param("list") List<RoleMenu> list);
}