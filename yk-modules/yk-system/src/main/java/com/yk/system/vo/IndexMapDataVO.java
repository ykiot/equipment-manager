package com.yk.system.vo;

import com.yk.api.system.dto.GatewayDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 首页地图数据VO
 *
 * @author lmx
 * @date 2023/11/8 16:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "IndexMapDataVO", description = "首页地图数据VO")
public class IndexMapDataVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("在线数量")
    private Integer onlineCount;
    @ApiModelProperty("离线数量")
    private Integer offlineCount;
    @ApiModelProperty("报警数量")
    private Integer alarmCount;
    @ApiModelProperty("网关列表")
    private List<GatewayDTO> gatewayList;
}
