package com.yk.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * 网关类型统计
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AssetsTypeStatisticsVO", description = "网关类型统计VO")
public class AssetsTypeStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "总数")
    private Integer count;
    @ApiModelProperty(value = "S702数量")
    private Integer s702;
    @ApiModelProperty(value = "M702数量")
    private Integer m702;
    @ApiModelProperty(value = "AM02数量")
    private Integer ab02;
    @ApiModelProperty(value = "VM04数量")
    private Integer vm04;
    @ApiModelProperty(value = "VM06数量")
    private Integer vm06;
    @ApiModelProperty(value = "其他数量")
    private Integer other;
    @ApiModelProperty(value = "查询时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date date;

    public AssetsTypeStatisticsVO(Integer count) {
        this.count = 0;
        this.s702 = 0;
        this.m702 = 0;
        this.ab02 = 0;
        this.vm04 = 0;
        this.vm06 = 0;
        this.other = 0;
    }
}