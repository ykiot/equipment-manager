package com.yk.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.DeviceRoleAlarmDTO;
import com.yk.api.system.dto.DeviceRoleDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.common.core.enums.DeviceControlsTypeEnum;
import com.yk.system.entity.DeviceRole;

import java.util.List;

/**
 * 设备权限表 服务类
 *
 * @author lmx
 * @since 2023-11-08
 */
public interface DeviceRoleService extends IService<DeviceRole> {

    /**
     * 设备创建时通过设备id初始化
     *
     * @param deviceId
     * @return
     */
    DeviceRole initDeviceRole(Long deviceId);

    /**
     * 设备创建时默认创建
     *
     * @param deviceRole
     */
    void save2Device(DeviceRole deviceRole);

    /**
     * 重新赋值部分字段
     *
     * @param deviceRoleDTO
     */
    void assignDeviceRole(DeviceRoleDTO deviceRoleDTO);

    /**
     * 验证设备权限
     *
     * @param typeEnum 设备共享权限类型
     * @param deviceId 设备ID
     */
    void checkDeviceRole(DeviceControlsTypeEnum typeEnum, Long deviceId);


    /**
     * 获取设备权限
     *
     * @param deviceId 设备ID
     * @return 设备权限
     */
    DeviceRole getDeviceRole(Long deviceId);

    /**
     * 小程序根据时间分页
     *
     * @param deviceId 设备ID
     * @return
     */
    List<DeviceRoleDTO> pageByTime(Long deviceId);

    /**
     * 特殊保存保存设备权限，计算过期时间并存入redis
     *
     * @param deviceRole 设备权限
     */
    void saveDeviceRole(DeviceRole deviceRole);

    /**
     * 将已删除的设备权限进行恢复，计算过期时间并存入redis
     *
     * @param deviceRole 设备权限
     */
    void updateDelDeviceRole(DeviceRole deviceRole);

    /**
     * 更新设备权限，计算过期时间并存入redis
     *
     * @param deviceRole 设备权限
     */
    void updateDeviceRole(DeviceRole deviceRole);

    /**
     * 更新设备分享状态
     *
     * @param id id
     */
    void update2Status(String id);

    /**
     * 根据id查询（不过滤删除）
     *
     * @param id
     * @return
     */
    DeviceRole getByIdIgnoreDel(Long id);

    /**
     * 验证设备是否被分享
     *
     * @param id      设备ID
     * @param message 消息提示
     * @param userId  接收人提示
     * @return
     */
    void checkShare(Long id, String message, Long userId);

    /**
     * 根据设备ID查询设备的管理人员的用户ID
     *
     * @param deviceId 设备ID
     * @return
     */
    List<Long> selectByDeviceIdAdmin(Long deviceId);

    /**
     * 分页查询-报警接收配置
     *
     * @param pageParam
     * @return
     */
    IPage<DeviceRoleAlarmDTO> page2Alarm(BasePageQuery<DeviceRoleAlarmDTO> pageParam);
}