package com.yk.system.service;

import cn.hutool.core.lang.tree.Tree;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Dept;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:34
 */
public interface DeptService extends IService<Dept>{

    /**
     * 批量更新
     * @param list
     * @return
     */
    int updateBatch(List<Dept> list);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int batchInsert(List<Dept> list);

    /**
     * 查询部门管理数据
     *
     * @param dept 部门信息
     * @return 部门信息集合
     */
    List<Dept> selectDeptList(Dept dept);

    /**
     * 校验部门是否有数据权限
     *
     * @param deptId 部门id
     */
    void checkDeptDataScope(Long deptId);

    /**
     * 查询部门树结构信息
     *
     * @param dept 部门信息
     * @return 部门树信息集合
     */
    List<Tree<Long>> selectDeptTreeList(Dept dept);

    /**
     * 构建前端所需要下拉树结构
     *
     * @param depts 部门列表
     * @return 下拉树结构列表
     */
    List<Tree<Long>> buildDeptTreeSelect(List<Dept> depts);
}
