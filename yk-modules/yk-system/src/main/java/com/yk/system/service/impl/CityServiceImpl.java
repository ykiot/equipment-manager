package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.City;
import com.yk.system.mapper.CityMapper;
import com.yk.system.service.CityService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16$ 17:30$
 */
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City> implements CityService {

    @Override
    public int updateBatch(List<City> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<City> list) {
        return baseMapper.batchInsert(list);
    }
}
