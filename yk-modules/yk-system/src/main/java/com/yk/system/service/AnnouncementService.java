package com.yk.system.service;

import com.yk.system.entity.Announcement;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 公告 服务类
 * @author lmx
 * @since 2024-01-04
 */
public interface AnnouncementService extends IService<Announcement>{

}