package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.VideoDTO;
import com.yk.system.entity.Video;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 摄像头 Mapper 接口
 *
 * @author lmx
 * @since 2023-11-07
 */
@Mapper
public interface VideoMapper extends BaseMapper<Video> {

    /**
     * 根据场景id查询
     *
     * @param groupId 场景ID
     * @return 摄像头集合
     */
    List<VideoDTO> listByGroupId(@Param("groupId") Long groupId);

    /**
     * 根据参数查询列表
     *
     * @param param
     * @return
     */
    List<VideoDTO> listByParam(@Param("query") VideoDTO param);
}