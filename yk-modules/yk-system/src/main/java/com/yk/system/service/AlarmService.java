package com.yk.system.service;

import com.yk.system.entity.Alarm;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 报警规则表 服务类
 *
 * @author lmx
 * @since 2023-11-21
 */
public interface AlarmService extends IService<Alarm> {

    /**
     * 根据ID查询（不过滤被删除的数据）
     */
    Alarm selectById(Long id);

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       场景id
     * @param pageSize 条数
     * @return
     */
    Integer getPageNum(Long id, Long pageSize);

    /**
     * 根据播报类型筛选
     *
     * @param informType 推送类型 0-公众号 1-短信 2-播报
     * @return
     */
    List<Alarm> getByInformType(String informType);
}