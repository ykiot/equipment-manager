package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 资产统计
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AssetsStatisticsVO", description = "资产统计VO")
public class AssetsStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "资产总数")
    private Long count;
    @ApiModelProperty(value = "出售数量")
    private Integer sellCount;
    @ApiModelProperty(value = "加入平台数量")
    private Integer joinCount;
    @ApiModelProperty(value = "回收数量")
    private Integer reclaimCount;
    @ApiModelProperty(value = "库存数量")
    private Integer inventoryCount;

    @ApiModelProperty(value = "S702数量")
    private Long s702;
    @ApiModelProperty(value = "S702出售数量")
    private Long s702SellCount;
    @ApiModelProperty(value = "S702加入平台数量")
    private Long s702JoinCount;
    @ApiModelProperty(value = "S702回收数量")
    private Long s702ReclaimCount;
    @ApiModelProperty(value = "S702库存数量")
    private Long s702InventoryCount;

    @ApiModelProperty(value = "M702数量")
    private Long m702;
    @ApiModelProperty(value = "M702出售数量")
    private Long m702SellCount;
    @ApiModelProperty(value = "M702加入平台数量")
    private Long m702JoinCount;
    @ApiModelProperty(value = "M702回收数量")
    private Long m702ReclaimCount;
    @ApiModelProperty(value = "M702库存数量")
    private Long m702InventoryCount;

    @ApiModelProperty(value = "AM02数量")
    private Long ab02;
    @ApiModelProperty(value = "AM02出售数量")
    private Long ab02SellCount;
    @ApiModelProperty(value = "AM02加入平台数量")
    private Long ab02JoinCount;
    @ApiModelProperty(value = "AM02回收数量")
    private Long ab02ReclaimCount;
    @ApiModelProperty(value = "AM02库存数量")
    private Long ab02InventoryCount;

    @ApiModelProperty(value = "VM04数量")
    private Long vm04;
    @ApiModelProperty(value = "VM04出售数量")
    private Long vm04SellCount;
    @ApiModelProperty(value = "VM04加入平台数量")
    private Long vm04JoinCount;
    @ApiModelProperty(value = "VM04回收数量")
    private Long vm04ReclaimCount;
    @ApiModelProperty(value = "VM04库存数量")
    private Long vm04InventoryCount;

    @ApiModelProperty(value = "VM06数量")
    private Long vm06;
    @ApiModelProperty(value = "VM06出售数量")
    private Long vm06SellCount;
    @ApiModelProperty(value = "VM06加入平台数量")
    private Long vm06JoinCount;
    @ApiModelProperty(value = "VM06回收数量")
    private Long vm06ReclaimCount;
    @ApiModelProperty(value = "VM06库存数量")
    private Long vm06InventoryCount;

    @ApiModelProperty(value = "其他数量")
    private Long other;
    @ApiModelProperty(value = "其他出售数量")
    private Long otherSellCount;
    @ApiModelProperty(value = "其他加入平台数量")
    private Long otherJoinCount;
    @ApiModelProperty(value = "其他回收数量")
    private Long otherReclaimCount;
    @ApiModelProperty(value = "其他库存数量")
    private Long otherInventoryCount;

    public AssetsStatisticsVO(Long count) {
        this.count = 0L;
        this.sellCount = 0;
        this.joinCount = 0;
        this.reclaimCount = 0;
        this.inventoryCount = 0;
        this.s702 = 0L;
        this.s702SellCount = 0L;
        this.s702JoinCount = 0L;
        this.s702ReclaimCount = 0L;
        this.s702InventoryCount = 0L;
        this.m702 = 0L;
        this.m702SellCount = 0L;
        this.m702JoinCount = 0L;
        this.m702ReclaimCount = 0L;
        this.m702InventoryCount = 0L;
        this.ab02 = 0L;
        this.ab02SellCount = 0L;
        this.ab02JoinCount = 0L;
        this.ab02ReclaimCount = 0L;
        this.ab02InventoryCount = 0L;
        this.vm04 = 0L;
        this.vm04SellCount = 0L;
        this.vm04JoinCount = 0L;
        this.vm04ReclaimCount = 0L;
        this.vm04InventoryCount = 0L;
        this.vm06 = 0L;
        this.vm06SellCount = 0L;
        this.vm06JoinCount = 0L;
        this.vm06ReclaimCount = 0L;
        this.vm06InventoryCount = 0L;
        this.other = 0L;
        this.otherSellCount = 0L;
        this.otherJoinCount = 0L;
        this.otherReclaimCount = 0L;
        this.otherInventoryCount = 0L;
    }
}