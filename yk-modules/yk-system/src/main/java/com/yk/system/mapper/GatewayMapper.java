package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yk.api.system.dto.GatewayDTO;
import com.yk.system.entity.Gateway;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface GatewayMapper extends BaseMapper<Gateway> {
    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Gateway> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Gateway> list);

    /**
     * 分页查询
     *
     * @param objectPage
     * @param param
     * @return
     */
    IPage<Gateway> queryPage(IPage<Gateway> objectPage, @Param("query") GatewayDTO param);

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       网关ID
     * @param pageSize 条数
     * @param userId   用户ID
     * @return
     */
    String getPageNum(@Param("id") Long id, @Param("pageSize") Long pageSize, @Param("userId") Long userId);
}