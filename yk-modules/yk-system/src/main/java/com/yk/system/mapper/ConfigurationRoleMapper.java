package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.api.system.dto.ConfigurationRoleDTO;
import com.yk.system.entity.ConfigurationRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组态权限表 Mapper 接口
 *
 * @author lmx
 * @since 2024-04-24
 */
@Mapper
public interface ConfigurationRoleMapper extends BaseMapper<ConfigurationRole> {

    /**
     * 根据id查询（不过滤删除）
     *
     * @param id 主键id
     * @return
     */
    ConfigurationRole getByIdIgnoreDel(@Param("id") Long id);

    /**
     * 根据id更新已删除
     *
     * @param configurationRole 参数
     */
    void updateById2Del(ConfigurationRole configurationRole);

    /**
     * 查询当前用户可查看的组组态
     * @param userId 用户id
     * @return
     */
    List<ConfigurationRoleDTO> selectRole2Device(@Param("userId") Long userId);
}