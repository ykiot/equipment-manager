package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.UserRole;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:53
 */
public interface UserRoleService extends IService<UserRole> {


    int updateBatch(List<UserRole> list);

    int batchInsert(List<UserRole> list);

}
