package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * tb_device_user_details
 * 用户设备详情信息
 *
 * @author lmx
 * @since 2024-06-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("tb_device_user_details")
@ApiModel(value = "DeviceUserDetails", description = "用户和角色关联表")
public class DeviceUserDetails implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    /**
     * 设备ID
     */
    @ApiModelProperty(value = "设备ID")
    @TableField(value = "device_id")
    private Long deviceId;
    /**
     * 用户ID
     */
    @ApiModelProperty(value = "用户ID")
    @TableField(value = "user_id")
    private Long userId;
    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    @TableField(value = "remarks")
    private String remarks;
    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人")

    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdAt;
    /**
     * 更新人
     */
    @ApiModelProperty(value = "更新人")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date updatedAt;


}