package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 全局在线离线状态VO
 *
 * @author lmx
 * @date 2023/11/8 16:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GlobalStatusVO", description = "全局在线离线状态VO")
public class GlobalStatusVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("网关、设备id")
    private Long id;
    @ApiModelProperty("在线离线状态 false-离线")
    private Boolean status;
}
