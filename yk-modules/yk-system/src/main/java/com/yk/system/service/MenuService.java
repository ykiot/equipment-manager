package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.system.entity.Menu;

import java.util.List;

/**
 *
 * @author lmx
 * @description
 * @date 2023/10/16 17:52
 */
public interface MenuService extends IService<Menu>{


    int updateBatch(List<Menu> list);

    int batchInsert(List<Menu> list);

}
