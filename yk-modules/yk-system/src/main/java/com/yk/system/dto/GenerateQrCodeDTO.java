package com.yk.system.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 二维码生成
 * @author lmx
 * @date 2023/11/10 11:32
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "GenerateQrCodeDTO", description = "二维码生成")
public class GenerateQrCodeDTO {

    @ApiModelProperty(value = "网关编号")
    @NotBlank(message = "网关编号不能为空")
    private String number;
    @ApiModelProperty(value = "网关密码")
    @NotBlank(message = "网关密码不能为空")
    private String password;
}
