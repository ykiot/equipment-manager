package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * 设备表
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description="设备表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_device")
public class Device implements Serializable {
    /**
     * 设备id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="设备id")
    private Long id;

    /**
     * 网关id
     */
    @TableField(value = "gateway_id", updateStrategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value="网关id")
    private Long gatewayId;

    /**
     * 变量模版id
     */
    @TableField(value = "template_id")
    @ApiModelProperty(value="变量模版id")
    private Long templateId;

    /**
     * 设备名称
     */
    @TableField(value = "device_name")
    @ApiModelProperty(value="设备名称")
    private String deviceName;

    /**
     * 从机地址
     */
    @TableField(value = "slave_address")
    @ApiModelProperty(value="从机地址")
    private String slaveAddress;

    /**
     * 图片
     */
    @TableField(value = "url")
    @ApiModelProperty(value="图片")
    private String url;

    /**
     * 备注  换成一对多了，此字段不在使用
     */
    @TableField(value = "notes")
    @ApiModelProperty(value="备注")
    private String notes;

    /**
     * 状态
     */
    @TableField(value = "status")
    @ApiModelProperty(value="设备状态 0-离线 1-在线")
    private Boolean status;

    /**
     * 创建人
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间")
    private Date createdAt;

    /**
     * 更新
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value="删除标志（0代表存在 2代表删除）")
    private String delFlag;

    private static final long serialVersionUID = 1L;
}