package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 用户与岗位关联表
 *
 * @author lmx
 * @date 2023/10/24 9:40
 */
@ApiModel(description = "用户与岗位关联表")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "sys_user_post")
public class UserPost implements Serializable {
    /**
     * 用户ID
     */
    @TableField(value = "user_id")
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 岗位ID
     */
    @TableField(value = "post_id")
    @ApiModelProperty(value = "岗位ID")
    private Long postId;

    private static final long serialVersionUID = 1L;
}