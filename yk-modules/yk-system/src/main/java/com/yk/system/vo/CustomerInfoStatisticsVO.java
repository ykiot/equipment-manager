package com.yk.system.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * 客户统计
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "CustomerInfoStatisticsVO", description = "客户统计VO")
public class CustomerInfoStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @ApiModelProperty(value = "数量")
    private Date date;
    @ApiModelProperty(value = "客户分布数据集合")
    private List<CustomerInfoRdStatisticsVO> rdsData;
}