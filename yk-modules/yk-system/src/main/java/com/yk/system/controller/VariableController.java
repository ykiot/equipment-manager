package com.yk.system.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yk.api.system.dto.VariableDTO;
import com.yk.common.core.constant.NumberConstant;
import com.yk.common.core.domain.Result;
import com.yk.system.convert.VariableConvert;
import com.yk.system.entity.Device;
import com.yk.system.entity.Variable;
import com.yk.system.service.DeviceService;
import com.yk.system.service.VariableService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * 变量 yk-system
 *
 * @author lmx
 * @since 2023-11-29
 */
@Api(tags = "变量控制器")
@RestController
@RequestMapping("/variable")
@RequiredArgsConstructor
public class VariableController {

    private final DeviceService deviceService;
    private final VariableService variableService;
    private final VariableConvert variableConvert;

    @GetMapping("/list/{deviceId}")
    @ApiOperation("变量-根据设备id查询变量")
    public Result<List<VariableDTO>> list(@PathVariable Long deviceId) {
        Device device = deviceService.getById(deviceId);
        List<VariableDTO> variableDTOList = CollUtil.newArrayList();
        if (Objects.isNull(device) || Objects.isNull(device.getTemplateId())) {
            return Result.data(variableDTOList);
        }
        List<Variable> variables = variableService.queryDtoByTemplateId(device.getTemplateId());
        if (CollUtil.isNotEmpty(variables)) {
            variableDTOList = variables.stream().map(variableConvert::entity2Dto).collect(Collectors.toList());
        }
        return Result.data(variableDTOList);
    }

    @PostMapping("/listByDevice")
    @ApiOperation("变量-根据设备查询变量列表")
    public Result<List<VariableDTO>> listByDevice(@RequestBody VariableDTO dto) {
        Device device = deviceService.getById(dto.getDeviceId());
        List<VariableDTO> variableDTOList = CollUtil.newArrayList();
        if (Objects.isNull(device) || Objects.isNull(device.getTemplateId())) {
            return Result.data(variableDTOList);
        }
        List<Variable> variables = variableService.queryDtoByTemplateId(device.getTemplateId());
        if (CollUtil.isNotEmpty(variables)) {
            variableDTOList = variables.stream()
                    .filter(variable -> StrUtil.isEmpty(dto.getShow()) || variable.getShow().equals(dto.getShow()))
                    .filter(variable -> StrUtil.isEmpty(dto.getStorage()) || variable.getStorage().equals(dto.getStorage()))
                    .map(variableConvert::entity2Dto)
                    .collect(Collectors.toList());
        }
        return Result.data(variableDTOList);
    }

    @PostMapping("/listByTemplateId")
    @ApiOperation("变量-根据模板查询变量列表")
    public Result<List<VariableDTO>> listByTemplateId(@RequestBody VariableDTO dto) {
        Assert.notNull(dto.getTemplateId(), "模板id不能为空");
        List<Variable> variables = variableService.queryDtoByTemplateId(dto.getTemplateId());
        List<VariableDTO> variableDTOList = CollUtil.newArrayList();
        if (CollUtil.isNotEmpty(variables)) {
            variableDTOList = variables.stream()
                    .filter(variable -> StrUtil.isEmpty(dto.getShow()) || variable.getShow().equals(dto.getShow()))
                    .filter(variable -> StrUtil.isEmpty(dto.getStorage()) || variable.getStorage().equals(dto.getStorage()))
                    .map(variableConvert::entity2Dto)
                    .collect(Collectors.toList());
        }
        return Result.data(variableDTOList);
    }

    @PostMapping("/listByIds")
    @ApiOperation("根据变量id查询详情")
    public Result<List<VariableDTO>> listByIds(@RequestBody VariableDTO dto) {
        List<VariableDTO> variableDTOList = CollUtil.newArrayList();
        if (Objects.isNull(dto) || CollUtil.isEmpty(dto.getIds())) {
            return Result.data(variableDTOList);
        }
        LambdaQueryWrapper<Variable> lambda = new LambdaQueryWrapper<>();
        lambda.in(Variable::getId, dto.getIds());
        List<Variable> variables = variableService.list(lambda);
        if (CollUtil.isNotEmpty(variables)) {
            variableDTOList = variables.stream().map(variableConvert::entity2Dto).collect(Collectors.toList());
        }
        return Result.data(variableDTOList);
    }

    @GetMapping("/listByDeviceId")
    @ApiOperation("根据设备id查询存储的变量")
    public Result<List<VariableDTO>> listByDeviceId(@RequestParam Long deviceId) {
        List<VariableDTO> variableDTOList = CollUtil.newArrayList();
        Device device = deviceService.getById(deviceId);
        Assert.notNull(device, "设备不存在");
        Assert.notNull(device.getTemplateId(), "模板id不存在");
        List<Variable> variables = variableService.queryDtoByTemplateId(device.getTemplateId());
        if (CollUtil.isNotEmpty(variables)) {
            variableDTOList = variables.stream()
                    .filter(variable -> NumberConstant.ONE_STR.equals(variable.getStorage()))
                    .map(variableConvert::entity2Dto)
                    .collect(Collectors.toList());
        }
        return Result.data(variableDTOList);
    }
}