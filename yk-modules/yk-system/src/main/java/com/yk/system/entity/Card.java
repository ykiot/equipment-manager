package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_card
 * 流量卡信息表
 *
 * @author lmx
 * @since 2024-02-20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_card")
@ApiModel(value = "Card", description = "流量卡信息表")
public class Card implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户ID")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "物联网卡号")
    @TableField("cardno")
    private String cardno;
    @ApiModelProperty(value = "使用流量(-1表示未知，单位KB，时间当月)")
    @TableField("used")
    private String used;
    @ApiModelProperty(value = "	剩余流量(单位KB)")
    @TableField("surplus")
    private String surplus;
    @ApiModelProperty(value = "物联网卡状态")
    @TableField("state")
    private String state;
    @ApiModelProperty(value = "卡可使用量")
    @TableField("canused")
    private String canused;
    @ApiModelProperty(value = "物联网卡状态描述")
    @TableField("message")
    private String message;
    @ApiModelProperty(value = "报警值")
    @TableField("alarm")
    private String alarm;
    @ApiModelProperty(value = "卡有效期")
    @TableField("expired_at")
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date expiredAt;
    @ApiModelProperty(value = "创建者")
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    private Long createdBy;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createdAt;
    @ApiModelProperty(value = "更新者")
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    private Long updatedBy;
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date updatedAt;
    @ApiModelProperty(value = "删除标志（0代表存在 2代表删除）")
    @TableField("del_flag")
    private String delFlag;
    @ApiModelProperty(value = "运营商")
    @TableField("autoname")
    private String autoname;
    @ApiModelProperty(value = "0-管理端 1-客户端")
    @TableField("type")
    private String type;
    public Card(String cardno) {
        this.cardno = cardno;
    }
}