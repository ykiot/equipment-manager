package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.VariableTemplate;
import com.yk.system.mapper.VariableTemplateMapper;
import com.yk.system.service.VariableTemplateService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Service
public class VariableTemplateServiceImpl extends ServiceImpl<VariableTemplateMapper, VariableTemplate> implements VariableTemplateService {

    @Override
    public int updateBatch(List<VariableTemplate> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<VariableTemplate> list) throws Exception{
        return baseMapper.batchInsert(list);
    }
}
