package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 地区
 * @author lmx
 * @date 2023/10/24 8:56
 */
@ApiModel(description="地区")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ct_area")
public class Area implements Serializable {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="")
    private Long id;

    /**
     * 地区名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="地区名称")
    private String name;

    /**
     * 邮政编码
     */
    @TableField(value = "zip_code")
    @ApiModelProperty(value="邮政编码")
    private String zipCode;

    /**
     * 城市id
     */
    @TableField(value = "city_id")
    @ApiModelProperty(value="城市id")
    private Integer cityId;

    private static final long serialVersionUID = 1L;
}