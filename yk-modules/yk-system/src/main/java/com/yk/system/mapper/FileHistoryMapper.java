package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.FileHistory;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @author lmx
 * @date 2023/10/24 9:19
 */
@Mapper
public interface FileHistoryMapper extends BaseMapper<FileHistory> {

    /**
     * 根据对象ID查询文件历史
     * @param objectId 对象ID
     * @return FileHistory
     */
    FileHistory selectByObjectId(@Param("objectId") Long objectId);
}