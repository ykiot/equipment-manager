package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Province;
import com.yk.system.mapper.ProvinceMapper;
import com.yk.system.service.ProvinceService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16 17:33
 */
@Service
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper, Province> implements ProvinceService {

    @Override
    public int updateBatch(List<Province> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Province> list) {
        return baseMapper.batchInsert(list);
    }
}
