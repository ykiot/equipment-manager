package com.yk.system.service.impl;

import com.yk.system.entity.Announcement;
import com.yk.system.mapper.AnnouncementMapper;
import com.yk.system.service.AnnouncementService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * 公告 服务类
 * @author lmx
 * @since 2024-01-04
 */
@Service
public class AnnouncementServiceImpl extends ServiceImpl<AnnouncementMapper, Announcement> implements AnnouncementService {

}