package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.ConfigurationRoleDTO;
import com.yk.system.entity.ConfigurationRole;

import java.util.List;

/**
 * 组态权限表 服务类
 *
 * @author lmx
 * @since 2024-04-24
 */
public interface ConfigurationRoleService extends IService<ConfigurationRole> {

    /**
     * 验证组态是否被分享
     *
     * @param configurationId 组态ID
     * @param message         消息提示
     * @param userId          接收人提示
     * @return
     */
    void checkShare(Long configurationId, String message, Long userId);

    /**
     * 根据id查询（不过滤删除）
     *
     * @param id 主键id
     * @return
     */
    ConfigurationRole getByIdIgnoreDel(Long id);

    /**
     * 将已删除的组态权限进行恢复，计算过期时间并存入redis
     *
     * @param configurationRole 组态权限
     */
    void updateDelConfigurationRole(ConfigurationRole configurationRole);

    /**
     * 更新组态组态权限进行恢复，计算过期时间并存入redis
     *
     * @param configurationRole 组态权限
     */
    void updateConfigurationRole(ConfigurationRole configurationRole);

    /**
     * 获取组态权限
     *
     * @param configurationId 组态id
     * @return 组态权限
     */
    ConfigurationRole getConfigurationRole(Long configurationId);

    /**
     * 重新赋值部分字段
     *
     * @param configurationRoleDTO 参数
     */
    void assignConfigurationRole(ConfigurationRoleDTO configurationRoleDTO);

    /**
     * 更新组态分享状态
     *
     * @param id 组态id
     */
    void update2Status(String id);

    /**
     * 组态创建时通过组态id初始化
     *
     * @param configurationId 组态id
     * @return 组态实体
     */
    ConfigurationRole initConfigurationRole(long configurationId);

    /**
     * 特殊保存保存组态权限，计算过期时间并存入redis
     *
     * @param configurationRole 参数
     */
    void saveDeviceRole(ConfigurationRole configurationRole);

    /**
     * 查询当前用户可查看的组组态
     *
     * @return 组态列表
     */
    List<ConfigurationRoleDTO> selectRole2Device();
}