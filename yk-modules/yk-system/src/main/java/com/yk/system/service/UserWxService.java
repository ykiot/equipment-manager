package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.UserDTO;
import com.yk.system.entity.UserWx;

/**
 * 用户-微信关联表 服务类
 * @author lmx
 * @since 2023-11-29
 */
public interface UserWxService extends IService<UserWx>{

    /**
     * 根据openId查询用户
     * @param openId
     * @return
     */
    UserDTO selectUserByOpenId(String openId);
}