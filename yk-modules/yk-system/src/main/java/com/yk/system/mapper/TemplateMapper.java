package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.TemplateDTO;
import com.yk.system.entity.Template;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface TemplateMapper extends BaseMapper<Template> {
    int updateBatch(List<Template> list);

    int batchInsert(@Param("list") List<Template> list);

    /**
     * 分页查询
     */
    IPage<TemplateDTO> queryPage(Page<TemplateDTO> objectPage, @Param("query") TemplateDTO param);
}