package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Area;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 8:56
 */
@Mapper
public interface AreaMapper extends BaseMapper<Area> {

    /**
     * 批量更新
     * @param list
     * @return
     */
    int updateBatch(List<Area> list);

    /**
     * 批量插入
     * @param list
     * @return
     */
    int batchInsert(@Param("list") List<Area> list);
}