package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 省
 * @author lmx
 * @date 2023/10/24 9:05
 */
@ApiModel(description="省")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "ct_province")
public class Province implements Serializable {
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="")
    private Long id;

    /**
     * 省名
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="省名")
    private String name;

    private static final long serialVersionUID = 1L;
}