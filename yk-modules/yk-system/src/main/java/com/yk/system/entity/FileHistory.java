package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;


/**
 * sys_file_history
 * 文件记录表
 *
 * @author lmx
 * @since 2023-11-06
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("sys_file_history")
@ApiModel(value = "SysFileHistory", description = "文件记录表")
public class FileHistory implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "文件id")
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private Long id;
    @ApiModelProperty(value = "文件访问地址")
    @TableField("url")
    private String url;
    @ApiModelProperty(value = "文件大小，单位字节")
    @TableField("size")
    private Long size;
    @ApiModelProperty(value = "文件名称")
    @TableField("filename")
    private String filename;
    @ApiModelProperty(value = "原始文件名")
    @TableField("original_filename")
    private String originalFilename;
    @ApiModelProperty(value = "基础存储路径")
    @TableField("base_path")
    private String basePath;
    @ApiModelProperty(value = "存储路径")
    @TableField("path")
    private String path;
    @ApiModelProperty(value = "文件扩展名")
    @TableField("ext")
    private String ext;
    @ApiModelProperty(value = "MIME类型")
    @TableField("content_type")
    private String contentType;
    @ApiModelProperty(value = "存储平台")
    @TableField("platform")
    private String platform;
    @ApiModelProperty(value = "缩略图访问路径")
    @TableField("th_url")
    private String thUrl;
    @ApiModelProperty(value = "缩略图名称")
    @TableField("th_filename")
    private String thFilename;
    @ApiModelProperty(value = "缩略图大小，单位字节")
    @TableField("th_size")
    private Long thSize;
    @ApiModelProperty(value = "缩略图MIME类型")
    @TableField("th_content_type")
    private String thContentType;
    @ApiModelProperty(value = "文件所属对象id")
    @TableField("object_id")
    private String objectId;
    @ApiModelProperty(value = "文件所属对象类型，例如用户头像，评价图片")
    @TableField("object_type")
    private String objectType;
    @ApiModelProperty(value = "文件元数据")
    @TableField("metadata")
    private String metadata;
    @ApiModelProperty(value = "文件用户元数据")
    @TableField("user_metadata")
    private String userMetadata;
    @ApiModelProperty(value = "缩略图元数据")
    @TableField("th_metadata")
    private String thMetadata;
    @ApiModelProperty(value = "缩略图用户元数据")
    @TableField("th_user_metadata")
    private String thUserMetadata;
    @ApiModelProperty(value = "附加属性")
    @TableField("attr")
    private String attr;
    @ApiModelProperty(value = "文件ACL")
    @TableField("file_acl")
    private String fileAcl;
    @ApiModelProperty(value = "缩略图文件ACL")
    @TableField("th_file_acl")
    private String thFileAcl;
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time", fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
    private Date createTime;

}