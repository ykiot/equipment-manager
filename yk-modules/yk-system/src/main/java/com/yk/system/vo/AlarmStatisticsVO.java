package com.yk.system.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;


/**
 * 报警统计VO
 *
 * @author lmx
 * @since 2023-11-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "AlarmStatisticsDTO", description = "报警统计VO")
public class AlarmStatisticsVO implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "今日报警数")
    private Integer todayCount;
    @ApiModelProperty(value = "今日报警中")
    private Long todayAlarmCount;
    @ApiModelProperty(value = "今日已恢复")
    private Long todayRecoverCount;
    @ApiModelProperty(value = "本周报警数")
    private Integer weekCount;
}