package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.VariableTemplate;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Mapper
public interface VariableTemplateMapper extends BaseMapper<VariableTemplate> {
    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<VariableTemplate> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     * @throws Exception
     */
    int batchInsert(@Param("list") List<VariableTemplate> list) throws Exception;
}