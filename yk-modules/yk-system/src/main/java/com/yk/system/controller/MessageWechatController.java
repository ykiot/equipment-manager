package com.yk.system.controller;

import com.yk.api.system.dto.MessageWechatDTO;
import com.yk.common.core.domain.Result;
import com.yk.system.convert.MessageWechatConvert;
import com.yk.system.service.MessageWechatService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 消息
 *
 * @author lmx
 * @since 2024-04-15
 */
@Api(tags = "消息") 
@RestController
@RequestMapping("/messageWechat")
public class MessageWechatController{

    @Autowired
    private MessageWechatService messageWechatService;
    @Autowired
    private MessageWechatConvert messageWechatConvert;

    @PostMapping("/save")
    @ApiOperation("新增")
    public Result<Boolean> save(@RequestBody MessageWechatDTO dto){
        return Result.data(messageWechatService.save(messageWechatConvert.dto2Entity(dto)));
    }
}