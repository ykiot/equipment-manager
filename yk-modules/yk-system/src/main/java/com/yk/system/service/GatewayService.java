package com.yk.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.GatewayDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.system.entity.Gateway;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface GatewayService extends IService<Gateway> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Gateway> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(List<Gateway> list);

    /**
     * 分页查询
     *
     * @param pageParam
     * @return
     */
    IPage<Gateway> queryPage(BasePageQuery<GatewayDTO> pageParam);

    /**
     * 初始化网关到redis
     */
    void initGateway();

    /**
     * 根据id查询当前数据所在页码
     *
     * @param id       网关ID
     * @param pageSize 条数
     * @return 页码
     */
    Integer getPageNum(Long id, Long pageSize);

    /**
     * 网关资产转让
     *
     * @param id    网关id
     * @param phone 接收人手机号
     */
    void transfer(Long id, String phone) throws Exception;

    /**
     * 根据用户id获取网关信息
     * @param id
     * @return
     */
    List<Gateway> getByUserId(Long id);
}
