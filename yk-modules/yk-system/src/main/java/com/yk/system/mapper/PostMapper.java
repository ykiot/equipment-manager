package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.Post;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:32
 */
@Mapper
public interface PostMapper extends BaseMapper<Post> {
    int updateBatch(List<Post> list);

    int batchInsert(@Param("list") List<Post> list);
}