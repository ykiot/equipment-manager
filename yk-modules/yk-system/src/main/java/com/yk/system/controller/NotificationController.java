package com.yk.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yk.api.system.dto.NotificationDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.common.core.domain.PageResult;
import com.yk.common.core.domain.Result;
import com.yk.system.convert.NotificationConvert;
import com.yk.system.entity.Notification;
import com.yk.system.service.NotificationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 系统通知 前端控制器
 *
 * @author lmx
 * @since 2024-01-05
 */
@Api(tags = "系统通知")
@RestController
@RequestMapping("/notification")
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationService notificationService;
    private final NotificationConvert notificationConvert;

    @GetMapping("/getById/{id}")
    @ApiOperation("系统通知-查询单个")
    public Result<Notification> getById(@PathVariable(value = "id") Long id) {
        return Result.data(notificationService.getById(id));
    }

    @PostMapping("/save")
    @ApiOperation("系统通知-新增")
    public Result<Boolean> save(@RequestBody @Validated NotificationDTO dto) {
        notificationService.saveNotification(notificationConvert.dto2Entity(dto));
        return Result.ok();
    }

    @PostMapping("/update")
    @ApiOperation("系统通知-修改")
    public Result<Boolean> updateById(@RequestBody @Validated NotificationDTO dto) {
        notificationService.updateNotification(notificationConvert.dto2Entity(dto));
        return Result.ok();
    }

    @GetMapping("/deleteById/{id}")
    @ApiOperation("系统通知-删除")
    public Result<Boolean> deleteById(@PathVariable(value = "id") Long id) {
        return Result.data(notificationService.removeById(id));
    }

    @PostMapping("/list")
    @ApiOperation("系统通知-查询列表")
    public Result<List<Notification>> list(@RequestBody NotificationDTO param) {
        final LambdaQueryWrapper<Notification> lambda = new QueryWrapper<Notification>().lambda();
        this.buildCondition(lambda, param);
        return Result.data(notificationService.list(lambda));
    }

    @PostMapping("/page")
    @ApiOperation("系统通知-分页查询")
    public PageResult<Notification> page(@RequestBody BasePageQuery<NotificationDTO> pageParam) {
        final NotificationDTO param = pageParam.getParam();
        final LambdaQueryWrapper<Notification> lambda = new QueryWrapper<Notification>().lambda();
        this.buildCondition(lambda, param);
        final IPage<Notification> page = notificationService.page(new Page<>(pageParam.getPageNum(), pageParam.getPageSize()), lambda);
        return PageResult.success(page.getRecords(), page.getTotal());
    }

    /**
     * 构造查询条件
     */
    private void buildCondition(LambdaQueryWrapper<Notification> lambda, NotificationDTO param) {
        lambda.like(StrUtil.isNotEmpty(param.getTitle()), Notification::getTitle, param.getTitle());
        lambda.orderByDesc(Notification::getCreatedAt);
    }


}