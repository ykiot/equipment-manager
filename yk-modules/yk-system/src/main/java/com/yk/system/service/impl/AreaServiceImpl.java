package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.Area;
import com.yk.system.mapper.AreaMapper;
import com.yk.system.service.AreaService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author lmx
 * @description
 * @date 2023/10/16$ 17:22$
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area> implements AreaService {

    @Override
    public int updateBatch(List<Area> list) {
        return baseMapper.updateBatch(list);
    }

    @Override
    public int batchInsert(List<Area> list) {
        return baseMapper.batchInsert(list);
    }
}
