package com.yk.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.DeviceDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.system.dto.ControlDTO;
import com.yk.system.entity.Device;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface DeviceService extends IService<Device> {

    /**
     * 批量更新
     *
     * @param list 数据列表
     * @return 数量
     */
    int updateBatch(List<Device> list);

    /**
     * 批量添加
     *
     * @param list 数据列表
     * @return 数量
     */
    int batchInsert(List<Device> list);

    /**
     * 查询未关联场景的设备
     *
     * @return 数据列表
     */
    List<DeviceDTO> selectNoGroup2Device();

    /**
     * 查询未关联场景的设备
     *
     * @return 数据列表
     */
    List<DeviceDTO> selectNoGroup2DeviceByParam(DeviceDTO param);

    /**
     * 查询当前用户可查看的设备
     *
     * @return 数据列表
     */
    List<DeviceDTO> selectRole2Device();

    /**
     * 查询当前用户可查看的设备
     *
     * @param token token
     * @return 数据列表
     */
    List<DeviceDTO> selectRole2Token(String token);

    /**
     * 分页查询
     *
     * @param pageParam 参数
     * @return 数据列表
     */
    IPage<DeviceDTO> queryPage(BasePageQuery<DeviceDTO> pageParam);

    /**
     * 设备控制
     *
     * @param dto 控制参数
     * @return true 成功 false 失败
     */
    Boolean control(ControlDTO dto);

    /**
     * 添加设备
     *
     * @param device   设备信息
     * @param groupIds 分组IDs
     */
    void add(Device device, List<Long> groupIds);

    /**
     * 查询列表
     *
     * @param param 参数
     * @return 列表
     */
    List<DeviceDTO> selectDeviceList(DeviceDTO param);

    /**
     * 复位命令下发
     *
     * @param cacheId 缓存Id
     */
    void sendResetWriteEmq(String cacheId);
}
