package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.api.system.dto.GroupDTO;
import com.yk.common.core.utils.LoginHelper;
import com.yk.system.entity.Group;
import com.yk.system.mapper.GroupMapper;
import com.yk.system.service.GroupService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
/**
 *
 * @author lmx
 * @date 2023/10/24 9:45
 */
@Service
public class GroupServiceImpl extends ServiceImpl<GroupMapper, Group> implements GroupService {

    @Override
    public int updateBatch(List<Group> list) {
        return baseMapper.updateBatch(list);
    }
    @Override
    public int batchInsert(List<Group> list) {
        return baseMapper.batchInsert(list);
    }

    @Override
    public Integer getPageNum(Long id, Long pageSize) {
        String pageNum = baseMapper.getPageNum(id, pageSize, LoginHelper.getLoginUserId());
        BigDecimal number = new BigDecimal(pageNum);
        return number.intValue();
    }

    @Override
    public List<GroupDTO> appList(GroupDTO dto) {
        dto.setCreatedBy(LoginHelper.getLoginUserId());
        return baseMapper.appList(dto);
    }
}
