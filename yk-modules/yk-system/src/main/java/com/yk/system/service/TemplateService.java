package com.yk.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.dataGatherer.dto.SuperTableDTO;
import com.yk.api.system.dto.TemplateDTO;
import com.yk.common.core.domain.BasePageQuery;
import com.yk.system.entity.Template;
import com.yk.system.entity.Variable;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:45
 */
public interface TemplateService extends IService<Template> {

    /**
     * 批量更新
     *
     * @param list 模板集合
     * @return 更新结果
     */
    int updateBatch(List<Template> list);

    /**
     * 批量插入
     *
     * @param list 模板集合
     * @return 插入结果
     */
    int batchInsert(List<Template> list);

    /**
     * 添加
     *
     * @param dto 模板DTO
     */
    void add(TemplateDTO dto) throws Exception;

    /**
     * 编辑
     *
     * @param dto 模板DTO
     */
    void edit(TemplateDTO dto) throws Exception;

    /**
     * 分页查询
     *
     * @param pageParam 分页对象
     * @return 分页结果
     */
    IPage<TemplateDTO> queryPage(BasePageQuery<TemplateDTO> pageParam);

    /**
     * 初始化变量模版到redis
     */
    void initTempRedis() throws Exception;

    /**
     * 根据id查询
     *
     * @param id 模板id
     * @return
     */
    TemplateDTO view(Long id);

    /**
     * 移除缓存
     *
     * @param templateIds 模板id集合
     */
    void removeCache(List<Long> templateIds);

    /**
     * 构建超级表
     *
     * @param tempId 模板id
     * @param vbs    变量列表
     * @return 返回超级表对象
     */
    SuperTableDTO buildSuperTable(Long tempId, List<Variable> vbs);

    /**
     * 创建超级表并缓存
     *
     * @param templateId 模版id
     * @param vbs        变量列表
     * @throws Exception
     */
    void createSuperTableAndCache(Long templateId, List<Variable> vbs) throws Exception;

    /**
     * 更新模板在redis的数据
     *
     * @param dto 超级表实体
     */
    void updateRedis(SuperTableDTO dto) throws Exception;

}
