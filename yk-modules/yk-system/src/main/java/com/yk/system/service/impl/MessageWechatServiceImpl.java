package com.yk.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yk.system.entity.MessageWechat;
import com.yk.system.mapper.MessageWechatMapper;
import com.yk.system.service.MessageWechatService;
import org.springframework.stereotype.Service;

/**
 * 消息 服务类
 * @author ${context.author}
 * @since 2024-04-15
 */
@Service
public class MessageWechatServiceImpl extends ServiceImpl<MessageWechatMapper, MessageWechat> implements MessageWechatService {

}