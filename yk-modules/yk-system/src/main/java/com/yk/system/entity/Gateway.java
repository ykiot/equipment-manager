package com.yk.system.entity;

import com.alibaba.excel.annotation.ExcelIgnoreUnannotated;
import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@ApiModel(description="网关")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_gateway")
@ExcelIgnoreUnannotated
public class Gateway implements Serializable {
    /**
     * 主键id
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    @ApiModelProperty(value="主键id")
    private Long id;

    /**
     * 网关名称
     */
    @TableField(value = "`name`")
    @ApiModelProperty(value="网关名称")
    @ExcelProperty(value = "网关名称")
    private String name;

    /**
     * 网关编号
     */
    @TableField(value = "`number`")
    @ApiModelProperty(value="网关编号")
    @ExcelProperty(value = "网关编号")
    private String number;

    /**
     * 通讯密码
     */
    @TableField(value = "communication_password")
    @ApiModelProperty(value="通讯密码")
    private String communicationPassword;

    /**
     * 网关型号
     */
    @TableField(value = "model")
    @ApiModelProperty(value="网关型号")
    @ExcelProperty(value = "网关型号")
    private String model;

    /**
     * 物联网卡号
     */
    @TableField(value = "card_number")
    @ApiModelProperty(value="物联网卡号")
    @ExcelProperty(value = "物联网卡号")
    private String cardNumber;

    /**
     * 网关地址
     */
    @TableField(value = "address")
    @ApiModelProperty(value="网关地址")
    private String address;

    /**
     * 经纬度
     */
    @TableField(value = "latitude_longitude")
    @ApiModelProperty(value="经纬度")
    private String latitudeLongitude;

    /**
     * 图片地址
     */
    @TableField(value = "url")
    @ApiModelProperty(value="图片地址")
    private String url;

    /**
     * 网关状态
     */
    @TableField(value = "status")
    @ApiModelProperty(value="网关状态 1-在线 0-离线")
    private Boolean status;

    /**
     * 网关连接Emq-ClientId
     */
    @TableField(value = "client_id")
    @ApiModelProperty(value="网关连接Emq-ClientId")
    private String clientId;

    /**
     * 创建人
     */
    @TableField(value = "created_by", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建人")
    private Long createdBy;

    /**
     * 创建时间
     */
    @TableField(value = "created_at", fill = FieldFill.INSERT)
    @ApiModelProperty(value="创建时间")
    private Date createdAt;

    /**
     * 更新人
     */
    @TableField(value = "updated_by", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新人")
    private Long updatedBy;

    /**
     * 更新时间
     */
    @TableField(value = "updated_at", fill = FieldFill.UPDATE)
    @ApiModelProperty(value="更新时间")
    private Date updatedAt;

    /**
     * 删除标志（0代表存在 2代表删除）
     */
    @TableField(value = "del_flag")
    @ApiModelProperty(value="删除标志（0代表存在 2代表删除）")
    private String delFlag;

    /**
     * 网关主题
     */
    @TableField(value = "topic")
    @ApiModelProperty(value="网关主题")
    private String topic;
    private static final long serialVersionUID = 1L;
}