package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.CustomerInfo;
import com.yk.system.vo.CustomerInfoRdStatisticsVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * 客户信息 Mapper 接口
 *
 * @author lmx
 * @since 2023-11-10
 */
@Mapper
public interface CustomerInfoMapper extends BaseMapper<CustomerInfo> {

    /**
     * 客户地区分布统计
     * @return
     */
    List<CustomerInfoRdStatisticsVO> rdStatistics();
}