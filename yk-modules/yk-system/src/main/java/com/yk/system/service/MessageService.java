package com.yk.system.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.MessageDTO;
import com.yk.system.entity.Device;
import com.yk.system.entity.Message;

import java.util.List;

/**
 * 消息 服务类
 *
 * @author lmx
 * @since 2023-12-16
 */
public interface MessageService extends IService<Message> {

    /**
     * 批量更新
     *
     * @param list 变量列表
     * @return
     */
    int updateBatch(List<Message> list);

    /**
     * 批量插入
     *
     * @param list 变量列表
     * @return
     */
    int batchInsert(List<Message> list);

    /**
     * 系统通知消息
     *
     * @param title   消息标题
     * @param userId  用户ID
     * @param content 消息内容
     * @param type    消息类型 0-系统消息 1-共享消息
     */
    void systemMessage(String title, Long userId, String content, String type);

    /**
     * 设备共享人消息
     *
     * @param content       消息内容
     * @param shareId       共享人ID
     * @param sharedId      被共享人ID
     * @param deviceRoleId  设备权限ID
     * @param receiveStatus true-接收
     * @param targetId      目标id
     */
    void shareMessage(String content, Long shareId, Long sharedId, Long deviceRoleId, Boolean receiveStatus, Long targetId);

    /**
     * 组态共享人消息
     *
     * @param content       消息内容
     * @param shareId       共享人ID
     * @param sharedId      被共享人ID
     * @param deviceRoleId  设备权限ID
     * @param receiveStatus true-接收
     * @param targetId      目标id
     */
    void configurationShareMessage(String content, Long shareId, Long sharedId, Long deviceRoleId, Boolean receiveStatus, Long targetId);

    /**
     * 设备被共享人消息
     *
     * @param content       消息内容
     * @param shareId       共享人ID
     * @param sharedId      被共享人ID
     * @param deviceRoleId  设备权限ID
     * @param receiveStatus true-接收
     * @param targetId      目标id
     */
    void sharedMessage(String content, Long shareId, Long sharedId, Long deviceRoleId, Boolean receiveStatus, Long targetId);

    /**
     * 组态被共享人消息
     *
     * @param content       消息内容
     * @param shareId       共享人ID
     * @param sharedId      被共享人ID
     * @param deviceRoleId  设备权限ID
     * @param receiveStatus true-接收
     * @param targetId      目标id
     */
    void configurationSharedMessage(String content, Long shareId, Long sharedId, Long deviceRoleId, Boolean receiveStatus, Long targetId);

    /**
     * 小程序分页查询消息-特殊分页
     *
     * @param param 查询参数
     * @return 返回消息集合
     */
    List<Message> pageByTime(MessageDTO param);

    /**
     * 小程序分页查询消息-总数
     *
     * @param param 查询参数
     * @return 返回消息集合
     */
    Integer countPageByTime(MessageDTO param);

    /**
     * 更新已接收状态
     *
     * @param deviceRoleId 设备权限ID
     */
    void updateReceiveStatus(Long deviceRoleId);

    /**
     * 检查共享消息
     *
     * @param sharedId   被共享人
     * @param shareId    共享人
     * @param targetType 目标类型
     * @param targetId   目标id
     */
    void checkShare(Long sharedId, Long shareId, String targetType, Long targetId);

    /**
     * 批量分享检查共享消息
     *
     * @param device     设备
     * @param sharedId   被共享人
     * @param shareId    共享人
     * @param targetType 目标类型
     */
    void batchCheckShare(Device device, Long sharedId, Long shareId, String targetType);
}