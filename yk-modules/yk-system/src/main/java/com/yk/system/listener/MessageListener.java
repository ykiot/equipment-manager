package com.yk.system.listener;

import com.yk.api.system.dto.MessageDTO;
import com.yk.common.rabbitmq.constant.QueueConstants;
import com.yk.system.convert.MessageConvert;
import com.yk.system.entity.Message;
import com.yk.system.service.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.Objects;

/**
 * 消息入库监听
 *
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
@Component
@RequiredArgsConstructor
public class MessageListener {

    private final MessageService messageService;
    private final MessageConvert messageConvert;

    @RabbitListener(queues = QueueConstants.MESSAGE_SAVE)
    @RabbitHandler
    public void onMessage(MessageDTO dto) {
        if (Objects.isNull(dto)){
            return;
        }
        Message message = messageConvert.dto2Entity(dto);
        messageService.save(message);
    }
}