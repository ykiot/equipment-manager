package com.yk.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yk.system.entity.OperLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:30
 */
@Mapper
public interface OperLogMapper extends BaseMapper<OperLog> {
    int updateBatch(List<OperLog> list);

    int batchInsert(@Param("list") List<OperLog> list);
}