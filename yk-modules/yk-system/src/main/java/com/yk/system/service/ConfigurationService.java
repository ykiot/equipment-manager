package com.yk.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yk.api.system.dto.ConfigurationDTO;
import com.yk.system.entity.Configuration;

import java.util.List;

/**
 * @author lmx
 * @date 2023/10/24 9:44
 */
public interface ConfigurationService extends IService<Configuration> {

    /**
     * 批量更新
     *
     * @param list
     * @return
     */
    int updateBatch(List<Configuration> list);

    /**
     * 批量插入
     *
     * @param list
     * @return
     */
    int batchInsert(List<Configuration> list);

    /**
     * 根据场景Id获取组态列表
     *
     * @param groupId 场景Id
     * @param name    场景名称
     * @return
     */
    List<ConfigurationDTO> getConfigurationDTOS(Long groupId, String name);
}
