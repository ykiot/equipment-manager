package com.yk.system.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 场景-组态
 * @author lmx
 * @date 2023/10/24 9:45
 */
@ApiModel(description="场景-组态")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "tb_group_configuration")
public class GroupConfiguration implements Serializable {
    /**
     * 场景id
     */
    @TableField(value = "group_id")
    @ApiModelProperty(value="场景id")
    private Long groupId;

    /**
     * 组态id
     */
    @TableField(value = "configuration_id")
    @ApiModelProperty(value="组态id")
    private Long configurationId;

    private static final long serialVersionUID = 1L;
}