package com.yk.system;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;
import com.google.common.collect.Maps;
import com.yk.common.core.utils.HutoolExcelUtil;
import com.yk.system.entity.ExcelHistory;
import com.yk.system.service.ExcelHistoryService;
import lombok.extern.slf4j.Slf4j;
import org.dromara.x.file.storage.core.FileStorageService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author lmx
 * @date 2023/11/21 11:14
 */
@Slf4j
@SpringBootTest
class ExcelTest {

    @Autowired
    private FileStorageService fileStorageService;
    @Autowired
    private ExcelHistoryService excelHistoryService;
    @Value("${file.path}")
    private String path;

    @Test
    public void excelTest() throws IOException {

        List<Map<String, Object>> dynamicDataByDeviceId = new ArrayList<>();
        Map<String, Object> map = Maps.newHashMap();
        map.put("ts", "2021-05-26");
        map.put("ua", "230.11");
        map.put("ub", "230.11");
        map.put("uc", "230.11");
        map.put("device_id", "1");
        dynamicDataByDeviceId.add(map);

        Map<String, Object> map1 = Maps.newHashMap();
        map1.put("ts", "2021-05-27");
        map1.put("ua", "230.22");
        map1.put("ub", "230.22");
        map1.put("uc", "230.22");
        map1.put("device_id", "2");
        dynamicDataByDeviceId.add(map1);

        // 获取设备名称
        Map<String, String> deviceMap = Maps.newHashMap();
        deviceMap.put("1", "设备1");
        deviceMap.put("2", "设备2");
        deviceMap.put("3", "设备3");
        deviceMap.put("4", "设备4");

        // 获取变量名称
        Map<String, String> firstMap = Maps.newHashMap();
        firstMap.put("ua", "电流");
        firstMap.put("ub", "电压");
        firstMap.put("uc", "功率");

        // 前两行数据生成
        Set<String> set1 = deviceMap.keySet();
        Set<String> set2 = firstMap.keySet();

        List<List<String>> rows = CollUtil.newArrayList();
        List<String> row1 = CollUtil.newArrayList();
        row1.add("设备");
        List<String> row2 = CollUtil.newArrayList();
        row2.add("变量");

        // 格式转换 <2021-05-27：[ua：230.22, ub：230.22...]>
        Map<String, List<Map<String, Object>>> ts = dynamicDataByDeviceId.stream().collect(Collectors.groupingBy(it -> it.get("ts").toString()));
        Set<String> set = ts.keySet();

        for (String s : set1){
            for (String s2 : set2){
                row1.add(deviceMap.get(s));
                row2.add(firstMap.get(s2));
            }
        }
        rows.add(row2);

        // 时间
        for (String s3 : set){
            List<String> row = CollUtil.newArrayList();
            row.add(s3);
            List<Map<String, Object>> mapList = ts.get(s3);

            // 设备
            for (String s : set1){

                // 变量名称
                for (String s2 : set2){

                    Optional<Map<String, Object>> device_id = mapList.stream().filter(it -> it.get("device_id").toString().equals(s)).findFirst();
                    if (device_id.isPresent()){
                        Map<String, Object> stringObjectMap = device_id.get();
                        // 变量值
                        String s1 = stringObjectMap.get(s2).toString();
                        row.add(s1);
                    }else {
                        row.add("");
                    }
                }
            }
            rows.add(row);
        }

        ExcelWriter writer = ExcelUtil.getWriter(true);
        writer.writeHeadRow(row1);
        writer.write(rows);
        HutoolExcelUtil.setBaseStyle(writer);
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            writer.flush(out);
            writer.close();

            // Excel历史表入库
            String dataSource = "设备1、设备2、设备3、设备4";
            String dataRadius = "电流、电流、功率";
            ExcelHistory excelHistory = excelHistoryService.saveExcelHistory("11", dataSource, dataRadius,
                    "2023-11-20 12:23:00", "2023-11-21 12:23:00", null, "2");
            fileStorageService.of(new ByteArrayInputStream(out.toByteArray()), null, null, (long)out.size())
                    .setSaveFilename("text.xlsx")
                    .setObjectId(excelHistory.getId())
                    .setPath(DateUtil.today() + "/")
                    .upload();
        } catch (Exception e) {
            log.error("自定义报表失败", e.getMessage());
        }
    }

}
