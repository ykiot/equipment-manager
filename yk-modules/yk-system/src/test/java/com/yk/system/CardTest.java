package com.yk.system;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import cn.hutool.script.ScriptUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.test.context.SpringBootTest;

import javax.script.ScriptEngine;
import java.math.BigInteger;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author lmx
 * @date 2023/11/21 11:14
 */
@Slf4j
@SpringBootTest
class CardTest {
    private static final String API_KEY = "623530524e6cd40d9a7b202ba5a384f7";
    private static final String BASE_URL = "http://hywx.xjict.com:32040/api/v1/getChaxun";

    public static void main1(String[] args) {
        try {
            // 用户ID和卡号
            String userId = "141";
            String cardno = "89860496162181192419";
            // 时间戳
            String times = System.currentTimeMillis() + "";
            // 创建MD5签名
            String signData = "userId=" + userId + "&apikey=" + API_KEY + "&times=" + times;
            SecureUtil.md5();
            String sign = md5EncryptToUpperCase(signData);
            // 构造请求URL
            String urlStr = BASE_URL +
                    "?userId=" + URLEncoder.encode(userId, StandardCharsets.UTF_8.name()) +
                    "&cardno=" + URLEncoder.encode(cardno, StandardCharsets.UTF_8.name()) +
                    "&times=" + times +
                    "&sign=" + sign;
            String result = HttpUtil.get(urlStr);
            JSONObject jsonObject = JSONUtil.parseObj(result);
            System.out.println("s = " + jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // MD5加密方法
    public static String md5EncryptToUpperCase(String input) {
        try {
            // 获取MessageDigest实例（指定MD5算法）
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 将需要加密的字符串转换成字节数组
            byte[] messageBytes = input.getBytes(StandardCharsets.UTF_8);
            // 对字节数组进行MD5计算，得到原始的16字节哈希值
            byte[] hashBytes = md.digest(messageBytes);
            // 将16字节哈希值转换为32位大写的十六进制字符串
            BigInteger number = new BigInteger(1, hashBytes);
            StringBuilder hexString = new StringBuilder(number.toString(16));
            // 补齐至32位
            while (hexString.length() < 32) {
                hexString.insert(0, '0');
            }
            return hexString.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unable to get MD5 instance", e);
        }
    }

    public static void main(String[] args) {
        try {
            ScriptEngine engine = ScriptUtil.getJsEngine();
            // 定义一个变量并初始化
            // 将Java变量传递到JavaScript，确保使用正确的JSON格式
            engine.put("inputData", "{\"vals\":[{\"data\":{\"温度\":30,\"开关状态\":1,\"电机状态\":1}},{\"data\":{\"温度\":20,\"开关状态\":10,\"电机状态\":10}}]}");
            // 执行JavaScript代码并返回结果
            Object eval = ScriptUtil.eval("(function (inputData) {\n" +
                    "    // 解析输入的JSON数据\n" +
                    "    var data = JSON.parse(inputData);\n" +
                    "    \n" +
                    "    // 构建新的JSON结构\n" +
                    "    var outputData = {\n" +
                    "        \"devices\": []\n" +
                    "    };\n" +
                    "\n" +
                    "    // 动态添加设备信息\n" +
                    "    for (var i = 0; i < data.vals.length; i++) {\n" +
                    "        var device = {\n" +
                    "            \"deviceId\": \"1\",\n" +
                    "            \"deviceState\": 1,\n" +
                    "            \"deviceData\": {}\n" +
                    "        };\n" +
                    "\n" +
                    "        // 动态添加设备数据\n" +
                    "        for (var key in data.vals[i].data) {\n" +
                    "            if (data.vals[i].data.hasOwnProperty(key)) {\n" +
                    "                device.deviceData[key] = data.vals[i].data[key];\n" +
                    "            }\n" +
                    "        }\n" +
                    "\n" +
                    "        // 将设备信息添加到输出数据中\n" +
                    "        outputData.devices.push(device);\n" +
                    "    }\n" +
                    "\n" +
                    "    // 将新的JSON结构转换为JSON字符串并返回\n" +
                    "    return JSON.stringify(outputData, null, 4);\n" +
                    "})(inputData)");
            System.out.println("Modified JSON = " + eval);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
