<h1 align="center">
      <a href="https://www.iot-cn.com.cn/#/login" style="font-size:40px; color:#0094fa;">设备管家</a>
</h1>

![输入图片说明](WechatIMG176.jpg)

#### 介绍
  设备管家是基于 Spring Cloud 的开源可分布式物联网 (IOT) 平台,平台运行在轻量级且高性能的Undertow容器上，结合docker-compose进行容器化部署，大大简化了部署和运维过程，提高了平台的可移植性和易用性。整体而言，我们的物联网平台通过这些先进的技术和工具，为用户提供了一个全面、高效且安全的解决方案，满足了现代物联网应用对于数据处理、服务管理和系统安全的高标准需求 



#### 软件架构

![输入图片说明](https://foruda.gitee.com/images/1708932686342497264/c71c079c_11549256.jpeg "WechatIMG172.jpg")

#### 技术实现

![输入图片说明](https://foruda.gitee.com/images/1708932752920293454/022c3876_11549256.png "屏幕截图")

  :ballot_box_with_check: **开发框架** ：使用Spring Boot，这是一个非常流行的Java开发框架，用于创建微服务应用。它简化了配置和部署过程，使开发更加快速和高效

  :ballot_box_with_check: **微服务框架** ：Spring Cloud Alibaba，提供了一套微服务解决方案，包括服务限流降级、服务链路追踪、服务配置管理等功能，有助于构建稳定且易于管理的微服务架构

 :ballot_box_with_check: **安全框架** ：使用sa-token，这是一个轻量级的权限认证框架，用于处理用户认证和权限控制，确保平台的安全性

 :ballot_box_with_check: **数据库支持** ：支持MySQL和TDengine，MySQL用于处理通用数据存储需求，而TDengine是一个专为物联网、大数据场景设计的高性能时序数据库

 :ballot_box_with_check: **数据库连接池** ：采用Druid，这是一个高性能的数据库连接池，提供强大的监控和扩展功能，确保数据库操作的高效和稳定

 :ballot_box_with_check: **服务注册与发现** ：通过Nacos实现，它是一个更适合云原生时代的动态服务发现和配置管理平台，有助于服务的自动注册和发现

 :ballot_box_with_check: **客户端负载均衡** ：使用Spring Cloud Loadbalancer，这是一个基于Spring Cloud的负载均衡工具，可以实现对服务的智能路由和负载分配

 :ballot_box_with_check: **网关组件** ：采用Spring Cloud Gateway，作为微服务架构中的API网关，提供路由、权限校验等功能，是微服务组件之间通信的重要桥梁

 :ballot_box_with_check: **运行容器** ：使用Undertow，这是一个轻量级且高性能的Web服务器，支持非阻塞IO，适合作为微服务的运行容器

 :ballot_box_with_check: **部署** ：通过docker-compose进行部署，这使得在不同环境中的部署和管理变得更加简单和一致

 :ballot_box_with_check: **VUE3** ：作为前端框架的核心，Vue3提供了响应式数据绑定和组合式API，变得更加简单和高效

 :ballot_box_with_check: **TypeScript** ：使用TypeScript作为开发语言，提高了代码的可读性和可维护性，同时也减少了运行时错误

 :ballot_box_with_check: **Vite** ：利用Vite作为构建工具，我们实现了快速的开发迭代和部署使平台能够迅速适应市场变化和业务需求

 :ballot_box_with_check: **CryptoJS** ：CryptoJS是一个加密标准的JavaScript库，用于数据的加密和解密，保证了数据传输的安全性

Sass让我们能够以更组织化的方式编写CSS，而Vite作为下一代前端构建工具，大大提升了我们的开发和构建速度，我们还整合了VueRouter和Pinia，分别用于前端路由管理和状态管理，这使得应用的导航和数据流更加流畅和可预测。ElementPlus作为一套基于Vue3的组件库，极大地加速了我们的UI开发。

#### 示例 <a href="https://www.iot-cn.com.cn/#/login" style="font-size:40px; color:#0094fa;">试例地址点击跳转</a>


#### 版权说明
- 设备管家代码开发基座采用AGPL-3.0技术协议
- 设备管家代码完全开源，可用于个人学习交流使用
- 不允许商业使用,如需商业使用请联系作者
- 不允许进行简单修改包装声称是自己的项目

#### 协助计划

如果你在使用或者部署过程遇到任何问题，请联系作者微信。

🌟 如果你觉得很不错，请点个 Start 鼓励我们吧！🌟

![输入图片说明](WechatIMG43.jpg)

#### 赞助 一杯奶茶足已



![输入图片说明](image.png)
                   