package com.yk.auth.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户忘记密码对象
 *
 * @author lmx
 * @date 2023/10/16 15:15
 */
@Data
@ApiModel(description="用户忘记密码对象")
public class ForgotPasswordBody{

    /**
     * 密码
     */
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "密码")
    private String password;

    /**
     * 确认密码
     */
    @NotBlank(message = "确认密码不能为空")
    @ApiModelProperty(value = "确认密码")
    private String confirmPassword;

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String phone;

    /**
     * 短信code
     */
    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value="短信code")
    private String smsCode;

    /**
     * 短信code类型
     */
    @ApiModelProperty(value="短信code类型")
    private Integer codeType;
}
