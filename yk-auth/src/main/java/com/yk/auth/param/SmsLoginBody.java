package com.yk.auth.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 短信登录对象
 *
 * @author lmx
 * @date 2023/10/16 15:15
 */
@Data
@ApiModel(description = "短信登录对象")
public class SmsLoginBody {

    /**
     * 手机号
     */
    @NotBlank(message = "手机号不能为空")
    @ApiModelProperty(value="手机号")
    private String phone;

    /**
     * 短信code
     */
    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value="短信code")
    private String smsCode;

    /**
     * 3天内自动登录
     */
    @ApiModelProperty(value = "3天内自动登录")
    private Boolean autoLogin;
}
