package com.yk.auth.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 小程序登录参数
 *
 * @author lmx
 * @date 2023/11/29 15:03
 */
@Data
@ApiModel(description = "小程序登录参数")
public class XcxLoginBody {
    @NotBlank(message = "vi 不能为空")
    @ApiModelProperty(value = "用户唯一标识")
    String vi;
    @NotBlank(message = "encryptedData 不能为空")
    @ApiModelProperty(value = "包括敏感数据在内的完整用户信息的加密数据")
    String encryptedData;
    @NotBlank(message = "jsCode 不能为空")
    @ApiModelProperty(value = "加密算法的初始向量")
    String jsCode;
}
