package com.yk.auth.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 用户登录对象
 *
 * @author lmx
 * @date 2023/10/16 15:14
 */
@Data
@ApiModel(description="用户登录对象")
public class LoginBody {

    /**
     * 用户名
     */
    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名")
    private String username;

    /**
     * 用户密码
     */
    @NotBlank(message = "用户密码不能为空")
    @ApiModelProperty(value = "用户密码")
    private String password;


    /**
     * 3天内自动登录
     */
    @ApiModelProperty(value = "3天内自动登录")
    private Boolean autoLogin;
}
