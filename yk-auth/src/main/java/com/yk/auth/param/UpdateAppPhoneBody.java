package com.yk.auth.param;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 小程序修改手机号对象
 *
 * @author lmx
 * @date 2023/10/16 15:15
 */
@Data
@ApiModel(description="小程序修改手机号对象")
public class UpdateAppPhoneBody {

    @NotBlank(message = "新手机号不能为空")
    @ApiModelProperty(value = "手机号")
    private String newPhone;

    @NotBlank(message = "验证码不能为空")
    @ApiModelProperty(value="短信code")
    private String smsCode;
}
