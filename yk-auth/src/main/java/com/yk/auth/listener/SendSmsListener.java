package com.yk.auth.listener;

import com.yk.common.core.dto.SendAlibabaSmsDTO;
import com.yk.common.core.enums.AliYunSmsCodeEnum;
import com.yk.common.rabbitmq.constant.QueueConstants;
import lombok.extern.slf4j.Slf4j;
import org.dromara.sms4j.api.SmsBlend;
import org.dromara.sms4j.api.entity.SmsResponse;
import org.dromara.sms4j.core.factory.SmsFactory;
import org.dromara.sms4j.provider.enumerate.SupplierType;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.util.LinkedHashMap;

/**
 * 短信发送监听
 * @author lmx
 * @date 2023/10/21 10:08
 */
@Slf4j
@Component
public class SendSmsListener {

	@RabbitListener(queues = QueueConstants.SEND_SMS)
	@RabbitHandler
	public void onMessage(SendAlibabaSmsDTO dto) {
		LinkedHashMap<String, String> map = new LinkedHashMap<>(1);
		map.put("code", dto.getContent());
		SmsBlend smsBlend = SmsFactory.createSmsBlend(SupplierType.ALIBABA);
		SmsResponse smsResponse = smsBlend.sendMessage(dto.getPhone(), dto.getTemplateId(), map);
		if (!smsResponse.isSuccess()) {
			AliYunSmsCodeEnum codeEnum = AliYunSmsCodeEnum.getCodeEnum(smsResponse.getCode());
			log.error("验证码短信发送异常 => {}", codeEnum.getErrMsg() + smsResponse.getMessage());
		}
	}

	@RabbitListener(queues = QueueConstants.SEND_ALARM_SMS)
	@RabbitHandler
	public void onAlarmMessage(SendAlibabaSmsDTO dto) {
		SmsBlend smsBlend = SmsFactory.createSmsBlend(SupplierType.ALIBABA);
		SmsResponse smsResponse = smsBlend.sendMessage(dto.getPhone(), dto.getTemplateId(), dto.getMap());
		if (!smsResponse.isSuccess()) {
			AliYunSmsCodeEnum codeEnum = AliYunSmsCodeEnum.getCodeEnum(smsResponse.getCode());
			log.error("验证码短信发送异常 => {}", codeEnum.getErrMsg() + smsResponse.getMessage());
		}
	}
}