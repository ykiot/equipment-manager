package com.yk.auth.controller;

import com.yk.auth.param.LoginBody;
import com.yk.auth.param.SmsLoginBody;
import com.yk.auth.param.XcxLoginBody;
import com.yk.auth.service.SysLoginService;
import com.yk.common.core.domain.Result;
import com.yk.common.core.enums.DeviceType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 小程序登录控制器 yk-auth
 *
 * @author lmx
 * @module 设备管家
 * @date 2023/10/17 11:48
 */
@Slf4j
@Api(tags = "小程序登录控制器")
@RestController
@RequiredArgsConstructor
@RequestMapping("/auth/wx")
public class XcxTokenController {

    private final SysLoginService sysLoginService;

    @ApiOperation("小程序授权")
    @GetMapping("/{jsCode}")
    public Result<String> loginAppKey(@PathVariable String jsCode) {
        return Result.data(sysLoginService.loginAppKey(jsCode));
    }

    @ApiOperation("小程序登录")
    @PostMapping("/xcxLogin")
    public Result<String> xcxLogin(@RequestBody XcxLoginBody xcxLoginBody) {
        return Result.data(sysLoginService.xcxLogin(xcxLoginBody));
    }

    @ApiOperation("账号密码登录")
    @PostMapping("/login")
    public Result<String> login(@Validated @RequestBody LoginBody form) {
        return Result.data(sysLoginService.login(form, DeviceType.XCX));
    }

    @ApiOperation("短信登录")
    @PostMapping("/smsLogin")
    public Result<String> smsLogin(@Validated @RequestBody SmsLoginBody smsLoginBody) {
        return Result.data(sysLoginService.smsLogin(smsLoginBody, DeviceType.XCX));
    }
}
